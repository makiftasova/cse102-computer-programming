/* ************************************************************************* */
/*  FILENAME: HW05_111044016_PART2.c                                         */
/*  Created on 27.04.2012 by Mehmet Akif TASOVA                              */
/*  ------------------------------------------------------------------------ */
/*  This program takes nouns separated by spaces and returns their           */
/*     plural forms. The rules are:                                          */
/*  1-) If a noun ends in “y”, remove the “y” and add “ies”.                 */
/*  2-) If a noun ends in “s”, “ch” or “sh” add “es”.                        */
/*  3-) In all other cases, just add “s”.                                    */
/*                                                                           */
/* ************************************************************************* */

/* ************************************************************************* */
/*                               INCLUDES                                    */
/* ************************************************************************* */

#include <stdio.h>  /* Standart input/output library                */
#include <string.h> /*Some userful String manipulating functions    */
#include <ctype.h>  /* Some useful chat manupilating functions      */

/* ************************************************************************* */
/*                               DEFINITIONS                                 */
/* ************************************************************************* */

#define MAX_LEN 1025    /* maximum length of string with NULL character  */
#define SPACE ' '       /* space character in type char                  */
#define STR_SPACE " "   /* space character as string                     */
#define ENDS_Y "ies"    /* suffix for nouns ending with 'y'              */
#define ENDS_S "es"     /* suffix for nouns ending with 's','ch' or 'sh' */
#define ENDS_DEF "s"    /* suffix for all other nouns                    */
#define NULL_CHAR '\0'  /* NULL pointer as type char */

/* ************************************************************************* */
/*                         FUNCTION PROTOTYPES                               */
/* ************************************************************************* */

/* Fill string with space(' ') character */
void cleanString(char string[MAX_LEN]);

/* Find last character of string. Function will return character before NULL */
char* findLastChar(char string[MAX_LEN], int *index);

/* Gets a string which includes some nouns separetd by spaces
 *      and makes this nouns plural
 */
int makePlural(char string[MAX_LEN]);


/* Finds and replaces space characters in a string with NULL 
 *      if there is at least one
 */
int replaceSpaces(char string[MAX_LEN]);

/* Gets a string and counts how many space 
 *      characters included in this string 
 */
int countSpaces(const char string[MAX_LEN]);

/* Finds first space character in a string and returns its index */
int findSpaces(const char string[MAX_LEN]);

/* finds and replaces NULL characters in a string with space 
 *      if there is more than one
 */
int replaceNulls(char string[MAX_LEN]);

/* Counts total number of NULL chracters in a string */
int countNulls(char string[MAX_LEN]);


/* Main Function */
int main(void)
{
    char string[MAX_LEN], restString[MAX_LEN], tempStr[MAX_LEN];
    int position=0, length, timesToChop=0;
    int printLen=0, programCounter=0, i=0;
    
    /* Preparing program for avoiding errors */
    cleanString(string);
    cleanString(tempStr);
    cleanString(restString);
    tempStr[0] = NULL_CHAR;
    
    /* print beginning instruction to screen */
    printLen = printf("ENTER NOUN(S) SEPARATED BY SPACES\n");
    for(i=0;i<(printLen-1);++i)
        printf("-");
    
    /* get nouns from user */
    printf("\n>>>");
    gets(string);
    
    replaceSpaces(string); /* separate each noun by NULLs */
    
    timesToChop = countNulls(string); /* number of nouns in string */
    
    for(programCounter=0;programCounter<timesToChop;++programCounter)
    {
        strcat(tempStr, &string[position]);
        length = strlen(&string[position]);
        position += (length + 1);
        makePlural(tempStr);
        strcat(tempStr, STR_SPACE);
    };
    
    printLen = printf("PLURAL FORM(S) OF GIVEN NOUNS\n");
    for(i=0;i<(printLen-1);++i)
        printf("-");
    printf("\n");
    
    printf("%s", tempStr); /* print manipulated string to screen */
    
    return 0;
}

/* ************************************************************************* */
/*                          FUNCTION DEFINITIONS                             */
/* ************************************************************************* */

/* Fill string with space character 
 * Pre: a string defined and length is MAX_LEN
 * Post: string filled with space(' ') characters 
 */
void cleanString(char string[MAX_LEN])
{
    int i=0;
    
    for(i=0;i<MAX_LEN;++i)
        string[i] = SPACE;
    
    return;
}

/* Find last character of string. Function will return character before NULL
 * 
 * Pre: A defined string
 * Post: Found and returned address of character before NULL
 */
char* findLastChar(char string[MAX_LEN], /* input string to manipulate */
                    int *index)     /* output - index of found char */
{
    int indx=0; /* variable for storing index value */
    
    indx = (strlen(string) - 1);
    
    *index = indx; /* return index of noun's last character */
    
    return (&string[indx]); /* return address of noun's last character */
}

/* Gets a string which includes one noun and make this noun plural
 *      return number of inserted characters to noun
 * Pre: String which includes a noun defined
 * Post: Plural form of noun
 */
int makePlural(char string[MAX_LEN])
{
    int index=0, numOfChars=0;
    char lastChar, beforeLastChar;
    
    lastChar = *findLastChar(string, &index); /* get info abour last char */
    beforeLastChar = string[index - 1];
    
    switch (tolower(lastChar))
    {
        case 'y': /* if string ends with 'y' */
            string[index] = NULL_CHAR; /* crop last char of string */
            strcat(string, ENDS_Y); /* add 'ies' suffix to string */
            numOfChars = 2;
            break;
        case 's' : /* if string ends with 's' */
            strcat(string, ENDS_S); /* add 'es 'suffix to string */
            numOfChars = 2;
            break;
        case 'h': /* if string ends with 'h' */
            switch (tolower(beforeLastChar))
            {
                case 'c': /* if string ends with 'ch' */
                case 's': /* if string ends with 'sh' */
                    strcat(string, ENDS_S); /* add 'es 'suffix to string */
                    numOfChars = 2;
                    break;
                default: /* if noun doesn't fit cases above */
                    strcat(string, ENDS_DEF); /* add 's' suffix to string */
                    numOfChars = 1;
                    break;
            };
            break; /* end of "ending with 'h'" situation */
            
        default: /* if noun doesn't fit other cases */
            strcat(string, ENDS_DEF); /* add 's' suffix to string */
            numOfChars = 1;
            break;
    };
    
    return (numOfChars);
}

/* Finds and replaces space characters in a string with NULL 
 *      if there is at least one
 * Pre: a string defined and has at least one space character 
 * Post: all space characters in string replaced with NULLs
 *         and returned total number of changes
 */
int replaceSpaces(char string[MAX_LEN])
{
    int index=0, count=0;
    
    for(index=0;string[index]!=NULL_CHAR;++index)
    {
        ++count;
        if(string[index] == SPACE)
        {
            string[index] = NULL_CHAR;
        };
    };
    
    return (count);
}

/* Gets a string and counts how many space 
 *      characters included in this string
 * Pre: string defined
 * Post: total number of space characters in given string counted and returned
 */
int countSpaces(const char string[MAX_LEN])
{
    int numOfSpaces=0, count=0;
    
    for(count=0;string[count]!=NULL_CHAR; ++count)
    {
        if(string[count] == SPACE)
            ++numOfSpaces;
    };
    
    return (numOfSpaces);
}

/* Finds first space character in a string and returns its index 
 * Pre: string defined
 * Post: index of first space character found and returned
 */
int findSpaces(const char string[MAX_LEN])
{
    int index=0;
    
    for(index=0;string[index]!=SPACE;++index);
    
    return (index);
}

/* Finds and replaces NULL characters in a string with space 
 *      if there is more than one
 * Pre: a string defined and has more than one NULL('\0') characters 
 * Post: NULL characters in string changed by spaces except last NULL
 *          and returned total number of changes
 */
int replaceNulls(char string[MAX_LEN])
{
    int index=0, totalNulls=0, count=0;
    
    totalNulls = countNulls(string);
    
    for(index=0;index<MAX_LEN;++index)
    {
        ++count;
        if((string[index] == NULL_CHAR) && (count < totalNulls))
        {
            string[index] = SPACE;
        };
    };
    
    return (count);
}

/* Counts total number of NULL chracters in a string 
 * Pre: A string defined with more than one NULLs
 * Post: Total number of Nulls in string counted and returned */
int countNulls(char string[MAX_LEN])
{
    int numOfNulls=0, count=0;
    
    for(count=0;count<MAX_LEN;++count)
    {
        if(string[count] == NULL_CHAR)
            ++numOfNulls;
    };
    
    return (numOfNulls);
}


/* ************************************************************************* */
/*                         END OF HW05_111044016_PART2.c                     */
/* ************************************************************************* */
