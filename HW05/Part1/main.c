
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <ctype.h>
#include "Postures.h"


#define WAITING_TIME 0.2    /* Wait time between postures               */
#define START_POSITION 30   /* Initial Position of the character        */
#define NOT_FOGGED 15       /* Additional value for visible range       */
#define ZYBK_WAIT 0.5       /* Wait time between postures for zeybek    */
#define DEBUG_MODE 0        /* On/Off Debug mode (1 - On / 0 - Off)     */

/* Directions that the character can turn or move */
typedef enum
{
    LEFT=-1,FRONT,RIGHT
} courses;

/* Possible error values */
typedef enum
{
    incorrectInput
} errors;


/* Display and initialization functions */
char menu(void);
void initialize(int ,int* , char [][WIDTH]);

/* Turning functions */
void turnLeft(int, int* , char [][WIDTH]);
void turnRight(int, int* , char [][WIDTH]);
void turnFront(int, int* , char [][WIDTH]);

/* Movement functions */
void walk(int, courses, int*, char [][WIDTH]);
void walkToRight(int , courses , int* , char [][WIDTH]);
void walkToLeft(int , courses , int* , char [][WIDTH]);
void jump(int, int, courses, char character[][WIDTH]);
void playZeybek(int*, courses, char character[][WIDTH]);
void move(int , int , char [][WIDTH], void (char [][WIDTH]));
void zeybekMove(int, courses, char character[][WIDTH],
                 void (char [][WIDTH]), double);

/* Accessory functions */
void deleteCinAli(char [][WIDTH]);
void printCinAli(int , char [][WIDTH]);
void setStepNumber(int *step);
void error(errors i);
void gameOver(void);
void flushInputBuffer(void);
void wait ( double );


/* Main function */
int main(void)
{
    courses direction=FRONT;    /* Direction of the character */
    int     currentPosition=START_POSITION, /* Position of the character */
            step;   /* The step number that athe character moves in a direction */

    char    cinAli[HEIGHT][WIDTH], /* the Character of the Game*/
            action;    /* Type of action that user enters */

    /* Initialize Cin Ali */
    initialize(currentPosition, &direction, cinAli);

    /* Play game until user exits */
    do
    {
        /* Display menu and read the order user gave*/
        action=menu();
        /* Do the action */
        switch (action)
        {
            case '>': /* Move Right */
                setStepNumber(&step); /* Read and set the step number */
                turnRight(currentPosition, &direction, cinAli);
                walk(step, direction, &currentPosition, cinAli);
                break;
            case '<': /* Move left */
                setStepNumber(&step);
                turnLeft(currentPosition, &direction, cinAli);
                walk(step, direction, &currentPosition, cinAli);
                break;
            case '=': /* Turn Front */
                turnFront(currentPosition, &direction, cinAli);
                break;
            case 'r': /* Turn Right */
                turnRight(currentPosition, &direction, cinAli);
                break;
            case 'l': /* Turn Left */
                turnLeft(currentPosition, &direction, cinAli);
                break;
            case 'x': /* Exit */
                gameOver();
                break;
            case 'j':
                setStepNumber(&step);
                turnFront(currentPosition, &direction, cinAli);
                jump(step, currentPosition, direction, cinAli);
                break;
            case 'z': /* Play Zeybek */
                playZeybek(&currentPosition, direction, cinAli);
                break;
            default:
                error(incorrectInput); /* Incorrect input from the user */
                break;
        }
        flushInputBuffer(); /* Clean the input buffer */

    }while(action!='x');

    return 0;
}


/*
 *  Displays options for the user to select an available action
 *  Pre:  database accesses a binary file of product_t records that has
 *        been opened as an input file, and params is defined
 *  Post: It returns the chosen action
 */
char menu(void)
{
    char select;
    printf("\n\nEnter '>' for Cin Ali to walk right\n");
    printf("Enter '<' for Cin Ali to walk left\n");
    printf("Enter '=' for Cin Ali to turn front\n");
    printf("Enter 'r' for Cin Ali to turn right\n");
    printf("Enter 'l' for Cin Ali to turn left\n");
    printf("Enter 'j' for Cin Ali to jump\n");
    printf("Enter 'z' for Cin Ali to play Zeybek\n");
    printf("Enter 'x' to Exit Game\n");
    scanf("%c",&select);
    return tolower(select);
}


/*
* Prepares program for first use
* Pre:  Current position, direction and situation of Cin Ali
* Post: Call move function to initialise Cin Ali's situation
*       at program start
*/
void initialize(int currentPosition, courses* direction, 
                char character[][WIDTH])
{
    *direction=FRONT;
    move(currentPosition, *direction, character, frontPosture);
}


/*
* Gets number of step
* Pre: an int variable step defined and its address sent into this function
* Post: step number read from user and retured as output parameter
*/
void setStepNumber(int *step)
{
    printf("Step Number: ");
    scanf("%d", step);
}


/*
* Cleans frame array before copying next frame in it
* Pre: an array for storing frames defined
* Post: (the frame store array filled with space(' ') characters
*/
void deleteCinAli(char character[][WIDTH])
{
    int i, j;
    for(i=0; i<HEIGHT; ++i)
    {
        for(j=0; j<WIDTH; ++j)
        {
            character[i][j]=' ';
        }
    }
    
    if(DEBUG_MODE)
    {
        printf("Delete succesful\n");
    };
}


/*
* Prints given Cin Ali frame and ground part which visible 
*       by Cin Ali to screen. Other areas cannot visible because of fog of war
* Pre: Curent position and frame defined
* Post: Cin Ali frame printed screen on given position
*/
void printCinAli(int currentPosition, char character[][WIDTH])
{
    int i, j;
    for(i=0; i<HEIGHT; ++i)
    {
        /* Draw spaces of position */
        for(j=0; j<currentPosition; ++j)
        {
            printf(" ");
        }
        /* draw Cin Ali */
        for(j=0; j<WIDTH; ++j)
        {
            printf("%c", character[i][j]);
        }
        printf("\n");
    };
    
    /* draw ground */
    /* Draw spaces of position */
    for(j=0; j<(currentPosition-NOT_FOGGED); ++j)
    {
        printf(" ");
    };
    for(j=0; j<(WIDTH+(2*NOT_FOGGED)); ++j)
    {
        printf("#");
    };
    printf("\n");
    
    if(DEBUG_MODE)
    {
        printf("Print Succesful\n");
    };
}


/*
* Moves Cin Ali to given position and posture
* Pre: current position, direction, current frame and next frame given
* Post: Printed cin ali to screen at given direction and posture
*       then waited for WAITING_TIME seconds
*/
void move(int currentPosition, courses direction, char character[][WIDTH], 
           void posture(char [][WIDTH]))
{
    system("clear");
    deleteCinAli(character);
    posture(character);
    printCinAli(currentPosition, character);
    if(DEBUG_MODE)
    {
        printf("Position: %d\n", currentPosition);
        printf("Direction: %d\n", direction);
    };
    wait(WAITING_TIME);
}

/*
* Moves Cin Ali to given zeybek position
* Pre: current position, direction, current frame and next frame given
* Post: Printed cin ali to screen at given direction and posture
*       then waited for waitTime seconds
*/
void zeybekMove(int currentPosition, courses direction, 
                char character[][WIDTH], void posture(char [][WIDTH]),
                double waitTime)
{
    system("clear");
    deleteCinAli(character);
    posture(character);
    printCinAli(currentPosition, character);
    if(DEBUG_MODE)
    {
        printf("Position: %d\n", currentPosition);
        printf("Direction: %d\n", direction);
    };
    wait(waitTime);
}

/*
* Turns Cin Ali to left side
* Pre: current position, and curreny frame as input parameter,
*       address of direction variable as output parameter
* Post: Prints Cin Ali as turned left
*/
void turnLeft(int currentPosition, courses* direction, char character[][WIDTH])
{
    *direction=LEFT;
    move(currentPosition, *direction, character, leftPosture);
}


/*
* Turns Cin Ali to right side
* Pre: current position, and curreny frame as input parameter,
*       address of direction variable as output parameter
* Post: Prints Cin Ali as turned right
*/
void turnRight(int currentPosition, courses* direction, char character[][WIDTH])
{
    *direction=RIGHT;
    move(currentPosition, *direction, character, rightPosture);
}


/*
* Turns Cin Ali to front side
* Pre: current position, and curreny frame as input parameter,
*       address of direction variable as output parameter
* Post: Prints Cin Ali as turned front
*/
void turnFront(int currentPosition, courses* direction, char character[][WIDTH])
{
    *direction=FRONT;
    move(currentPosition, *direction, character, frontPosture);
}


/*
* Walks Cin Ali to given direction by given steps
* Pre: current position, walking direction and number of steps defined
* Post: Cin Ali completed walking
*/
void walk(int step, courses direction, int *currentPosition, char character[][WIDTH])
{
    switch (direction)
    {
        case LEFT:
            walkToLeft(step, direction, currentPosition, character);
            break;
        case RIGHT:
            walkToRight(step, direction, currentPosition, character);
            break;
        default:
            break;
    }
}


/*
* Walks Cin Ali to right side
* Pre: number of steps of walk and direction given
* Post: Cin Ali walked to given position and last position of Cin Ali
*       returned as output parameter
*/
void walkToRight(int step, courses direction, int* currentPosition, char character[][WIDTH])
{
    int i;
    for(i=0; i<step; ++i)
    {
        move((*currentPosition)++, direction, character, toRightFootsDown);
        move((*currentPosition)++, direction, character, toRightBeckFootUp);
        move((*currentPosition)++, direction, character, toRightCenteral);
        move((*currentPosition)++, direction, character, toRightFrontFootUp);
    }
    move((*currentPosition), direction, character, rightPosture);
    
    if(DEBUG_MODE)
    {
        printf("Total steps: %d", i);
    };
}

/*
* Jumps Cin Ali by given times
* Pre:  number of times, current position and direction given
* Post: Cin Ali jumped by given times
*/
void jump(int step, int currentPosition, courses direction, char character[][WIDTH])
{
    int i;
    for(i=0; i<step; ++i)
    {
        move((currentPosition), direction, character, frontPosture);
        move((currentPosition), direction, character, jump1);
        move((currentPosition), direction, character, jump2);
        move((currentPosition), direction, character, jump3);
        move((currentPosition), direction, character, jump2);
        move((currentPosition), direction, character, frontPosture);
    }
}

/*
* Plays Zeybek to Cin Ali.
* Pre: currentPositon, direction and frame array given
* Post: Cin Ali plays zeybek for 1 loop thes stops playing
*/
void playZeybek(int* currentPosition, courses direction, char character[][WIDTH])
{
    zeybekMove((*currentPosition), direction, character, zeybek0, ZYBK_WAIT);
    
    zeybekMove((*currentPosition)++, direction, character, zeybek1, ZYBK_WAIT);
    zeybekMove((*currentPosition)++, direction, character, zeybek2, ZYBK_WAIT);
    zeybekMove((*currentPosition)++, direction, character, zeybek3, ZYBK_WAIT);
    zeybekMove((*currentPosition)++, direction, character, zeybek4, ZYBK_WAIT);
    zeybekMove((*currentPosition)++, direction, character, zeybek5, ZYBK_WAIT);
    zeybekMove((*currentPosition)--, direction, character, zeybek6, ZYBK_WAIT);
    printf("\a"); /* beep when Cin Ali hits to ground */
    zeybekMove((*currentPosition)--, direction, character, zeybek7, ZYBK_WAIT);
    zeybekMove((*currentPosition)--, direction, character, zeybek8, ZYBK_WAIT);
    zeybekMove((*currentPosition)--, direction, character, zeybek9, ZYBK_WAIT);
    zeybekMove((*currentPosition)--, direction, character, zeybek10, ZYBK_WAIT);
    printf("\a"); /* beep when Cin Ali hits to ground */
    zeybekMove((*currentPosition), direction, character, zeybek11, 2*ZYBK_WAIT);
    zeybekMove((*currentPosition), direction, character, zeybek12, 2*ZYBK_WAIT);
    zeybekMove((*currentPosition), direction, character, zeybek13, 2*ZYBK_WAIT);
    zeybekMove((*currentPosition), direction, character, zeybek14, 2*ZYBK_WAIT);
    zeybekMove((*currentPosition), direction, character, zeybek15, 2*ZYBK_WAIT);
    printf("\a"); /* beep when Cin Ali hits to ground */
    zeybekMove((*currentPosition), direction, character, zeybek8, 2*ZYBK_WAIT);
    
    /* end of playing zeybek */
    zeybekMove((*currentPosition), direction, character, frontPosture, ZYBK_WAIT);
}

/*
* Walks Cin Ali to left side
* Pre: number of steps of walk and direction given
* Post: Cin Ali walked to given position and last position of Cin Ali
*       returned as output parameter
*/
void walkToLeft(int step, courses direction, int* currentPosition, char character[][WIDTH])
{
    int i;
    for(i=0; i<step; ++i)
    {
        move((*currentPosition)--, direction, character, toLeftFootsDown);
        move((*currentPosition)--, direction, character, toLeftBeckFootUp);
        move((*currentPosition)--, direction, character, toLeftCenteral);
        move((*currentPosition)--, direction, character, toLeftRightFootUp);
    }
    move((*currentPosition), direction, character, leftPosture);
    
    if(DEBUG_MODE)
    {
        printf("Total steps: %d", i);
    };
}


/*
* Determines which error occurred while program execution
* Pre: an error occured and its error code given into this function
* Post: a message printed to screen related with occurred error
*/
void error(errors i)
{
    switch(i)
    {
        case incorrectInput:
            printf("Please, enter the right input\n");
            break;
        default:
            break;
    }
}


/*
* Prints "Game Over!" to screen and wait 1 second
*   it says user to end of game
* Pre: call by other function
* Post: prints "Game Over!" to screen then waits for 1 seconds 
*       and returns caller function
*/
void gameOver(void)
{
    printf("Game Over!");
    wait(1);
}


/*
* Pauses program execution for given seconds length
* Pre: a wait time given by seconds
* Post: program pauses for given seconds
*/
void wait ( double seconds )
{
    clock_t endwait;
    endwait = clock () + seconds * CLOCKS_PER_SEC ;
    while (clock() < endwait) {}
}



/*
* Cleans input buffer, so scanf function can read user's choice 
*   without errors
* Pre: Has no requirements
* Post: input buffer cleaned
*/
void flushInputBuffer(void)
{
    char junk;
    /* A loop that cleans the input buffer till '\n' character being entered*/
    do
    {
        scanf("%c",&junk);
    }while(junk!='\n');
}
