/* Mehmet Akif TASOVA --- 111044016 --- Postures.c */

#include "Postures.h"

/*
* Change the Character to fornt state posture
* Pre:  character[][WIDTH] should be initialized
*       global parameters are defined
* Post: character[][WIDTH] is modified
*/
void frontPosture(char character[][WIDTH])
{
    character[3][3]=TEXTURE;
    character[4][2]=TEXTURE;
    character[4][4]=TEXTURE;
    character[5][3]=TEXTURE;
    character[6][2]=TEXTURE;
    character[6][3]=TEXTURE;
    character[6][4]=TEXTURE;
    character[7][1]=TEXTURE;
    character[7][3]=TEXTURE;
    character[7][5]=TEXTURE;
    character[8][3]=TEXTURE;
    character[9][2]=TEXTURE;
    character[9][4]=TEXTURE;
    character[10][2]=TEXTURE;
    character[10][4]=TEXTURE;
    character[11][1]=TEXTURE;
    character[11][2]=TEXTURE;
    character[11][4]=TEXTURE;
    character[11][5]=TEXTURE;
}


/*
* Change the Character to turned left side state posture
* Pre:  character[][WIDTH] should be initialized
*       global parameters are defined
* Post: character[][WIDTH] is modified
*/
void leftPosture(char character[][WIDTH])
{
    character[3][3]=TEXTURE;
    character[4][2]=TEXTURE;
    character[4][4]=TEXTURE;
    character[5][3]=TEXTURE;
    character[6][3]=TEXTURE;
    character[7][2]=TEXTURE;
    character[7][3]=TEXTURE;
    character[8][3]=TEXTURE;
    character[9][3]=TEXTURE;
    character[10][3]=TEXTURE;
    character[11][2]=TEXTURE;
    character[11][3]=TEXTURE;
}


/*
* Change the Character to turned right side state posture
* Pre:  character[][WIDTH] should be initialized
*       global parameters are defined
* Post: character[][WIDTH] is modified
*/
void rightPosture(char character[][WIDTH])
{
    character[3][3]=TEXTURE;
    character[4][2]=TEXTURE;
    character[4][4]=TEXTURE;
    character[5][3]=TEXTURE;
    character[6][3]=TEXTURE;
    character[7][3]=TEXTURE;
    character[7][4]=TEXTURE;
    character[8][3]=TEXTURE;
    character[9][3]=TEXTURE;
    character[10][3]=TEXTURE;
    character[11][3]=TEXTURE;
    character[11][4]=TEXTURE;
}


/*
* First frame of walking right, taking first step
* Pre:  character[][WIDTH] should be initialized
*       global parameters are defined
* Post: character[][WIDTH] is modified
*/
void toRightFootsDown(char character[][WIDTH])
{
    character[4][3]=TEXTURE;
    character[5][2]=TEXTURE;
    character[5][4]=TEXTURE;
    character[6][3]=TEXTURE;
    character[7][2]=TEXTURE;
    character[7][3]=TEXTURE;
    character[7][4]=TEXTURE;
    character[7][5]=TEXTURE;
    character[8][1]=TEXTURE;
    character[8][3]=TEXTURE;
    character[9][3]=TEXTURE;
    character[9][4]=TEXTURE;
    character[10][2]=TEXTURE;
    character[10][5]=TEXTURE;
    character[11][1]=TEXTURE;
    character[11][5]=TEXTURE;
}


/*
* Second frame of walking right. backside foot on air and other one on ground
* Pre:  character[][WIDTH] should be initialized
*       global parameters are defined
* Post: character[][WIDTH] is modified
*/
void toRightBeckFootUp(char character[][WIDTH])
{
    character[3][3]=TEXTURE;
    character[4][2]=TEXTURE;
    character[4][4]=TEXTURE;
    character[5][3]=TEXTURE;
    character[6][2]=TEXTURE;
    character[6][3]=TEXTURE;
    character[6][4]=TEXTURE;
    character[7][2]=TEXTURE;
    character[7][3]=TEXTURE;
    character[7][5]=TEXTURE;
    character[8][3]=TEXTURE;
    character[9][2]=TEXTURE;
    character[9][3]=TEXTURE;
    character[10][1]=TEXTURE;
    character[10][3]=TEXTURE;
    character[11][3]=TEXTURE;
}


/*
* Third frame of waking right. One foot going to touch ground when other is
*       leaving from ground
* Pre:  character[][WIDTH] should be initialized
*       global parameters are defined
* Post: character[][WIDTH] is modified
*/
void toRightCenteral(char character[][WIDTH])
{
    character[3][3]=TEXTURE;
    character[4][2]=TEXTURE;
    character[4][4]=TEXTURE;
    character[5][3]=TEXTURE;
    character[6][2]=TEXTURE;
    character[6][3]=TEXTURE;
    character[6][4]=TEXTURE;
    character[7][2]=TEXTURE;
    character[7][3]=TEXTURE;
    character[7][4]=TEXTURE;
    character[8][3]=TEXTURE;
    character[9][3]=TEXTURE;
    character[9][4]=TEXTURE;
    character[10][2]=TEXTURE;
    character[10][3]=TEXTURE;
    character[11][3]=TEXTURE;
}


/*
* Fourth frame of walking right. Front foot on air and other one on ground
* Pre:  character[][WIDTH] should be initialized
*       global parameters are defined
* Post: character[][WIDTH] is modified
*/
void toRightFrontFootUp(char character[][WIDTH])
{
    character[3][3]=TEXTURE;
    character[4][2]=TEXTURE;
    character[4][4]=TEXTURE;
    character[5][3]=TEXTURE;
    character[6][2]=TEXTURE;
    character[6][3]=TEXTURE;
    character[6][4]=TEXTURE;
    character[7][1]=TEXTURE;
    character[7][3]=TEXTURE;
    character[7][4]=TEXTURE;
    character[8][3]=TEXTURE;
    character[8][4]=TEXTURE;
    character[9][2]=TEXTURE;
    character[9][5]=TEXTURE;
    character[10][2]=TEXTURE;
    character[10][5]=TEXTURE;
    character[11][2]=TEXTURE;
}


/*
* First frame of walkng left. Both foot touching ground and back foot
*       is about to going air
* Pre:  character[][WIDTH] should be initialized
*       global parameters are defined
* Post: character[][WIDTH] is modified
*/
void toLeftFootsDown(char character[][WIDTH])
{
    character[4][3]=TEXTURE;
    character[5][2]=TEXTURE;
    character[5][4]=TEXTURE;
    character[6][3]=TEXTURE;
    character[7][1]=TEXTURE;
    character[7][2]=TEXTURE;
    character[7][3]=TEXTURE;
    character[7][4]=TEXTURE;
    character[8][3]=TEXTURE;
    character[8][5]=TEXTURE;
    character[9][2]=TEXTURE;
    character[9][3]=TEXTURE;
    character[10][1]=TEXTURE;
    character[10][4]=TEXTURE;
    character[11][1]=TEXTURE;
    character[11][5]=TEXTURE;
}


/*
* Second frame of walking left. Front foot is on ground and other is on air
* Pre:  character[][WIDTH] should be initialized
*       global parameters are defined
* Post: character[][WIDTH] is modified
*/
void toLeftBeckFootUp(char character[][WIDTH])
{
    character[3][3]=TEXTURE;
    character[4][2]=TEXTURE;
    character[4][4]=TEXTURE;
    character[5][3]=TEXTURE;
    character[6][2]=TEXTURE;
    character[6][3]=TEXTURE;
    character[6][4]=TEXTURE;
    character[7][1]=TEXTURE;
    character[7][3]=TEXTURE;
    character[7][4]=TEXTURE;
    character[8][3]=TEXTURE;
    character[9][3]=TEXTURE;
    character[9][4]=TEXTURE;
    character[10][3]=TEXTURE;
    character[10][5]=TEXTURE;
    character[11][3]=TEXTURE;
}


/*
* Third frame of walking left. front foot on ground and other one
*       is just passing by it
* Pre:  character[][WIDTH] should be initialized
*       global parameters are defined
* Post: character[][WIDTH] is modified
*/
void toLeftCenteral(char character[][WIDTH])
{
    character[3][3]=TEXTURE;
    character[4][2]=TEXTURE;
    character[4][4]=TEXTURE;
    character[5][3]=TEXTURE;
    character[6][2]=TEXTURE;
    character[6][3]=TEXTURE;
    character[6][4]=TEXTURE;
    character[7][2]=TEXTURE;
    character[7][3]=TEXTURE;
    character[7][4]=TEXTURE;
    character[8][3]=TEXTURE;
    character[9][2]=TEXTURE;
    character[9][3]=TEXTURE;
    character[10][3]=TEXTURE;
    character[10][4]=TEXTURE;
    character[11][3]=TEXTURE;
}


/*
* Fourth frame of walking left. Right foot is on air and getting near to ground
*       other foot is stable on ground
* Pre:  character[][WIDTH] should be initialized
*       global parameters are defined
* Post: character[][WIDTH] is modified
*/
void toLeftRightFootUp(char character[][WIDTH])
{
    character[3][3]=TEXTURE;
    character[4][2]=TEXTURE;
    character[4][4]=TEXTURE;
    character[5][3]=TEXTURE;
    character[6][2]=TEXTURE;
    character[6][3]=TEXTURE;
    character[6][4]=TEXTURE;
    character[7][2]=TEXTURE;
    character[7][3]=TEXTURE;
    character[7][5]=TEXTURE;
    character[8][2]=TEXTURE;
    character[8][3]=TEXTURE;
    character[9][1]=TEXTURE;
    character[9][4]=TEXTURE;
    character[10][1]=TEXTURE;
    character[10][4]=TEXTURE;
    character[11][4]=TEXTURE;
}

/*
* First frame of jumping. Cin Ali is preparing to jump
* Pre:  character[][WIDTH] should be initialized
*       global parameters are defined
* Post: character[][WIDTH] is modified
*/
void jump1(char character[][WIDTH])
{
    character[4][3]=TEXTURE;
    character[5][2]=TEXTURE;
    character[5][4]=TEXTURE;
    character[6][3]=TEXTURE;
    character[7][1]=TEXTURE;
    character[7][2]=TEXTURE;
    character[7][3]=TEXTURE;
    character[7][4]=TEXTURE;
    character[7][5]=TEXTURE;
    character[8][3]=TEXTURE;
    character[9][2]=TEXTURE;
    character[9][3]=TEXTURE;
    character[9][4]=TEXTURE;
    character[10][2]=TEXTURE;
    character[10][4]=TEXTURE;
    character[11][1]=TEXTURE;
    character[11][2]=TEXTURE;
    character[11][4]=TEXTURE;
    character[11][5]=TEXTURE;
}

/*
* Second frame of jumping. Cin Ali is started to jump
* Pre:  character[][WIDTH] should be initialized
*       global parameters are defined
* Post: character[][WIDTH] is modified
*/
void jump2(char character[][WIDTH])
{
    character[2][3]=TEXTURE;
    character[3][2]=TEXTURE;
    character[3][4]=TEXTURE;
    character[4][3]=TEXTURE;
    character[5][2]=TEXTURE;
    character[5][3]=TEXTURE;
    character[5][4]=TEXTURE;
    character[6][1]=TEXTURE;
    character[6][3]=TEXTURE;
    character[6][5]=TEXTURE;
    character[7][3]=TEXTURE;
    character[8][2]=TEXTURE;
    character[8][4]=TEXTURE;
    character[9][2]=TEXTURE;
    character[9][4]=TEXTURE;
    character[10][2]=TEXTURE;
    character[10][4]=TEXTURE;
    character[11][1]=TEXTURE;
    character[11][5]=TEXTURE;
}

/*
* Third and last frame of jumping. Cin Ali was jumped and at the top
* Pre:  character[][WIDTH] should be initialized
*       global parameters are defined
* Post: character[][WIDTH] is modified
*/
void jump3(char character[][WIDTH])
{
    character[0][3]=TEXTURE;
    character[1][2]=TEXTURE;
    character[1][4]=TEXTURE;
    character[2][3]=TEXTURE;
    character[3][2]=TEXTURE;
    character[3][3]=TEXTURE;
    character[3][4]=TEXTURE;
    character[4][2]=TEXTURE;
    character[4][3]=TEXTURE;
    character[4][4]=TEXTURE;
    character[5][3]=TEXTURE;
    character[6][2]=TEXTURE;
    character[6][4]=TEXTURE;
    character[7][2]=TEXTURE;
    character[7][4]=TEXTURE;
    character[8][2]=TEXTURE;
    character[8][4]=TEXTURE;
    character[9][2]=TEXTURE;
    character[9][4]=TEXTURE;
}


/*
* First frame of zeybek. Cin Ali raises his arms
* Pre:  character[][WIDTH] should be initialized
*       global parameters are defined
* Post: character[][WIDTH] is modified
*/
void zeybek0 (char character[][WIDTH])
{
    character[3][3]=TEXTURE;
    character[4][2]=TEXTURE;
    character[4][4]=TEXTURE;
    character[5][3]=TEXTURE;
    character[6][2]=TEXTURE;
    character[6][3]=TEXTURE;
    character[6][4]=TEXTURE;
    character[7][1]=TEXTURE;
    character[7][3]=TEXTURE;
    character[7][5]=TEXTURE;
    character[8][3]=TEXTURE;
    character[9][2]=TEXTURE;
    character[9][4]=TEXTURE;
    character[10][2]=TEXTURE;
    character[10][4]=TEXTURE;
    character[11][2]=TEXTURE;
    character[11][4]=TEXTURE;
}

/*
* Second frame of zeybek. Cin Ali preparing to turn right
* Pre:  character[][WIDTH] should be initialized
*       global parameters are defined
* Post: character[][WIDTH] is modified
*/
void zeybek1 (char character[][WIDTH])
{
    character[3][3]=TEXTURE;
    character[4][2]=TEXTURE;
    character[4][4]=TEXTURE;
    character[5][0]=TEXTURE;
    character[5][3]=TEXTURE;
    character[5][6]=TEXTURE;
    character[6][2]=TEXTURE;
    character[6][3]=TEXTURE;
    character[6][4]=TEXTURE;
    character[7][3]=TEXTURE;
    character[8][3]=TEXTURE;
    character[9][2]=TEXTURE;
    character[9][4]=TEXTURE;
    character[10][2]=TEXTURE;
    character[10][4]=TEXTURE;
    character[11][2]=TEXTURE;
}

/*
* Tihrd frame of zeybek. Cin Ali walking right when arms raised
* Pre:  character[][WIDTH] should be initialized
*       global parameters are defined
* Post: character[][WIDTH] is modified
*/
void zeybek2 (char character[][WIDTH])
{
    character[3][3]=TEXTURE;
    character[4][2]=TEXTURE;
    character[4][4]=TEXTURE;
    character[5][0]=TEXTURE;
    character[5][3]=TEXTURE;
    character[6][2]=TEXTURE;
    character[6][3]=TEXTURE;
    character[6][4]=TEXTURE;
    character[6][5]=TEXTURE;
    character[7][3]=TEXTURE;
    character[8][3]=TEXTURE;
    character[9][3]=TEXTURE;
    character[9][4]=TEXTURE;
    character[10][2]=TEXTURE;
    character[10][3]=TEXTURE;
    character[11][3]=TEXTURE;
}

/*
* Fouth frame of zeybek. Cin Ali takes a step to right and prepares to jump
* Pre:  character[][WIDTH] should be initialized
*       global parameters are defined
* Post: character[][WIDTH] is modified
*/
void zeybek3 (char character[][WIDTH])
{
    character[3][3]=TEXTURE;
    character[4][2]=TEXTURE;
    character[4][4]=TEXTURE;
    character[5][3]=TEXTURE;
    character[5][6]=TEXTURE;
    character[6][1]=TEXTURE;
    character[6][2]=TEXTURE;
    character[6][3]=TEXTURE;
    character[6][4]=TEXTURE;
    character[7][3]=TEXTURE;
    character[8][3]=TEXTURE;
    character[9][3]=TEXTURE;
    character[9][4]=TEXTURE;
    character[9][5]=TEXTURE;
    character[10][2]=TEXTURE;
    character[10][5]=TEXTURE;
    character[11][2]=TEXTURE;
}

/*
* Fifth frame of zeybek. Cin Ali starts to turning jump
* Pre:  character[][WIDTH] should be initialized
*       global parameters are defined
* Post: character[][WIDTH] is modified
*/
void zeybek4 (char character[][WIDTH])
{
    character[2][3]=TEXTURE;
    character[3][2]=TEXTURE;
    character[3][4]=TEXTURE;
    character[4][0]=TEXTURE;
    character[4][3]=TEXTURE;
    character[4][6]=TEXTURE;
    character[5][2]=TEXTURE;
    character[5][3]=TEXTURE;
    character[5][4]=TEXTURE;
    character[6][3]=TEXTURE;
    character[7][3]=TEXTURE;
    character[8][2]=TEXTURE;
    character[8][3]=TEXTURE;
    character[8][4]=TEXTURE;
    character[9][2]=TEXTURE;
    character[9][4]=TEXTURE;
    character[10][4]=TEXTURE;
}

/*
* Sixth frame of zeybek. Cin Ali reaches to top
* Pre:  character[][WIDTH] should be initialized
*       global parameters are defined
* Post: character[][WIDTH] is modified
*/
void zeybek5 (char character[][WIDTH])
{
    character[1][3]=TEXTURE;
    character[2][2]=TEXTURE;
    character[2][4]=TEXTURE;
    character[3][0]=TEXTURE;
    character[3][3]=TEXTURE;
    character[3][6]=TEXTURE;
    character[4][2]=TEXTURE;
    character[4][3]=TEXTURE;
    character[4][4]=TEXTURE;
    character[5][3]=TEXTURE;
    character[6][3]=TEXTURE;
    character[7][2]=TEXTURE;
    character[7][3]=TEXTURE;
    character[7][4]=TEXTURE;
    character[8][2]=TEXTURE;
    character[8][4]=TEXTURE;
    character[9][4]=TEXTURE;
}

/*
* Seventh frame of zeybek. Cin Ali ends to turning jump and starts fall
* Pre:  character[][WIDTH] should be initialized
*       global parameters are defined
* Post: character[][WIDTH] is modified
*/
void zeybek6 (char character[][WIDTH])
{
    character[2][3]=TEXTURE;
    character[3][2]=TEXTURE;
    character[3][4]=TEXTURE;
    character[4][0]=TEXTURE;
    character[4][3]=TEXTURE;
    character[4][6]=TEXTURE;
    character[5][2]=TEXTURE;
    character[5][3]=TEXTURE;
    character[5][4]=TEXTURE;
    character[6][3]=TEXTURE;
    character[7][3]=TEXTURE;
    character[8][2]=TEXTURE;
    character[8][3]=TEXTURE;
    character[8][4]=TEXTURE;
    character[9][2]=TEXTURE;
    character[9][4]=TEXTURE;
    character[10][2]=TEXTURE;
}

/*
* Eighth frame of zeybek. Cin Ali touches to ground again
* Pre:  character[][WIDTH] should be initialized
*       global parameters are defined
* Post: character[][WIDTH] is modified
*/
void zeybek7 (char character[][WIDTH])
{
    character[3][3]=TEXTURE;
    character[4][2]=TEXTURE;
    character[4][4]=TEXTURE;
    character[5][0]=TEXTURE;
    character[5][3]=TEXTURE;
    character[6][2]=TEXTURE;
    character[6][3]=TEXTURE;
    character[6][4]=TEXTURE;
    character[6][5]=TEXTURE;
    character[7][3]=TEXTURE;
    character[8][3]=TEXTURE;
    character[9][2]=TEXTURE;
    character[9][3]=TEXTURE;
    character[10][3]=TEXTURE;
    character[10][4]=TEXTURE;
    character[11][3]=TEXTURE;
}

/*
* Ninth frame of zeybek. Cin Ali turns to guests
* Pre:  character[][WIDTH] should be initialized
*       global parameters are defined
* Post: character[][WIDTH] is modified
*/
void zeybek8 (char character[][WIDTH])
{
    character[3][3]=TEXTURE;
    character[4][2]=TEXTURE;
    character[4][4]=TEXTURE;
    character[5][0]=TEXTURE;
    character[5][3]=TEXTURE;
    character[5][6]=TEXTURE;
    character[6][2]=TEXTURE;
    character[6][3]=TEXTURE;
    character[6][4]=TEXTURE;
    character[7][3]=TEXTURE;
    character[8][3]=TEXTURE;
    character[9][2]=TEXTURE;
    character[9][3]=TEXTURE;
    character[9][4]=TEXTURE;
    character[10][2]=TEXTURE;
    character[10][4]=TEXTURE;
    character[11][2]=TEXTURE;
    character[11][4]=TEXTURE;
}

/*
* Tenth frame of zeybek.
* Pre:  character[][WIDTH] should be initialized
*       global parameters are defined
* Post: character[][WIDTH] is modified
*/
void zeybek9 (char character[][WIDTH])
{
    character[3][3]=TEXTURE;
    character[4][2]=TEXTURE;
    character[4][4]=TEXTURE;
    character[5][0]=TEXTURE;
    character[5][3]=TEXTURE;
    character[5][6]=TEXTURE;
    character[6][2]=TEXTURE;
    character[6][3]=TEXTURE;
    character[6][4]=TEXTURE;
    character[7][3]=TEXTURE;
    character[8][3]=TEXTURE;
    character[9][2]=TEXTURE;
    character[9][3]=TEXTURE;
    character[10][3]=TEXTURE;
    character[10][4]=TEXTURE;
    character[11][3]=TEXTURE;
}

/*
* Eleventh frame of zeybek.
* Pre:  character[][WIDTH] should be initialized
*       global parameters are defined
* Post: character[][WIDTH] is modified
*/
void zeybek10 (char character[][WIDTH])
{
    character[3][3]=TEXTURE;
    character[4][2]=TEXTURE;
    character[4][4]=TEXTURE;
    character[5][0]=TEXTURE;
    character[5][3]=TEXTURE;
    character[5][6]=TEXTURE;
    character[6][2]=TEXTURE;
    character[6][3]=TEXTURE;
    character[6][4]=TEXTURE;
    character[7][3]=TEXTURE;
    character[8][3]=TEXTURE;
    character[9][2]=TEXTURE;
    character[9][3]=TEXTURE;
    character[10][3]=TEXTURE;
    character[11][3]=TEXTURE;
}

/*
* Twelveth frame of zeybek.
* Pre:  character[][WIDTH] should be initialized
*       global parameters are defined
* Post: character[][WIDTH] is modified
*/
void zeybek11 (char character[][WIDTH])
{
    character[4][3]=TEXTURE;
    character[5][2]=TEXTURE;
    character[5][4]=TEXTURE;
    character[6][3]=TEXTURE;
    character[7][2]=TEXTURE;
    character[7][3]=TEXTURE;
    character[7][4]=TEXTURE;
    character[8][1]=TEXTURE;
    character[8][3]=TEXTURE;
    character[9][2]=TEXTURE;
    character[9][3]=TEXTURE;
    character[10][2]=TEXTURE;
    character[10][3]=TEXTURE;
    character[11][3]=TEXTURE;
    character[11][4]=TEXTURE;
}

/*
* Thirteenth frame of zeybek.
* Pre:  character[][WIDTH] should be initialized
*       global parameters are defined
* Post: character[][WIDTH] is modified
*/
void zeybek12 (char character[][WIDTH])
{
    character[4][3]=TEXTURE;
    character[5][2]=TEXTURE;
    character[5][4]=TEXTURE;
    character[6][0]=TEXTURE;
    character[6][3]=TEXTURE;
    character[7][1]=TEXTURE;
    character[7][2]=TEXTURE;
    character[7][3]=TEXTURE;
    character[7][4]=TEXTURE;
    character[8][3]=TEXTURE;
    character[9][2]=TEXTURE;
    character[9][3]=TEXTURE;
    character[10][2]=TEXTURE;
    character[10][3]=TEXTURE;
    character[11][3]=TEXTURE;
    character[11][4]=TEXTURE;
}

/*
* Fourteenth frame of zeybek.
* Pre:  character[][WIDTH] should be initialized
*       global parameters are defined
* Post: character[][WIDTH] is modified
*/
void zeybek13 (char character[][WIDTH])
{
    character[4][3]=TEXTURE;
    character[5][1]=TEXTURE;
    character[5][2]=TEXTURE;
    character[5][4]=TEXTURE;
    character[6][1]=TEXTURE;
    character[6][3]=TEXTURE;
    character[7][1]=TEXTURE;
    character[7][2]=TEXTURE;
    character[7][3]=TEXTURE;
    character[7][4]=TEXTURE;
    character[8][3]=TEXTURE;
    character[9][2]=TEXTURE;
    character[9][3]=TEXTURE;
    character[10][2]=TEXTURE;
    character[10][3]=TEXTURE;
    character[11][3]=TEXTURE;
    character[11][4]=TEXTURE;
}

/*
* Fifteenth frame of zeybek.
* Pre:  character[][WIDTH] should be initialized
*       global parameters are defined
* Post: character[][WIDTH] is modified
*/
void zeybek14 (char character[][WIDTH])
{
    character[4][3]=TEXTURE;
    character[5][0]=TEXTURE;
    character[5][2]=TEXTURE;
    character[5][4]=TEXTURE;
    character[6][0]=TEXTURE;
    character[6][3]=TEXTURE;
    character[7][1]=TEXTURE;
    character[7][2]=TEXTURE;
    character[7][3]=TEXTURE;
    character[7][4]=TEXTURE;
    character[8][3]=TEXTURE;
    character[9][2]=TEXTURE;
    character[9][3]=TEXTURE;
    character[10][2]=TEXTURE;
    character[10][3]=TEXTURE;
    character[11][3]=TEXTURE;
    character[11][4]=TEXTURE;
}

/*
* Sixteenth frame of zeybek.
* Pre:  character[][WIDTH] should be initialized
*       global parameters are defined
* Post: character[][WIDTH] is modified
*/
void zeybek15 (char character[][WIDTH])
{
    character[3][3]=TEXTURE;
    character[4][2]=TEXTURE;
    character[4][4]=TEXTURE;
    character[5][0]=TEXTURE;
    character[5][3]=TEXTURE;
    character[5][6]=TEXTURE;
    character[6][2]=TEXTURE;
    character[6][3]=TEXTURE;
    character[6][4]=TEXTURE;
    character[7][3]=TEXTURE;
    character[8][3]=TEXTURE;
    character[9][3]=TEXTURE;
    character[9][4]=TEXTURE;
    character[10][3]=TEXTURE;
    character[11][3]=TEXTURE;
}
