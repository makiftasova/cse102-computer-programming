/* *********************************************************************** */
/*  FILENAME: HW04_111044016_part2.c                                       */
/*  Created on 16.04.2012 by Mehmet Akif TASOVA                            */
/*  ---------------------------------------------------------------------- */
/* This program can multiple and sum two polynomials                       */
/*    up to 8th degree                                                     */
/*    Program reads polynomial data from polynomial1.txt                   */
/*    and polynomial2.txt for each polynomial                              */
/*    Polynomials must be given in descending order by powers of x         */
/*                                                                         */
/* *********************************************************************** */

/* *********************************************************************** */
/*                             INCLUDES                                    */
/* *********************************************************************** */
#include <stdio.h> /* standart input-output library */
#include <math.h> /* definition of pow function */

/* *********************************************************************** */
/*                               DEFINITIONS                               */
/* *********************************************************************** */
 
/* maximum possible degree of given polynomial */
#define MAX_DEGREE 8

/* maximum kength of polynomial storage arrays */
#define MAX_LEN (MAX_DEGREE + 1)

/* maximum length for array which is storing product of polynomials */
#define MAX_PRODUCT_LEN ((2 * MAX_DEGREE) + 1)

/* a double number for evaluating function */
#define EVAL_X 2.25

/* name of first polynomial for printing it to screen */
#define FIRST_POL_NAME 'P'

/* name of second polynomial for printing it to screen */
#define SECOND_POL_NAME 'Q'

/* name of product of polynomials for printing it to screen */
#define PRODUCT_NAME 'R'

/* name of sum of polynomials for printing it to screen */
#define SUM_NAME 'T'

/* a near zero value for comparing doubles with zero */
#define FAKE_ZERO 0.00000000000000000000000000000001


/* *********************************************************************** */
/*                         FUNCTION PROTOTYPES                             */
/* *********************************************************************** */

/* Read coefficients from a given input file */
int getPoly(FILE * inputFile, double coeff[], int * degreep);

/* Evaluate a polynomial at a given value of x */
double evaluate(const double coeff[], int degree, double x);

/* Set all coeffients to zero */
int setToZero(double coeff[], int degree);

/* result = poly1 + poly2 */
double addPoly(const double poly1[], const double poly2[], 
                            double result[], int degreePoly1,
                            int degreePoly2, int *degreeResult);

/* result = poly1 * poly2 */
double multiplyPoly(const double poly1[], const double poly2[], 
                                double result[], int degreePoly1, 
                                int degreePoly2, int *degreeResult);

/* Print polynomial to screen ascending by degree*/
int printPolynomial(const double coeff[], int degree, char polyName);


/* Main Function Begin */
int main(void)
{
    /* arrays for stroing polynomials */
    double poly1[MAX_LEN], poly2[MAX_LEN]; 
    
    /* array for storing product of polynomials */
    double product[MAX_PRODUCT_LEN]; 
    
    double sum[MAX_LEN]; /* array for storing sum of polynomials */
    
    int poly1Degree, poly2Degree; /* storing for polynomial's degrees */
    
    int sumDegree; /* degree of sum of polynomials */
    
    int productDegree; /* degree of product of polynomials */
    
    FILE * polynomial1, * polynomial2; /* input files of polynoms */
    
    /* fill all arrays with zero */
    setToZero(poly1, MAX_LEN);
    setToZero(poly2, MAX_LEN);
    setToZero(sum, MAX_LEN);
    setToZero(product, MAX_PRODUCT_LEN);
    
    /* Read data of first polynomial and close input file */
    polynomial1 = fopen("polynomial1.txt", "r");
    getPoly(polynomial1, poly1, &poly1Degree);
    fclose(polynomial1);
    
    /* Print first polynomial and its degree */
    printf("First polynomial %c(x) is\n", FIRST_POL_NAME);
    printPolynomial(poly1, poly1Degree, FIRST_POL_NAME);
    printf("Degree of first polynomial: %d\n", poly1Degree);
    
    /* Evaluate and print first polynomial with EVAL_X value */
    printf("Evaluated %c(x) for x = %.3f:\n", FIRST_POL_NAME, EVAL_X);
    printf("%c(%.3f) = %.3f\n", FIRST_POL_NAME, EVAL_X, 
                                evaluate(poly1, poly1Degree, EVAL_X));
    
    printf("\n"); /* print an empty line */
    
    /* Read data of second polynomial and close input file */
    polynomial2 = fopen("polynomial2.txt", "r");
    getPoly(polynomial2, poly2, &poly2Degree);
    fclose(polynomial2);
    
    /* Print second polynomial and its degree */
    printf("Second polynomial %c(x) is\n", SECOND_POL_NAME);
    printPolynomial(poly2, poly2Degree, SECOND_POL_NAME);
    printf("Degree of second polynomial: %d\n", poly2Degree);
    
    /* Evaluate and print second polynomial with EVAL_X value */
    printf("Evaluated %c(x) for x = %.3f:\n", SECOND_POL_NAME, EVAL_X);
    printf("%c(%.3f) = %.3f\n", SECOND_POL_NAME, EVAL_X, 
                                evaluate(poly2, poly2Degree, EVAL_X));
    
    printf("\n"); /* print an empty line */
    
    /* Calcualte sum of polynomials */
    addPoly(poly1, poly2, sum, poly1Degree, poly2Degree, &sumDegree);
    printf("Calculating sum of polynomials....\n\n");
    
    /* Print sum of polynomials and its degree */
    printf("Sum of polynomials %c(x) is\n", SUM_NAME);
    printPolynomial(sum, sumDegree, SUM_NAME);
    printf("Degree of sum polynomials: %d\n",sumDegree);
    
    /* Evaluate and print sum of polynomials with EVAL_X value */
    printf("Evaluated %c(x) for x = %.3f:\n", SUM_NAME, EVAL_X);
    printf("%c(%.3f) = %.3f\n", SUM_NAME, EVAL_X, 
                                evaluate(sum, sumDegree, EVAL_X));
    
    printf("\n"); /* print an empty line */
    
    /*Calculate product of polynomials */
    multiplyPoly(poly1, poly2, product, 
                     poly1Degree, poly2Degree, &productDegree);
    printf("Calculating product of polynomials....\n\n");
    
    /* Print sproduct of polynomials and its degree */
    printf("Product of polynomials %c(x) is\n", PRODUCT_NAME);
    printPolynomial(product, productDegree, PRODUCT_NAME);
    printf("Degree of product of polynomials: %d\n",productDegree);
    
    /* Evaluate and print product of polynomials with EVAL_X value */
    printf("Evaluated %c(x) for x = %.3f:\n", PRODUCT_NAME, EVAL_X);
    printf("%c(%.3f) = %.3f\n", PRODUCT_NAME, EVAL_X, 
                                evaluate(product, productDegree, EVAL_X));
    
    printf("\n"); /* print an empty line */
    
    return 0; /* End of Program Execution */
}
/* Main Function End */

/* *********************************************************************** */
/*                          FUNCTION DEFINITIONS                           */
/* *********************************************************************** */

/* Read coefficients from a given input file */
int getPoly(FILE * inputFile, double coeff[], int * degreep)
{
    double temp[MAX_LEN]; /* Temp array for reading array */
    
    int i=0, j=0; /* Loop counters */
    
    int status, isEOF=1; /* variables for checking EOF status */
    /* If isEOF equals zero then loop will break */
    
    int degree; /* Local value for storing degree of polynomial */
    
    /* Read polynoninal */
    do{
        status = fscanf(inputFile, "%lf", &temp[i]);
        if(status == EOF)
        {
            isEOF = 0;
        };
        i += 1;
    }while(isEOF && i<MAX_LEN);
    
    *degreep = (i - 2); /* Return degree of polynomial */
    
    degree = (i - 2); /* Degree of polynomial for using in this function */
    
    /* Change array's order to ascending by degree */
    /* This part also returns polynomial to given array as output paramater */
    j = 0;
    for(i=8; i>=0; --i)
    {
        coeff[j - (8 - degree)] = temp[i];
        j += 1;
    };
    
    return 1; /* Return 1 if execution successfuly done */
}

/* Evaluate a polynomial at a given value of x */
double evaluate(const double coeff[], int degree, double x)
{
    double result=0.0; /* variable for storing result */
    
    int i=0; /* Loop counter */
    
    for(i=0;i<(degree+1);++i) /* Evaluate polynomial for given x value */
    {
        result += (coeff[i] * pow(x,i)); /* coeff * (x ^ i) */
    };
    
    return result; /* Return result to caller */
}

/* Set all coeffients to zero */
int setToZero(double coeff[], int degree)
{
    int i=0; /*Loop counter */
    
    for(i=0; i<(degree+1); ++i)
    {
        coeff[i] = 0.0; /* Assign 0.0 to each element of given array */
    };
    
    return 1; /* Return 1 if execution successfuly done */
}

/* result = poly1 + poly2 */
double addPoly(const double poly1[], const double poly2[], 
                            double result[], int degreePoly1,
                            int degreePoly2, int *degreeResult)
{
    int i=0; /* Loop counters */
    
    int maxDegree; /* Local variable for storing results degree */
    
    if(degreePoly1 >= degreePoly2) /* Decide degree of result */
    {
        maxDegree = degreePoly1;
    }
    else
    {
        maxDegree = degreePoly2;
    };
    
    for(i=0;i<(maxDegree+1);++i) /* Calculate sum of polynomials */
    {
        result[i] = (poly1[i] + poly2[i]);
    };
    
    *degreeResult = maxDegree; /* Return results degree as output parameter */
    
    return result[maxDegree]; /* Return coefficient of maximum degree */
}

/* result = poly1 * poly2 */
double multiplyPoly(const double poly1[], const double poly2[], 
                                double result[], int degreePoly1, 
                                int degreePoly2, int *degreeResult)
{
    int i=0, j=0; /* Variables for seeking in arrays */
    
    int maxDegree=0; /* Local variable for stroing results degree */
    
    /* Calculate product of given two polynomials */
    for(i=0;i<MAX_LEN;++i)
    {
        for(j=0;j<MAX_LEN;++j)
        {
            result[i+j] += (poly1[i] * poly2[j]);
            
            if((int)result[i+j] != 0)
            {
                maxDegree = (i + j);
            };
        };
    };
    
    *degreeResult = maxDegree; /* Return results degree */
    
    return result[maxDegree]; /* Return coefficient of maximum degree */
}

/* Print polynomial to screen ascending by degree*/
int printPolynomial(const double coeff[], int degree, char polyName)
{
    int i=0; /* Variable as loop counter */
    
    printf("%c(x) = ", polyName);/* Print the polynomial's name and variable */
    
    for(i=0;(i<degree+1);++i)
    {
        if(i == 0)
        {
            printf("%.3f ", coeff[i]);
        }
        else
        {
            if(coeff[i] > FAKE_ZERO)
            {
                printf("%.3fx^%d ", coeff[i], i);
            };
        };
        
        if(coeff[i+1] > FAKE_ZERO)
        {
            printf("+ ");
        };
    };
    
    printf("\n");
    
    return 1; /* Return 1 is execution was successful */
}


/* *********************************************************************** */
/*                         END OF HW04_111044016_part2.c                   */
/* *********************************************************************** */
