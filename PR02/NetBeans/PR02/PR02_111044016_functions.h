/* *********************************************************************** */
/*  FILENAME: PR02_111044016_functions.h                                   */
/*  Created on 01.06.2012 by Mehmet Akif TASOVA                            */
/*  ---------------------------------------------------------------------- */
/* Function Header file of Project 2                                       */
/*                                                                         */
/* *********************************************************************** */

/* *********************************************************************** */
/*                             INCLUDES                                    */
/* *********************************************************************** */
#include <stdio.h>
/* *********************************************************************** */
/*                               DEFINITIONS                               */
/* *********************************************************************** */

#define FALSE 0        /* FALSE sign */
#define TRUE 1         /* TRUE sign */

#define SPACE ' '
#define NULL_CHAR '\0'

#define HUM_NAME 40    /* Maximum name lenght of Homo sapiens */
#define CLI_NAME 50    /* Maximum name lenght of clinics */

#define DIAG_NAME 50    /* Name lenght of diagnosis */
#define MED_NAME 50     /* Given medicine's name array's lenght */

#define MAX_DOC_NUM 10  /* Maximum number of doctors per clinic */
#define MAX_PAT_NUM 150  /* Maximum number of patients per clinic */
#define MAX_CLI_NUM 20  /* Maximum number of clinics supported by program */

#define MAX_RES_NUM 15 /* Maximum number of reservations per doctor */

/* Database input/output files (binary format) */
#define DOCTOR_DB "doctors.bin"   /* Doctors database */
#define CLINIC_DB "clinics.bin"   /* Clinics database */
#define PATIENT_DB "patients.bin" /* Patients database */
#define RESERVATION_DB "reservations.bin" /* Reservations database */

/* Report files in text format */
#define DOCTOR_RP "doctors.txt"
#define CLINIC_RP "clinics.txt"
#define PATIENT_RP "patients.txt"
#define RESERVATION_RP "reservations.txt"

/* Command line Arguments */
#define VERSION_ARG "--version"
#define DEBUG_ARG "--debug"

/* *** Type Definations *** */

/* Date struct */
typedef struct{
                int day,
                    month,
                    year;
              } date_t;

/* Doctor's job condition type */
typedef enum {
                fired,
                vacation,
                working} cond_t;

/* Cure structure */
typedef struct {
                int patientId;             /* Patient id */
                char diagnosis[DIAG_NAME]; /* diagnosis of illness */
                char medicines[MED_NAME];  /* medicines for cure */
                } cure_t;

/* Doctor structure */
typedef struct {                                                     
                int dipNum;          /* Diploma Number of Doctor */
                char name[HUM_NAME]; /* Name of Doctor */
                int clinicID;        /* ID of doctor's clinic */
                cond_t condition;    /* Active/passive sign */
                int maxPatients;     /* Max number of acceptible patients */
                int currPatients;    /* Number of Curret patients */
                } doctor_t;

/* Patients structure */
typedef struct {
                int patientID;        /* patient ID (Patient's identity ID)*/
                char name[HUM_NAME];  /* Patient Name */
                unsigned long int phoneNo; /* Patient's phone number */
                int assignedClinicID; /* Id of assigned clinic */
                int assignedDocID;    /* Diploma number of Assigned Doctor */
                } patient_t;
                
/* Clinics structure */
typedef struct {
                int clinicID;        /* Clinic ID number */
                char name[CLI_NAME]; /* Clinic Name */
                doctor_t doctors[MAX_DOC_NUM];
                int currDocs;       /* Total number of current doctors */
                patient_t patients[MAX_PAT_NUM]; 
                int currPatients;    /* Number of Curret patients */
                } clinic_t;

/* Reservations structure */
typedef struct {
                int id;                 /* reservation id */
                int isFull;             /* if already reserved 1 else 0 */
                clinic_t clinic;       /* data of clinic */
                patient_t patient;     /* data of patient */
                doctor_t doctor;       /* data of doctor */                 
                date_t date;               /* visit date */
                } reservation_t;

/* Medical Exemination union */
typedef union {
                reservation_t reservation;
                cure_t cure;
                } med_exam_t;

/* *********************************************************************** */
/*                         FUNCTION PROTOTYPES                             */
/* *********************************************************************** */

/*** utility Functions ***/
           
/* Flush Buffer */
extern void flushBuffer(void);

/* pause program execution function */
extern void wait(double seconds);
                
/*** Command Line Arguments functions ***/

/* check for version argument "--version" */
extern int printVersion(int argc, char **argv);

/* check for debug mode argument "--debug" */
extern int debugMode(int argc, char **argv);
                
/*** date_t struct functions ***/

/* Inıtilise date by 01.01.2013 */
extern date_t initDate(date_t *date);

/* Checks and updates date. it will change month and year values if necessary */
extern date_t updateDate(date_t *date);

/* adds a day to current date */
extern date_t nextDay(date_t *date);

/* adds a month to current month */
extern date_t nextMonth(date_t *date);

/* adds a year to current date */
extern date_t nextYear(date_t *date);

/* adds days by given amount */
extern date_t addDays(date_t *date, int daysToAdd);

/* adds months by given amount */
extern date_t addMonths(date_t *date, int monthsToAdd);

/* adds years by given amount */
extern date_t addYears(date_t *date, int yearsToAdd);

/* prints date to screen as DD.MM.YYY */
extern void printDate(date_t date);

/*** Menu Instructions ***/
                
/* Print main menu function*/
extern void printMainMenu(date_t date);

/* clinic menu instructions function */
extern void printClinicMenu(date_t date);

/* doctor menu instructions function */
extern void printDoctorMenu(date_t date);

/* print doctor vacation/fire menu instructions */
extern void printDoctorVacMenu(date_t date);

/* Patient menu instructions */
extern void printPatientMenu(date_t date);

/* reservations menu instructions function */
extern void printReservMenu(date_t date);


/*** Struct Manipulators ***/

/* Clinic id finder function */
extern int getClinicId(char clinicName[CLI_NAME],
                        clinic_t arrayofClinics[], int maxClinicNumber);

/* Get clinic by clinics ID */
extern clinic_t getClinic(int clinicID, clinic_t clinics[], int size);
                        
/* Doctor id finder function */
extern int getDoctorId(char doctorName[HUM_NAME],
                        doctor_t arrayOfDoctors[], int maxDoctorNumber);

/* Find doctor's name by doctor's id */
extern char* getDoctorName(int doctorID,
                            doctor_t arrayOfDoctors[], int maxDoctorNumber);

/* prints data of all avaible clinics */
extern void printClinics(const clinic_t arrayOfClinics[], int maxClinicNumber);

/* prints data of a single clinic */
extern void printClinic(const clinic_t clinic);

/* prints doctors who works in a clinic */
extern void printDoctors(doctor_t arrayOfDoctors[], int maxDoctorNumber);

/* "print patients to screen" function */
extern void printPatients(patient_t arrayOfPatients[], int patArrSiz,
                                         clinic_t clinics[], int cliArrSiz);

/* Find doctor who cares about given patient */
extern doctor_t findDoctor(patient_t patient, clinic_t clinics[], int size);



/*** Binary File operations ***/

/* Write clinics to binary file*/
extern int writeClinics(FILE *file, const clinic_t clinics[], int size);

/* read clinics from binary file*/
extern int readClinics(FILE *file, clinic_t clinics[], int size);

/* Write doctors to binary file */
extern int writeDoctors(FILE *file, const doctor_t doctors[], int size);

/* read doctors from binary file */
extern int readDoctors(FILE *file, doctor_t doctors[], int size);

/* write patients to binary file */
extern int writePatients(FILE *file, const patient_t patients[], int size);

/* read patients from binary file */
extern int readPatients(FILE *file, patient_t patients[], int size);

/* Generates an empty doctor and returns it */
extern doctor_t emptyDoc(void);

/* Generates an empty patient and returns it */
extern patient_t emptyPat(void);

/* read clinic entry from user */
extern void addClinic(clinic_t clinics[], int *numOfClinics);

/* add a new doctor */
extern void addDoctor(doctor_t doctors[], int *numOfDocs);

/* edit doctors vacation/working/fired condition */
extern void editDoctor(doctor_t *doctor, cond_t toCondition);

/* adds a new patient */
extern void addPatient(patient_t patients[], int size);

/* get a reservation*/
extern void getReservation(reservation_t reservations[]);

/* *********************************************************************** */
/*                         END OF PR02_111044016_functions.h               */
/* *********************************************************************** */
