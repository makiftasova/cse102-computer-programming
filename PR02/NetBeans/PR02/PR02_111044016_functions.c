/* *********************************************************************** */
/*  FILENAME: PR02_111044016_functions.c                                   */
/*  Created on 01.06.2012 by Mehmet Akif TASOVA                            */
/*  ---------------------------------------------------------------------- */
/* Function Implementation file of Project 2                               */
/*                                                                         */
/* *********************************************************************** */

/* *********************************************************************** */
/*                             INCLUDES                                    */
/* *********************************************************************** */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#ifndef FUNCTIONS_H
#include "PR02_111044016_functions.h"
#define FUNCTIONS_H
#endif

/* *********************************************************************** */
/*                          FUNCTION DEFINITIONS                           */
/* *********************************************************************** */

/*** utility Functions ***/
           
/* Flush Buffer */
void flushBuffer(void)
{
    char junk;
    do{
        scanf("%c", &junk);
    } while(junk != '\n');
    
    return;
}

/* pause program execution function */
void wait(double seconds)
{
    clock_t endwait;
    
    endwait = clock () + seconds * CLOCKS_PER_SEC ;
    while (clock() < endwait);

}

/*** Command Line Arguments functions ***/

/* check for version argument "--version" */
int printVersion(int argc, char **argv)
{
    int i;
    
    for(i=0; i<argc; ++i){
        if(strcmp(argv[i], VERSION_ARG) == 0){
            printf("CSE 102 - Project 02\n");
            printf("release date: 13.06.2012\n");
            printf("Mehmet Akif TASOVA - 111044016\n");
            printf("Version: 0.1 BETA \n");
            printf("for more info: makiftasova@gmail.com\n");
            
            exit(0);
        };
    };
    
    return TRUE;
}

/* check for debug mode argument "--debug" */
int debugMode(int argc, char **argv)
{
    int i, returnValue=FALSE;
    
    for (i=0; i<argc; ++i){
        if(strcmp(argv[i], DEBUG_ARG) == 0){
            printf("*** ATTENTION: Debug mode is ON! ***\n\n");
            returnValue = TRUE;
        };
    };
    
    return returnValue;
}

/*** date_t struct functions ***/

/* Inıtilise date by 01.01.2013 */
date_t initDate(date_t *date)
{
    date->day = 1;
    date->month = 1;
    date->year = 2013;
    
    return *date;
}

/* Checks and updates date. it will change month and year values if necessary */
date_t updateDate(date_t *date)
{
    int monInc=0, yearInc=0; /* month and year incereasers */
    
    /* update day */
    while(date->day > 30){
        date->day -= 30;
        monInc += 1;
    };
    
    date->month += monInc; /* update month temporary */
    
    /* update month */
    while(date->month > 12){
        date->month -= 12;
        yearInc += 1;        
    };
    
    date->year += yearInc; /* update year */
    
    return *date;
    
    
}

/* adds a day to current date */
date_t nextDay(date_t *date)
{
    date->day += 1;
    
    updateDate(date);
    
    return *date;
}

/* adds a month to current month */
date_t nextMonth(date_t *date)
{
    date->month += 1;
    
    updateDate(date);
    
    return *date;
}

/* adds a year to current date */
date_t nextYear(date_t *date)
{
    date->year += 1;
    
    return *date;
}

/* adds days by given amount */
date_t addDays(date_t *date, int daysToAdd)
{
    date->day += daysToAdd;
    
    updateDate(date);
    
    return *date;
}

/* adds months by given amount */
date_t addMonths(date_t *date, int monthsToAdd)
{
    date->month += monthsToAdd;
    
    updateDate(date);
    
    return *date;
}

/* adds years by given amount */
date_t addYears(date_t *date, int yearsToAdd)
{
    date->year += yearsToAdd;
    
    return *date;
}

/* prints date to screen as DD.MM.YYY */
void printDate(date_t date)
{
        printf("Today is %d.%d.%d\n", date.day, date.month, date.year);
        
        return;
}


/* main menu instructions function*/
void printMainMenu(date_t date)
{
    printDate(date);    
    printf("[1] Clinic Operations\n");
    printf("[2] Doctor Operations\n");
    printf("[3] Patient Operations\n");
    printf("[4] Reservation Operations\n");
    printf("[5] Print all reports\n");
    printf("[6] Exit\n");

    return;
}

/* clinic menu instructions function */
void printClinicMenu(date_t date)
{
    printDate(date); 
    printf("[1] Add a clinic\n");
    printf("[2] Edit a clinic\n");
    printf("[3] Print Clinics report\n");

    return;
}

/* doctor menu instructions function */
void printDoctorMenu(date_t date)
{
    printDate(date); 
    printf("[1] Add a doctor\n");
    printf("[2] Edit doctor\n");
    printf("[3] Vacation/Fire Operations\n");
    printf("[4] Print Doctors Report\n");

    return;
}

/* print doctor vacation/fire menu instructions */
void printDoctorVacMenu(date_t date)
{
    printDate(date); 
    printf("[1] Set to Working\n");
    printf("[2] Set to On Vacation\n");
    printf("[3] Set to Fired\n");

    return;
}

/* Patient menu instructions */
void printPatientMenu(date_t date)
{
    printDate(date); 
    printf("[1] Add a patient\n");
    printf("[2] Edit a patient\n");
    printf("[3] Print patient report\n");

    return;
}

/* reservations menu instructions function */
void printReservMenu(date_t date)
{
    printDate(date); 
    printf("[1] Book a visit\n");
    printf("[2] Edit a reservation\n");
    printf("[3] Cancel a reservation\n");
    printf("[4] Print reservations report\n");

    return;
}

/* Clinic id finder function */
int getClinicId(char clinicName[CLI_NAME],
                clinic_t arrayofClinics[], int maxClinicNumber)
{
    int id=FALSE,
        count;

    for(count=0; count<maxClinicNumber; ++count){
        if(strcmp(clinicName, arrayofClinics[count].name) == 0){
            id = arrayofClinics[count].clinicID;
            break;
        };
    };

    return id;
}

/* Get clinic by clinics ID */
clinic_t getClinic(int clinicID, clinic_t clinics[], int size)
{
    clinic_t found;
    int i;
    
    for(i=0; (clinics[i].clinicID!=clinicID) && (i<size); ++i);
    
    found = clinics[i];
    
    return found;
}

/* Doctor id finder function */
int getDoctorId(char doctorName[HUM_NAME],
                doctor_t arrayOfDoctors[], int maxDoctorNumber)
{
    int id=FALSE,
         count;
    
    for(count=0; count<maxDoctorNumber; ++count){
        if(strcmp(doctorName, arrayOfDoctors[count].name) == 0){
            id = arrayOfDoctors[count].dipNum;
            break;
        } else {
            continue;
        };
    };

    return id;
}

/* Find doctor's name by doctor's id */
char* getDoctorName(int doctorID,
                            doctor_t arrayOfDoctors[], int maxDoctorNumber)
{
    int count;
    char* name=NULL;

    for(count=0; count<maxDoctorNumber; ++count){
        if(doctorID == arrayOfDoctors[count].dipNum){
            name = arrayOfDoctors[count].name;
            break;
        } else {
            continue;
        };
    };

    return name;
}

/* "print clinics to screen" function */
void printClinics(const clinic_t arrayOfClinics[], int size)
{
    int count, printlen;

    printlen = printf("Clinic Number%3cClinicID%3cClinic Name\n", SPACE, SPACE);
    for(count=0;count<(printlen-1);++count)
        printf("-");
    printf("\n");
    
    
    for(count=0; count<size; ++count){
        printf("%4c[%2d]%10c%d%9c%s\n", SPACE, (count+1),
                                        SPACE, arrayOfClinics[count].clinicID, 
                                        SPACE, arrayOfClinics[count].name);
    };

    return;
}

/* prints data of a single clinic */
void printClinic(const clinic_t clinic)
{
    int count, printlen;

    printlen = printf("ClinicID%3cClinic Name\n", SPACE);
    for(count=0;count<(printlen-1);++count)
        printf("-");
    printf("\n");
    
    printf("%4c%d%9c%s\n", SPACE, clinic.clinicID, 
                                    SPACE, clinic.name);
    
    return;
}

/* "print doctors to screen" function */
void printDoctors(doctor_t arrayOfDoctors[], int maxDoctorNumber)
{
    int count, printlen;

    printlen = printf("Doctor Number%3cDoctorID%3cDoctor Name%8cCondition\n",
                                                           SPACE, SPACE, SPACE);
    for(count=0;count<(printlen-1);++count)
        printf("-");
    printf("\n");
    
    for(count=0; count<maxDoctorNumber; ++count){
        printf("%4c[%2d]%12c%d%6c%s", SPACE, (count+1),
                                      SPACE, arrayOfDoctors[count].dipNum, 
                                      SPACE, arrayOfDoctors[count].name);
        
        if(arrayOfDoctors[count].condition == fired)
            printf("%9cFired\n", SPACE);
        else if (arrayOfDoctors[count].condition == vacation)
            printf("%9cOn Vacation\n", SPACE);
        else
            printf("%9cWorking\n", SPACE);
    };


    return;
}

/* "print patients to screen" function */
void printPatients(patient_t arrayOfPatients[], int patArrSiz,
                    clinic_t clinics[], int cliArrSiz)
{
    clinic_t clinic;
    doctor_t doctor;
    int clinicID, numOfDocs, docID;
    int printlen, printlen1, printlen2, i, j;
    char *docName, *clinicName;
    
    

    printlen1 = printf("Patient Number%cPatient ID%2cPatient Name",
                                                            SPACE, SPACE);
                                                            
    printlen2 = printf("%9cAssigned Doctor%11cClinic\n", SPACE, SPACE);

    printlen = printlen1 + printlen2;
    
    for(i=0;i<(printlen-1);++i)
        printf("-");
    printf("\n");
    
    for(i=0; i<patArrSiz; ++i){
        docID = arrayOfPatients[i].assignedDocID;
        clinicID = arrayOfPatients[i].assignedClinicID;
        clinic = getClinic(clinicID, clinics, cliArrSiz);
        docName = getDoctorName(docID, clinic.doctors, clinic.currDocs);
        clinicName = clinic.name;
                                                
        printf("%4c[%2d]%7c%5d%6c", SPACE, (i+1),
                    SPACE, arrayOfPatients[i].patientID, SPACE);

        printf("%-20s%c%-20s", arrayOfPatients[i].name, SPACE, docName);
        printf("%6c%-15s\n", SPACE, clinicName);
    };

    return;
}

/* Find doctor who cares about given patient */
doctor_t findDoctor(patient_t patient, clinic_t clinics[], int size)
{
    doctor_t found;
    int docIDtoFind, numOfDocs, i, j;
    
    
    docIDtoFind = patient.assignedDocID;
    
    for(i=0;i<size;++i){
        numOfDocs = clinics[i].currDocs;
        for(j=0; j<numOfDocs; ++j){
            if(docIDtoFind == clinics[i].doctors[j].dipNum)
                found = clinics[i].doctors[j];
            else
                continue;
        };
    };
    
    
    return found;
}

/*** Binary File operations ***/

/* Write clinics to binary file*/
int writeClinics(FILE *file, const clinic_t clinics[], int size)
{
    return fwrite(clinics, size, sizeof(clinic_t), file);
}

/* read clinics from binary file*/
int readClinics(FILE *file, clinic_t clinics[], int size)
{
     return fread(clinics, size, sizeof(clinic_t), file);
    
}

/* Write doctors to binary file */
int writeDoctors(FILE *file, const doctor_t doctors[], int size)
{
    return fwrite(doctors, size, sizeof(doctor_t), file);
}

/* read doctors from binary file */
int readDoctors(FILE *file, doctor_t doctors[], int size)
{
    return fread(doctors, sizeof(doctor_t), size, file);
}

/* write patients to binary file */
int writePatients(FILE *file, const patient_t patients[], int size)
{
    return fwrite(patients, size, sizeof(patient_t), file);
}

/* read patients from binary file */
int readPatients(FILE *file, patient_t patients[], int size)
{
    return fread(patients, sizeof(patient_t), size, file);
}

/* Generates an empty doctor and returns it */
doctor_t emptyDoc(void)
{
    doctor_t doc;
    
    doc.dipNum = 0;
    strcpy(doc.name, "");
    doc.clinicID = 0;
    doc.condition = fired;
    doc.maxPatients = 0;
    doc.currPatients = 0;
    
    return doc;
}

/* Generates an empty patient and returns it */
patient_t emptyPat(void){
    patient_t pat;
    
    pat.patientID = 0;
    strcpy(pat.name ,"");
    pat.phoneNo = 0;
    pat.assignedClinicID = 0;
    pat.assignedDocID = 0;
}

/* read clinic entry from user */
void addClinic(clinic_t clinics[], int *numOfClinics)
{
    char name[CLI_NAME], junk;
    int docNum, patNum;
    int i;
    
    if((*numOfClinics) < MAX_CLI_NUM){
        printf("Enter name of Clinic >>");
        gets(name);
        name[20] = NULL_CHAR;
        
        for(i=0; i<(*numOfClinics); ++i){
                if(strcmp(clinics[i].name, name) == 0){
                    printf("ERROR: Another Clinic Exist with same name!\n");
                    return;
                };
        };
    
        printf("Creating required files...\n");
    
        clinics[(*numOfClinics)].clinicID = (*numOfClinics);
        strcpy(clinics[(*numOfClinics)].name, name);
        clinics[(*numOfClinics)].doctors[1] = emptyDoc();
        clinics[(*numOfClinics)].currDocs = 0;
        clinics[(*numOfClinics)].patients[1] = emptyPat();
        clinics[(*numOfClinics)].currPatients = 0;
        
        (*numOfClinics) += 1;
        
        printf("Clinic Created Successfuly\n");
        
    } else {
        printf("Clinic Capacity of program is overloaded.\n");
        printf("Please Contact your IT Department.\n");
    }
    
    
    
    return;
}

/* add a new doctor */
void addDoctor(doctor_t doctors[], int *numOfDocs)
{
    int cond, i;
    
    char name[HUM_NAME];
    int dipNum, condition;
    
    if((*numOfDocs) < MAX_DOC_NUM){
        printf("Enter Diploma Number of Doctor>>");
        
        do{
            cond = scanf("%d", &dipNum);
            flushBuffer();
            if(cond != 1){
                printf("You entered an illegal diploma number\n");
                flushBuffer();
            };
        }while(cond != 1);
        
        for(i=0; i<(*numOfDocs); ++i){
            if(doctors[i].dipNum == dipNum){
                printf("ERROR: Another doctor exists with same diploma number\n");
                return;
            };
        };
        
        doctors[(*numOfDocs)].dipNum = dipNum;
        
        printf("Enter name of doctor >>");
        gets(name);
        
        strcpy(doctors[(*numOfDocs)].name, name);
        
        printf("Enter doctor's clinic ID>>");
        scanf("%d", &(doctors[(*numOfDocs)].clinicID));
        
        doctors[(*numOfDocs)].condition = working;
        
        doctors[(*numOfDocs)].maxPatients = MAX_RES_NUM;
        doctors[(*numOfDocs)].currPatients = 0;
        (*numOfDocs) += 1;
    } else {
        printf("Doctor Capacity of program is overloaded.\n");
        printf("Please Contact your IT Department.\n");
    };
    
    return;   
}

/* edit doctors vacation/working/fired condition */
void editDoctor(doctor_t *doctor, cond_t toCondition)
{
    doctor->condition = toCondition;
    
    return;
}

/* *********************************************************************** */
/*                         END OF PR02_111044016_functions.c               */
/* *********************************************************************** */
