/* *********************************************************************** */
/*  FILENAME: PR02_111044016_main.c                                        */
/*  Created on 01.06.2012 by Mehmet Akif TASOVA                            */
/*  ---------------------------------------------------------------------- */
/* Main function source code of Project 02 CSE 102                         */
/*                                                                         */
/* *********************************************************************** */

/* *********************************************************************** */
/*                             INCLUDES                                    */
/* *********************************************************************** */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifndef FUNCTIONS_H
#include "PR02_111044016_functions.h"
#define FUNCTIONS_H
#endif

/* *********************************************************************** */
/*                               DEFINITIONS                               */
/* *********************************************************************** */

/* *********************************************************************** */
/*                         FUNCTION PROTOTYPES                             */
/* *********************************************************************** */

/* Beginning of Main Function */
int main(int argc, char **argv)
{
    char* string = {"kbb"};

    clinic_t clinic[MAX_CLI_NUM], readCli[2], fnd;
    patient_t pat[2];
    date_t date;
    
    int numOfClinics=0;
    int isDebugMode=FALSE;
    int index, docNo, choice;
    cond_t condition;
    
    FILE *clinics;
    
    system("clear"); /* clear screen at startup */
    
    /*** Execution begins ***/
    
    /* if any command line arguments exists */
    if(argc > 1){
        isDebugMode = debugMode(argc, argv); /* check for "--debug" argument */
        printVersion(argc, argv); /* check for "--version" argument */
    };
    
    
    
    initDate(&date);
    if(isDebugMode)
        printf("DEBUG: Date initialised as 01.01.2013\n");
    
    
    /* test doctors */
    
    clinic[1].doctors[0].dipNum = 1;
    strcpy(clinic[1].doctors[0].name, "ali ahmet");
    clinic[1].doctors[0].clinicID = 1;
    clinic[1].doctors[0].condition = working;
    clinic[1].doctors[0].maxPatients = 1;
    clinic[1].doctors[0].currPatients = 1;
    
   

    clinic[0].doctors[0].dipNum = 2;
    strcpy(clinic[0].doctors[0].name, "hilmi uzunboyun");
    clinic[0].doctors[0].clinicID = 2;
    clinic[0].doctors[0].condition = working;
    clinic[0].doctors[0].maxPatients = 1;
    clinic[0].doctors[0].currPatients = 1;
    


    /* test clinics */

    clinic[0].clinicID = 1;
    strcpy(clinic[0].name, "dahiliye");
    clinic[0].currDocs = 1;
    clinic[0].currPatients = 1;

     numOfClinics += 1;
    
    clinic[1].clinicID = 2;
    strcpy(clinic[1].name, "kbb");
    clinic[1].currDocs = 1;
    clinic[1].currPatients = 1;

        numOfClinics += 1;
    
    /* test patients */

    pat[0].patientID = 321321;
    strcpy(pat[0].name, "john doe");
    pat[0].phoneNo = 6280900;
    pat[0].assignedDocID = clinic[0].doctors[0].dipNum;
    pat[0].assignedClinicID = 1;

    pat[1].patientID = 123123;
    strcpy(pat[1].name, "jane doe");
    pat[1].phoneNo = 4884200;
    pat[1].assignedDocID = clinic[1].doctors[0].dipNum;
    pat[1].assignedClinicID = 2;
    
    /* User Interface Part */
    do{
        printMainMenu(date);
        printf("Enter Your Choice>>");
        scanf("%d", &choice);
        system("clear");
        switch(choice){
            case 1:
                printClinicMenu(date);
                scanf("%d", &choice);
                system("clear");
                switch(choice){
                    case 1:
                        addClinic(clinic, &numOfClinics);
                        break;
                    default:
                        printf("This Part is under Construction\n");
                        printf("Returning Main Menu...\n");
                        wait(3);
                        break;
                };
                break;
            case 2:
                printDoctorMenu(date);
                scanf("%d", &choice);
                system("clear");
                switch(choice){
                    case 1:
                        printClinics(clinic, numOfClinics);
                        printf("Select clinic's number to add doctor>>");
                        scanf("%d", &index);
                        index -= 1;
                     
                        addDoctor(clinic[index].doctors, 
                                                &(clinic[index].currDocs));
                        break;
                    case 2:
                        printClinics(clinic, numOfClinics);
                        printf("Select clinic's number to edit its doctors>>");
                        scanf("%d", &index);
                        index -= 1;
                        
                        printDoctors(clinic[index].doctors, 
                                                        clinic[index].currDocs);
                        printf("Select dctor to edit>>");
                        scanf("%d", &docNo);
                        docNo -= 1;
                        printDoctorVacMenu(date);
                        printf("Enter your choice>>");
                        scanf("%d", &choice);
                        if(choice == 1)
                            condition = working;
                        else if(choice == 2)
                            condition = vacation;
                        else if(choice == 3)
                            condition = fired;
                        
                        editDoctor(&(clinic[index].doctors[docNo]), condition);
                        
                        break;
                        
                    default:
                        printf("This Part is under Construction\n");
                        printf("Returning Main Menu...\n");
                        wait(3);
                        break;
                };
                
            case 6:
                printf("Good Bye...\n");
                return 0; /* exit program */
                break;
                
            default:
                printf("This Part is under Construction\n");
                printf("Returning Main Menu...\n");
                wait(3);
                break;
        }
    }while(TRUE);
    
    
    
   /* 
    
    *** Remains of Test Part ***

    editDoctor(&(clinic[0].doctors[0]), vacation);
    printDoctors(clinic[0].doctors, clinic[0].currDocs);
    
    printClinics(clinic, numOfClinics);
    
    printDoctors(clinic[0].doctors, clinic[0].currDocs);
    printDoctors(clinic[1].doctors, clinic[1].currDocs);
    
    clinics = fopen(CLINIC_DB, "wb");
    writeClinics(clinics, clinic, numOfClinics);
    fclose(clinics);
    
    
    return 0;           */
}
/*End of Main Function*/

/* *********************************************************************** */
/*                          FUNCTION DEFINITIONS                           */
/* *********************************************************************** */


/* *********************************************************************** */
/*                         END OF PR02_111044016_main.c                    */
/* *********************************************************************** */
