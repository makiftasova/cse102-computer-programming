/* *********************************************************************** */
/*  FILENAME: PR02_111044016_functions.c                                   */
/*  Created on 21.05.2012 by Mehmet Akif TASOVA                            */
/*  ---------------------------------------------------------------------- */
/* Function Implementation file of Project 2                               */
/*                                                                         */
/* *********************************************************************** */

/* *********************************************************************** */
/*                             INCLUDES                                    */
/* *********************************************************************** */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#ifndef FUNCTIONS_H
#include "PR02_111044016_functions.h"
#define FUNCTIONS_H
#endif

/* *********************************************************************** */
/*                          FUNCTION DEFINITIONS                           */
/* *********************************************************************** */

/* main menu instructions function*/
void printMainMenu(void)
{
    printf("[1] Clinic Operations\n");
    printf("[2] Doctor Operations\n");
    printf("[3] Patient Operations\n");
    printf("[4] Reservation Operations\n");
    printf("[5] Print all reports\n");

    return;
}

/* clinic menu instructions function */
void printClinicMenu(void)
{
    printf("[1] Add a clinic\n");
    printf("[2] Edit a clinic\n");
    printf("[3] Print Clinics report\n");

    return;
}

/* doctor menu instructions function */
void printDoctorMenu(void)
{
    printf("[1] Add a doctor\n");
    printf("[2] Edit doctor\n");
    printf("[3] Vacation/Fire Operations\n");
    printf("[4] Print Doctors Report\n");

    return;
}

/* print doctor vacation/fire menu instructions */
void printDoctorVacMenu(void)
{
    printf("[1] Vacation Operations\n");
    printf("[2] Fire Operations\n");

    return;
}

/* Patient menu instructions */
void printPatientMenu(void)
{
    printf("[1] Add a patient\n");
    printf("[2] Edit a patient\n");
    printf("[3] Print patient report\n");

    return;
}

/* reservations menu instructions function */
void printReservMenu(void)
{
    printf("[1] Book a visit\n");
    printf("[2] Edit a reservation\n");
    printf("[3] Cancel a reservation\n");
    printf("[4] Print reservations report\n");

    return;
}

/* Clinic id finder function */
int getClinicId(char clinicName[CLI_NAME],
                clinic_t arrayofClinics[], int maxClinicNumber)
{
    int id=FALSE,
        count;

    for(count=0; count<maxClinicNumber; ++count){
        if(strcmp(clinicName, arrayofClinics[count].name) == 0){
            id = arrayofClinics[count].clinicID;
            break;
        };
    };

    return id;
}

/* Doctor id finder function */
int getDoctorId(char doctorName[HUM_NAME],
                doctor_t arrayOfDoctors[], int maxDoctorNumber)
{
    int id=FALSE,
         count;
    
    for(count=0; count<maxDoctorNumber; ++count){
        if(strcmp(doctorName, arrayOfDoctors[count].name) == 0){
            id = arrayOfDoctors[count].dipNum;
            break;
        } else {
            continue;
        };
    };

    return id;
}

/* Find doctor's name by doctor's id */
extern char* getDoctorName(int doctorID,
                            doctor_t arrayOfDoctors[], int maxDoctorNumber)
{
    int count;
    char* name=NULL;

    for(count=0; count<maxDoctorNumber; ++count){
        if(doctorID == arrayOfDoctors[count].dipNum){
            name = arrayOfDoctors[count].name;
            break;
        } else {
            continue;
        };
    };

    return name;
}

/* "print clinics to screen" function */
void printClinics(clinic_t arrayOfClinics[], int maxClinicNumber)
{
    int count, printlen;

    printlen = printf("Clinic Number%3cClinic Name\n", SPACE);
    for(count=0;count<(printlen-1);++count)
        printf("-");
    printf("\n");
    
    for(count=0; count<maxClinicNumber; ++count){
        printf("%4c[%2d]%10c%s\n", SPACE, (count+1),
                    SPACE, arrayOfClinics[count].name);
    };

    return;
}

/* "print doctors to screen" function */
void printDoctors(doctor_t arrayOfDoctors[], int maxDoctorNumber)
{
    int count, printlen;

    printlen = printf("Doctor Number%3cDoctor Name\n", SPACE);
    for(count=0;count<(printlen-1);++count)
        printf("-");
    printf("\n");
    
    for(count=0; count<maxDoctorNumber; ++count){
        printf("%4c[%2d]%10c%s\n", SPACE, (count+1),
                    SPACE, arrayOfDoctors[count].name);
    };


    return;
}

/* "print patients to screen" function */
void printPatients(patient_t arrayOfPatients[], int maxPatientNumber,
                    doctor_t doctors[], int maxDoctorNumber)
{
    int count, printlen, printlen1, printlen2;
    char *docName;

    printlen1 = printf("Patient Number%3cPatient ID%3cPatient Name",
                                                            SPACE, SPACE);
                                                            
    printlen2 = printf("%10cAssigned Doctor\n", SPACE);

    printlen = printlen1 + printlen2;
    
    for(count=0;count<(printlen-1);++count)
        printf("-");
    printf("\n");
    
    for(count=0; count<maxPatientNumber; ++count){
        docName = getDoctorName(arrayOfPatients[count].assignedDocID,
                                                doctors, maxDoctorNumber);
                                                
        printf("%4c[%2d]%10c%5d%6c", SPACE, (count+1),
                    SPACE, arrayOfPatients[count].patientID, SPACE);

        printf("%s%6c%13s\n", arrayOfPatients[count].name, SPACE, docName);
    };

    return;
}


/* *********************************************************************** */
/*                         END OF PR02_111044016_functions.c               */
/* *********************************************************************** */
