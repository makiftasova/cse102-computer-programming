/* *********************************************************************** */
/*  FILENAME: PR02_111044016_main.c                                        */
/*  Created on [CREATION DATE] by Mehmet Akif TASOVA                       */
/*  ---------------------------------------------------------------------- */
/* [INFORMATION ABOUT WHY YOU WROTE THIS CODE]                             */
/*                                                                         */
/*  ---------------------------------------------------------------------  */
/*  References                                                             */
/*                                                                         */
/*                                                                         */
/* *********************************************************************** */

/* *********************************************************************** */
/*                             INCLUDES                                    */
/* *********************************************************************** */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifndef FUNCTIONS_H
#include "PR02_111044016_functions.h"
#define FUNCTIONS_H
#endif

/* *********************************************************************** */
/*                               DEFINITIONS                               */
/* *********************************************************************** */

/* *********************************************************************** */
/*                         FUNCTION PROTOTYPES                             */
/* *********************************************************************** */

/* Beginning of Main Function */
int main(void)
{
    char* string = {"kbb"};

    clinic_t clinic[2];
    doctor_t doc[2];
    patient_t pat[2];

    /* test doctors */
    
    doc[0].dipNum = 1;
    strcpy(doc[0].name, "ali ahmet");
    doc[0].clinicID = 1;
    doc[0].condition = 1;
    doc[0].maxPatients = 1;
    doc[0].currPatients = 1;

    doc[1].dipNum = 2;
    strcpy(doc[1].name, "mahmut yuksel");
    doc[1].clinicID = 2;
    doc[1].condition = 1;
    doc[1].maxPatients = 1;
    doc[1].currPatients = 1;

    /* test clinics */

    clinic[0].clinicID= 1;
    strcpy(clinic[0].name, "dahiliye");
    clinic[0].numOfDocs = 1;
    clinic[0].maxPatients = 1;
    clinic[0].currPatients = 1;

    clinic[1].clinicID = 2;
    strcpy(clinic[1].name, "kbb");
    clinic[1].numOfDocs = 1;
    clinic[1].maxPatients = 1;
    clinic[1].currPatients = 1;

    /* test patients */

    pat[0].patientID = 321321;
    strcpy(pat[0].name, "zehra nur tasova");
    pat[0].assignedDocID = doc[1].dipNum;

    pat[1].patientID = 123123;
    strcpy(pat[1].name, "hesna tasova");
    pat[1].assignedDocID = doc[0].dipNum;


    /* driver part */

    printf("%d\n%d\n%d\n\n", fired, vacation, working);

    printMainMenu();
    printf("\n");
    printClinicMenu();
    printf("\n");
    printDoctorMenu();
    printf("\n");
    printPatientMenu();
    printf("\n");
    printReservMenu();
    printf("\n");
    system("clear");

    printf("doc name: %s\n",getDoctorName(2, doc, 2));

    printf("cli ID: %d\n", getClinicId(string, clinic, 2));

    printClinics(clinic, 2);

    printf("\n");

    printDoctors(doc, 2);

    printf("\n");

    printPatients(pat, 2, doc, 2);
    
    return 0;
}
/*End of Main Function*/

/* *********************************************************************** */
/*                          FUNCTION DEFINITIONS                           */
/* *********************************************************************** */


/* *********************************************************************** */
/*                         END OF PR02_111044016_main.c                    */
/* *********************************************************************** */
