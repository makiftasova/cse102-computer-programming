/* *********************************************************************** */
/*  FILENAME: PR02_111044016_functions.h                                   */
/*  Created on 21.05.2012 by Mehmet Akif TASOVA                            */
/*  ---------------------------------------------------------------------- */
/* Function Header file of Project 2                                       */
/*                                                                         */
/* *********************************************************************** */

/* *********************************************************************** */
/*                             INCLUDES                                    */
/* *********************************************************************** */
#include <stdio.h>
/* *********************************************************************** */
/*                               DEFINITIONS                               */
/* *********************************************************************** */

#define FALSE 0        /* FALSE sign */
#define TRUE 1         /* TRUE sign */

#define SPACE ' '
#define NULL_CHAR '\0'

#define HUM_NAME 40    /* Maximum name lenght of Homo sapiens */
#define CLI_NAME 50    /* Maximum name lenght of clinics */

#define DIAG_NAME 50    /* Name lenght of diagnosis */
#define MED_NAME 50     /* Given medicine's name array's lenght */

#define MAX_DOC_NUM 20  /* Maximum number of doctors per clinic */
#define MAX_CLI_NUM 30

#define MAX_RES_NUM 15 /* Maximum number of reservations per doctor */

/* Database input/output files (binary format) */
#define DOCTOR_DB "doctors.bin"   /* Doctors database */
#define CLINIC_DB "clinics.bin"   /* Clinics database */
#define PATIENT_DB "patients.bin" /* Patients database */
#define RESERVATION_DB "reservations.bin" /* Reservations database */

/* Report files in text format */
#define DOCTOR_RP "doctors.txt"
#define CLINIC_RP "clinics.txt"
#define PATIENT_RP "patients.txt"
#define RESERVATION_RP "reservations.txt"

/* *** Type Definations *** */

/* Doctor's job condition type */
typedef enum {
                fired,
                vacation,
                working} cond_t;

/* Cure structure */
typedef struct cure_s{
                int patientId;             /* Patient id */
                char diagnosis[DIAG_NAME]; /* diagnosis of illness */
                char medicines[MED_NAME];  /* medicines for cure */
                } cure_t;

/* Doctor structure */
typedef struct doctor_s{                                                     
                int dipNum;          /* Diploma Number of Doctor */
                char name[HUM_NAME]; /* Name of Doctor */
                int clinicID;        /* ID of doctor's clinic */
                cond_t condition;    /* Active/passive sign */
                int maxPatients;     /* Max number of acceptible patients */
                int currPatients;    /* Number of Curret patients */
                } doctor_t;

/* Clinix structure */
typedef struct clinic_s{
                int clinicID;        /* Clinic ID number */
                char name[CLI_NAME]; /* Clinic Name */
                doctor_t doctors[MAX_DOC_NUM];
                int numOfDocs;       /* Total number of current doctors */
                int maxPatients;     /* Max number of acceptible patients */
                int currPatients;    /* Number of Curret patients */
                } clinic_t;

/* Patients structure */
typedef struct patient_s {
                int patientID;      /* patient ID (Patient's identity ID)*/
                char name[HUM_NAME]; /* Patient Name */
                int assignedDocID;    /* Diploma number of Assigned Doctor */
                } patient_t;

/* Reservations structure */
typedef struct reservation_s{
                int id;                 /* reservation id */
                int isFull;             /* if already reserved 1 else 0 */
                clinic_t clinic;       /* data of clinic */
                patient_t patient;     /* data of patient */
                doctor_t doctor;       /* data of doctor */                 
                int date;               /* visit date */
                } reservation_t;

/* Medical Exemination union */
typedef union med_exam_u{
                reservation_t reservation;
                cure_t cure;
                } med_exam_t;

/* *********************************************************************** */
/*                         FUNCTION PROTOTYPES                             */
/* *********************************************************************** */

/* Print main menu function*/
extern void printMainMenu(void);

/* clinic menu instructions function */
extern void printClinicMenu(void);

/* doctor menu instructions function */
extern void printDoctorMenu(void);

/* print doctor vacation/fire menu instructions */
extern void printDoctorVacMenu(void);

/* Patient menu instructions */
extern void printPatientMenu(void);

/* reservations menu instructions function */
extern void printReservMenu(void);

/* Clinic id finder function */
extern int getClinicId(char clinicName[CLI_NAME],
                        clinic_t arrayofClinics[], int maxClinicNumber);
                        
/* Doctor id finder function */
extern int getDoctorId(char doctorName[HUM_NAME],
                        doctor_t arrayOfDoctors[], int maxDoctorNumber);

/* Find doctor's name by doctor's id */
extern char* getDoctorName(int doctorID,
                            doctor_t arrayOfDoctors[], int maxDoctorNumber);

/* "print Clinics to screen" function */
extern void printClinics(clinic_t arrayOfClinics[], int maxClinicNumber);

/* "print doctors to screen" function */
extern void printDoctors(doctor_t arrayOfDoctors[], int maxDoctorNumber);

/* "print patients to screen" function */
extern void printPatients(patient_t arrayOfPatients[], int maxPatientNumber,
                           doctor_t doctors[], int maxDoctorNumber);

/* *********************************************************************** */
/*                         END OF PR02_111044016_functions.h               */
/* *********************************************************************** */
