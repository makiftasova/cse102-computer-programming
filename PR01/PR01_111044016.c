/* *********************************************************************** */
/*  FILENAME: PR01_111044016.c                                             */
/*  Created on 23.03.2012 by Mehmet Akif TASOVA                            */
/*  ---------------------------------------------------------------------- */
/* This code is a game which like a Simple Financial Simulation            */
/*   and it is also a project for CSE102                                   */
/* *********************************************************************** */

/* *********************************************************************** */
/*                               INCLUDES                                  */
/* *********************************************************************** */
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
/* *********************************************************************** */
/*                               DEFINITIONS                               */
/* *********************************************************************** */
#define START_MONEY 10000.0
#define EXCHANGE_FEE 0.0004
#define START_CURRENCY  't'
#define EURO_SIGN       'e'
#define DOLLAR_SIGN     'd'
#define POUND_SIGN      'p'
/* *********************************************************************** */
/*                         FUNCTION PROTOTYPES                             */
/* *********************************************************************** */

/* Main Menu function */
int mainMenu(void);

/* Function for prointing general instructions to screen */
int printInstuctions(double balance);

/* Function of ingame menu */
char printMenu(void);

/*Buy Menu function */
char buy(void);

/* Function of advisor selection menu */
double selectAdvisor(double balance, double rate1, double rate1Old,
            double rate2, double rate2Old, double rate3, double rate3Old);

/* Fuction for passing header to reach rates */
int getHeaders(FILE *fileName); 

/* Function for readind day number from file and checking EOF status of file*/
int getDayNumber(FILE *fileName);

/* Function for getting exchange rates */
double getExchangeRate(FILE *fileName);

/* Returns an integer value in range [minimum,maximum) */
int getRandomInt(int minimum, int maximum); 

/*funtion for printing currenct balance to screen */
int printBalance(double balance, char currency);

/* Function of first advisor */
double mentorA(double balance, double rate1, double rate1Old,double rate2, 
                            double rate2Old, double rate3, double rate3Old);

/* Function of second advisor */
double mentorB(double balance, double rate1, double rate1Old,double rate2, 
                            double rate2Old, double rate3, double rate3Old);

/* Function of third advisor */
double mentorC(double balance, double rate1, double rate1Old,double rate2, 
                            double rate2Old, double rate3, double rate3Old);
                            

/* Function for converting currencies */
double exchangeMoney(double balanceTL, double euroRate, double dollarRate,
                        double poundRate, char destCurrency);

/* function for converting money into TL currency */
double calculateTLEquality(double balance, double euroRate, 
                        double dollarRate, double poundRate, char currency);

/* Simply calculates percentage profit after buying/selling */
double calculateProfit(double balance, double balanceOLD);

/*Function for calculating rates different from TL */
double convertRate(double currRate, double destRate);

/*Converts Exchange rates by given currencies and prints them to screen */
int convertAndPrint(double euroRate, double dollarRate, double poundRate,
                                                char destCurrency, int day);

/*Converts Exchange rates by given currencies and writes them into file */
int convertAndWrite(FILE *file, double euroRate, double dollarRate,
            double poundRate, char destCurrency, int day, double profit);

/* Function for writing investment report */
int writeReport(FILE *file, int day, double balance, double profit,
      double euroRate, double dollarRate, double poundRate, char currency);
                            

/* Function for printing investment report to screen */
int printReportToScreen(FILE *file);


/* Main Function Begin */

int main(void)
{
    mainMenu(); /* the good for everything function :) */
    return 0; /* end of program execution */
}

/*Main Function End*/


/*The mainMenu fınction. It nearly does everything about game */
int mainMenu(void)
{
    int day, dayOld; /* variables for storing integers about days */
    int isNextDay=0; /* variable for checking jump to next day */
    int i, j; /* junk integer values */
    
    /*variables for today's exchange rates */
    double euroRate=0, dollarRate=0, poundRate=0;
    
    /*variables for stroing balances */
    double balance = START_MONEY, balanceTL, balanceOLD = START_MONEY;
    
    /* variables for storing yesterday's exchange rates */
    double euroRateOld, dollarRateOld, poundRateOld;
    
    
    double convertFee;/* variables ofr storing convert fee */
    
    double profit; /* variable for percentage profit */
    
    /*char variables for storing choices which made by user */
    char buyChoice, choice='4';
    
    /* char variables for storing currecies */
    char currency = 't', currencyOLD;
        
    /* file pointer variables for using files */
    FILE *exchangesTable, *report;
    
    srand(time(NULL));/* set rand() function's seed */
    
    balanceTL = balance;
    
    printInstuctions(balance);
    
    /*open ExchangeRates.txt at readable mode */
    exchangesTable = fopen("ExchangeRates.txt", "r");
    
    /*open ExchangeRates.txt at wirteable mode */
    report = fopen("InvesmentReport.txt", "w");
    
    /* Header of investment report begin */
    i = fprintf(report, "Detailed Investment Report\n");
    for (j = 0; j < (i-1); ++j)
    {
        fprintf(report, "-");
    };
    fprintf(report, "\n");
    /* Header of investment report end */
    
    /* pass header of file to reach integer and double values */
    getHeaders(exchangesTable); 
    
    do{
        day = getDayNumber(exchangesTable); /* get day number */

    
        if(day != EOF)
        {
            
            if((day != 1) && (euroRate != 0) && 
                    (dollarRate != 0) && (poundRate != 0))
            {
                
                currencyOLD = currency;
                dayOld = day;
                euroRateOld = euroRate;
                dollarRateOld = dollarRate;
                poundRateOld = poundRate;
            };
            
            
            
                                   
            printf("\n");
            
            /* get exchange rates from ExchangeRates.txt */
            euroRate = getExchangeRate(exchangesTable);
            dollarRate = getExchangeRate(exchangesTable);
            poundRate = getExchangeRate(exchangesTable);
            
            
            
            printf("\n");
            
            if((day != 1) && (day != EOF))
            {
                do{
                    convertAndPrint(euroRateOld, dollarRateOld, poundRateOld,
                                                currency, day);
                    profit =  calculateProfit(balanceTL, START_MONEY);
                    printf("Your profit: %.2f%c \n", profit, '%');
                
                    choice = printMenu();
                
                    if(choice == '1') /* Get advises */
                    {
                        balanceTL = calculateTLEquality(balance, euroRate, 
                                            dollarRate, poundRate, currency);
                        isNextDay = 0;
                        balanceTL = selectAdvisor(balanceTL, euroRate,
                                       euroRateOld, dollarRate, dollarRateOld,
                                                poundRate, poundRateOld);
                        isNextDay = 0;
                        balance = exchangeMoney(balanceTL, euroRate, 
                                            dollarRate, poundRate, currency);
                        isNextDay = 0;
                        printBalance(balance, currency);
                        isNextDay = 0;
                    }
                    else if(choice == '2') /* Exchange money */
                    {
                        balanceOLD = balanceTL;
                        
                        /* Calculate convertion fee*/
                        convertFee = (balanceTL * EXCHANGE_FEE);
                        /* decrease balance by convert fee */
                        balanceTL -= convertFee;
                        
                        buyChoice = buy(); /* Get buy choice */
                        
                        switch(buyChoice)
                        {
                            case '1': /* Buy Euros */
                                currency = 'e';
                                balance = exchangeMoney(balanceTL, euroRate, 
                                            dollarRate, poundRate, currency);
                                break;
                            
                            case '2': /* Buy Dollars */
                                currency = 'd';
                                balance = exchangeMoney(balanceTL, euroRate, 
                                            dollarRate, poundRate, currency);
                                break;
                                
                            case '3': /* Buy Pounds */
                                currency = 'p';
                                balance = exchangeMoney(balanceTL, euroRate, 
                                            dollarRate, poundRate, currency);
                                break;
                                
                            case '4': /* Buy Turkish Liras */
                                currency = 't';
                                balance = exchangeMoney(balanceTL, euroRate, 
                                            dollarRate, poundRate, currency);
                                break;
                        };
                        balanceTL = calculateTLEquality(balance, euroRate, 
                                            dollarRate, poundRate, currency);
                        printBalance(balance, currency);
                        
                        writeReport(report, day, balance, profit, euroRateOld,
                        dollarRateOld, poundRateOld, currency);
                        isNextDay = 1;
                    }
                    else if(choice == '3') /* Keep Money */
                    {
                        printf("\nYou choosed \"keep money\"\n");
                        printf("Now jumping to next day\n");
                        
                        writeReport(report, day, balance, profit, euroRateOld,
                        dollarRateOld, poundRateOld, currency);
                        
                        printBalance(balance, currency);
                        isNextDay = 1;
                    }
                    else if(choice == '4') /* Rage Quit */
                    {
                        printf("\n\nNow exiting program...\n");
                        printf("Thanks for playing.\n");
                        printf("I hope you enjoyed well...\n");
                        printf("Good Bye...\n");
                        isNextDay = 0;
                        return 0;
                    };
                
                    
                }while(isNextDay != 1);
                
                
            };
        };
    }while(day != EOF);
    
    printf("\n\n\n\n\n"); /* Cleaning a little part of screen */
    
    
    
    fclose(exchangesTable);/* close the file which includes exchange rates */
    fclose(report); /* Close report file after report written */
    
    
    
    /*reopen report file for printing its includes to screen */
    report = fopen("InvesmentReport.txt", "r"); 
    
    /*Print report to screen */
    printReportToScreen(report);
    
    return 1; /* Returns 1 after succesful execution */
}


/* *********************************************************************** */
/*                          FUNCTION DEFINITIONS                           */
/* *********************************************************************** */
int printInstuctions(double balance)
{
    printf("Welcome to Financial Times...\n");
    printf("Your goal is earning money by changing ");
    printf("currency of yours into others\n");
    printf("You have %.0f TL as initial money... \n", balance);
    printf("You can want some help from advisors ");
    printf("before changing your currency \n");
    printf("If you think your current currency will win more than others,\n");
    printf("You can keep it. You don't need change currency every turn\n");
    return 1; /* return integer value 1 if execution successful */
}

/* Print the ingame main menu and returns user's choice from main menu */
char printMenu(void)
{
    char choice, junk;
    int isOK=1;
    do{
        printf("\n[1]Get Advise\n[2]Exchange money\n");
        printf("[3]Keep money\n[4]Exit game\n");
        printf("Enter your choice: \n");
        scanf("%c", &choice);
        scanf("%c", &junk);
        
        if((choice >= '1') && (choice <= '4'))
        {
            return choice;
            isOK = 1;
        }
        else
        {
            printf("You entered an invalid option\n");
            printf("Please check it\n\n");
            isOK = 0;
        };
    }while(isOK != 1);
    return choice;
}

/*Prints buy menu and returns user's choice from buy menu */
char buy(void)
{
    char choice, junk;
    int isOK;
    
    do{
        printf("\n[1]Buy Euros (€)\n[2]Buy Dollars ($)\n");
        printf("[3]Buy Pounds (£)\n[4]Buy Turkish Liras(TL)\n");
        printf("Enter your choice: ");
        scanf("%c", &choice);
        scanf("%c", &junk);
        
        /* Choice validity chechk */
        if((choice >= '1') || (choice <= '4'))
        {
            isOK = 1;
            return choice;
        }
        else
        {
            printf("You entered an invalid option, please try again\n");
            isOK = 0;
        };
    }while(isOK != 1);
}

/*Prints advisor selection menu and call advisor's functions
 * then returns player balance after advisor fee paid*/
double selectAdvisor(double balance, double rate1, double rate1Old,
            double rate2, double rate2Old, double rate3, double rate3Old)
{
    char choice, junk;
    double returnBalance;
    int len, count, isOK=1;
    
    
    
    do{
        
        printf("\n\n");
        printf("[A]Take advise from advisor A\n");
        printf("[B]Take advise from advisor B\n");
        printf("[C]Take advise from advisor C\n");
        printf("Enter your choice: ");
        scanf("%c", &choice);
        scanf("%c", &junk);
        
        printf("\n");
        
        if((choice == 'a') || (choice == 'A')) 
        /* call advisor A's function */
        {
            returnBalance = mentorA(balance, rate1, rate1Old, 
                                            rate2, rate2Old,
                                            rate3, rate3Old);
                                            isOK = 1;
        }
        else if((choice == 'b') || (choice == 'B')) 
        /* call advisor B's function */
        {
            returnBalance = mentorB(balance, rate1, rate1Old, 
                                            rate2, rate2Old,
                                            rate3, rate3Old);
                                            isOK = 1;
        }
        else if((choice == 'c') || (choice == 'C'))
        /* call advisor C's function */
        {
            returnBalance = mentorC(balance, rate1, rate1Old, 
                                            rate2, rate2Old,
                                            rate3, rate3Old);
                                            isOK = 1;
        }
        else
        {
            len = printf("Unrecognized input (%c) please check it\n", choice);
            
            for (count = 0; count < len; ++count)
            {
                printf("-");
            };
            
            isOK = 0;
        };
        printf("\n");
    }while(isOK != 1);

    
    return returnBalance;
}

/*Passes headers od ExchangeRates.txt file */
int getHeaders(FILE *fileName)
{
    char temp;
    
    do{
        fscanf(fileName, "%c", &temp);      
    }while(temp != '\n');
    
    return 1;
    /* function return integer value 1 if execution was successful */
}


/*Gets day number from ExchangeRates.txt. 
 * if day == EOF, return EOF else return day number*/
int getDayNumber(FILE *fileName)
{
    int day, isEOF;
    isEOF = fscanf(fileName, "%d", &day);
    if(isEOF != EOF)
    {
        return day;
    }
    else
    {
        return isEOF;
    };
}

/*gets exhange rates from ExchangeRates.txt file */
double getExchangeRate(FILE *fileName)
{
    double rate;
    fscanf(fileName, "%lf", &rate);
    return rate;
}


/*generates a random integer between given minimum and maximum numbers
 * and minimum and maximum numbers are included in return range */
int getRandomInt(int minimum, int maximum)
{
    int temp=0;
    

    if (minimum < maximum)
    {
        return (rand() % ((maximum - minimum + 1)) + minimum);
    }
    if(minimum > maximum)
    {
        temp = maximum;
        maximum = minimum;
        minimum = temp;
        return (rand() % ((maximum - minimum + 1)) + minimum);
    }
    else
    {
        printf("ERROR: Invalid parameters!!!\n");
        printf("Please check your parameters...\n");
        return -1;
    };
}

/* Prints users balance to screen */
int printBalance(double balance, char currency)
{
    switch(currency)
    {
        case 't':
            printf("You have %.4f TRL\n", balance);
            break;
        case 'e':
            printf("You have %.4f Euros(€)\n", balance);
            break;
        case 'd':
            printf("You have %.4f Dollars($)\n", balance);
            break;
        case 'p':
            printf("You have %.4f Pounds(£)\n", balance);
            break;
        default:
            printf("Unregocnized cureency(%c)", currency);
            break;
    }
    return 1;
}



/* Function for expects of advisor A. it returns balace after his fee paid */
double mentorA(double balance, double rate1, double rate1Old, double rate2, 
                            double rate2Old, double rate3, double rate3Old)
{
    int posibility, choice, price;
    
    double rateDiff1, rateDiff2, rateDiff3;
    
    posibility = getRandomInt(1,100);
    price = getRandomInt(6,40);
    
    /* difference between two days rate for currency1 */
    rateDiff1 = (rate1 - rate1Old);
    
    /* difference between two days rate for currency2 */
    rateDiff1 = (rate2 - rate2Old);
    
    /* difference between two days rate for currency3 */
    rateDiff1 = (rate3 - rate3Old);
    
    printf("You paid %d TL to advisor A for his advise\n", price);
    
    if(posibility <= 75)
    {
        if((rateDiff1 > 0) && (rateDiff2 > 0) && (rateDiff3 > 0))
        {
            choice = getRandomInt(1,3);
            switch(choice)
            {
                case 1:
                    printf("and he said \"You should buy €\"\n");
                    break;
                case 2:
                    printf("and he said \"You should buy $\"\n");
                    break;
                case 3:
                    printf("and he said \"You should buy £\"\n");
                    break;
            };
        }
        else if((rateDiff1 > 0) && (rateDiff2 > 0))
        {
            choice = getRandomInt(1,3);
            switch(choice)
            {
                case 1:
                    printf("and he said \"You should buy €\"\n");
                    break;
                case 2:
                    printf("and he said \"You should buy $\"\n");
                    break;
                case 3:
                    printf("and he said \"You should keep your money\"\n");
                    break;
              };
        }
        else if((rateDiff1 > 0) && (rateDiff3 > 0))
        {
            choice = getRandomInt(1,3);
            switch(choice)
            {
                case 1:
                    printf("and he said \"You should buy €\"\n");
                    break;
                case 2:
                    printf("and he said \"You should buy £\"\n");
                    break;
                case 3:
                    printf("and he said \"You should keep your money\"\n");
                    break;
              };
        }
        else if((rateDiff2 > 0) && (rateDiff3 > 0))
        {
            choice = getRandomInt(1,3);
            switch(choice)
            {
                case 1:
                    printf("and he said \"You should buy £\"\n");
                    break;
                case 2:
                    printf("and he said \"You should buy $\"\n");
                    break;
                case 3:
                    printf("and he said \"You should keep your money\"\n");
                    break;
              };
        }
        else if(rateDiff1 > 0)
        {
            choice = getRandomInt(1,2);
            switch(choice)
            {
                case 1:
                    printf("and he said \"You should buy €\"\n");
                    break;
                case 2:
                    printf("and he said \"You should keep your money\"\n");
                    break;
              };
        }
        else if(rateDiff2 > 0)
        {
            choice = getRandomInt(1,2);
            switch(choice)
            {
                case 1:
                    printf("and he said \"You should buy $\"\n");
                    break;
                case 2:
                    printf("and he said \"You should keep your money\"\n");
                    break;
              };
        }
        else if(rateDiff3 > 0)
        {
            choice = getRandomInt(1,2);
            switch(choice)
            {
                case 1:
                    printf("and he said \"You should buy £\"\n");
                    break;
                case 2:
                    printf("and he said \"You should keep your money\"\n");
                    break;
              };
        };
    }
    else
    {
        if((rateDiff1 > 0) && (rateDiff2 > 0) && (rateDiff3 > 0))
        {
            printf("and he said \"You should keep your money\"\n");
        }
        else if((rateDiff1 > 0) && (rateDiff2 > 0))
        {
            choice = getRandomInt(1,2);
            switch(choice)
            {
                case 1:
                    printf("and he said \"You should buy £\"\n");
                    break;
                case 2:
                    printf("and he said \"You should keep your money\"\n");
                    break;
              };
        }
        else if((rateDiff1 > 0) && (rateDiff3 > 0))
        {
            choice = getRandomInt(1,2);
            switch(choice)
            {
                case 1:
                    printf("and he said \"You should buy $\"\n");
                    break;
                case 2:
                    printf("and he said \"You should keep your money\"\n");
                    break;
              };
        }
        else if((rateDiff3 > 0) && (rateDiff2 > 0))
        {
            choice = getRandomInt(1,2);
            switch(choice)
            {
                case 1:
                    printf("and he said \"You should buy €\"\n");
                    break;
                case 2:
                    printf("and he said \"You should keep your money\"\n");
                    break;
              };
        }
        else if(rateDiff1 > 0)
        {
            choice = getRandomInt(1,3);
            switch(choice)
            {
                case 1:
                    printf("and he said \"You should buy $\"\n");
                    break;
                case 2:
                    printf("and he said \"You should buy £\"\n");
                    break;
                case 3:
                    printf("and he said \"You should keep your money\"\n");
                    break;
              };
        }
        else if(rateDiff2 > 0)
        {
            choice = getRandomInt(1,3);
            switch(choice)
            {
                case 1:
                    printf("and he said \"You should buy €\"\n");
                    break;
                case 2:
                    printf("and he said \"You should buy £\"\n");
                    break;
                case 3:
                    printf("and he said \"You should keep your money\"\n");
                    break;
              };
        }
        else if(rateDiff3 > 0)
        {
            choice = getRandomInt(1,3);
            switch(choice)
            {
                case 1:
                    printf("and he said \"You should buy $\"\n");
                    break;
                case 2:
                    printf("and he said \"You should buy €\"\n");
                    break;
                case 3:
                    printf("and he said \"You should keep your money\"\n");
                    break;
              };
        };
        
    };
    
    return (balance - price);
    
}

/* Function for expects of advisor B. it returns balace after his fee paid */
double mentorB(double balance, double rate1, double rate1Old, double rate2, 
                            double rate2Old, double rate3, double rate3Old)
{
    int posibility, choice, price;
    
    double rateDiff1, rateDiff2, rateDiff3;
    
    posibility = getRandomInt(1,100);
    price = getRandomInt(8,20);
    
    /* difference between two days rate for currency1 */
    rateDiff1 = (rate1 - rate1Old);
    
    /* difference between two days rate for currency2 */
    rateDiff1 = (rate2 - rate2Old);
    
    /* difference between two days rate for currency3 */
    rateDiff1 = (rate3 - rate3Old);
    
    printf("You paid %d TL to advisor B for his advise\n", price);
    
    if(posibility <= 50)
    {
        if((rateDiff1 > 0) && (rateDiff2 > 0) && (rateDiff3 > 0))
        {
            choice = getRandomInt(1,3);
            switch(choice)
            {
                case 1:
                    printf("and he said \"You should buy €\"\n");
                    break;
                case 2:
                    printf("and he said \"You should buy $\"\n");
                    break;
                case 3:
                    printf("and he said \"You should buy £\"\n");
                    break;
            };
        }
        else if((rateDiff1 > 0) && (rateDiff2 > 0))
        {
            choice = getRandomInt(1,3);
            switch(choice)
            {
                case 1:
                    printf("and he said \"You should buy €\"\n");
                    break;
                case 2:
                    printf("and he said \"You should buy $\"\n");
                    break;
                case 3:
                    printf("and he said \"You should keep your money\"\n");
                    break;
              };
        }
        else if((rateDiff1 > 0) && (rateDiff3 > 0))
        {
            choice = getRandomInt(1,3);
            switch(choice)
            {
                case 1:
                    printf("and he said \"You should buy €\"\n");
                    break;
                case 2:
                    printf("and he said \"You should buy £\"\n");
                    break;
                case 3:
                    printf("and he said \"You should keep your money\"\n");
                    break;
              };
        }
        else if((rateDiff2 > 0) && (rateDiff3 > 0))
        {
            choice = getRandomInt(1,3);
            switch(choice)
            {
                case 1:
                    printf("and he said \"You should buy £\"\n");
                    break;
                case 2:
                    printf("and he said \"You should buy $\"\n");
                    break;
                case 3:
                    printf("and he said \"You should keep your money\"\n");
                    break;
              };
        }
        else if(rateDiff1 > 0)
        {
            choice = getRandomInt(1,2);
            switch(choice)
            {
                case 1:
                    printf("and he said \"You should buy €\"\n");
                    break;
                case 2:
                    printf("and he said \"You should keep your money\"\n");
                    break;
              };
        }
        else if(rateDiff2 > 0)
        {
            choice = getRandomInt(1,2);
            switch(choice)
            {
                case 1:
                    printf("and he said \"You should buy $\"\n");
                    break;
                case 2:
                    printf("and he said \"You should keep your money\"\n");
                    break;
              };
        }
        else if(rateDiff3 > 0)
        {
            choice = getRandomInt(1,2);
            switch(choice)
            {
                case 1:
                    printf("and he said \"You should buy £\"\n");
                    break;
                case 2:
                    printf("and he said \"You should keep your money\"\n");
                    break;
              };
        };
    }
    else
    {
        if((rateDiff1 > 0) && (rateDiff2 > 0) && (rateDiff3 > 0))
        {
            printf("and he said \"You should keep your money\"\n");
        }
        else if((rateDiff1 > 0) && (rateDiff2 > 0))
        {
            choice = getRandomInt(1,2);
            switch(choice)
            {
                case 1:
                    printf("and he said \"You should buy £\"\n");
                    break;
                case 2:
                    printf("and he said \"You should keep your money\"\n");
                    break;
              };
        }
        else if((rateDiff1 > 0) && (rateDiff3 > 0))
        {
            choice = getRandomInt(1,2);
            switch(choice)
            {
                case 1:
                    printf("and he said \"You should buy $\"\n");
                    break;
                case 2:
                    printf("and he said \"You should keep your money\"\n");
                    break;
              };
        }
        else if((rateDiff3 > 0) && (rateDiff2 > 0))
        {
            choice = getRandomInt(1,2);
            switch(choice)
            {
                case 1:
                    printf("and he said \"You should buy €\"\n");
                    break;
                case 2:
                    printf("and he said \"You should keep your money\"\n");
                    break;
              };
        }
        else if(rateDiff1 > 0)
        {
            choice = getRandomInt(1,3);
            switch(choice)
            {
                case 1:
                    printf("and he said \"You should buy $\"\n");
                    break;
                case 2:
                    printf("and he said \"You should buy £\"\n");
                    break;
                case 3:
                    printf("and he said \"You should keep your money\"\n");
                    break;
              };
        }
        else if(rateDiff2 > 0)
        {
            choice = getRandomInt(1,3);
            switch(choice)
            {
                case 1:
                    printf("and he said \"You should buy €\"\n");
                    break;
                case 2:
                    printf("and he said \"You should buy £\"\n");
                    break;
                case 3:
                    printf("and he said \"You should keep your money\"\n");
                    break;
              };
        }
        else if(rateDiff3 > 0)
        {
            choice = getRandomInt(1,3);
            switch(choice)
            {
                case 1:
                    printf("and he said \"You should buy $\"\n");
                    break;
                case 2:
                    printf("and he said \"You should buy €\"\n");
                    break;
                case 3:
                    printf("and he said \"You should keep your money\"\n");
                    break;
              };
        };
        
    };
    
    return (balance - price);
    
}

/* Function for expects of advisor C. it returns balace after his fee paid */
double mentorC(double balance, double rate1, double rate1Old, double rate2, 
                            double rate2Old, double rate3, double rate3Old)
{
    int posibility, choice, price;
    
    double rateDiff1, rateDiff2, rateDiff3;
    
    posibility = getRandomInt(1,100);
    price = getRandomInt(5,15);
    
    /* difference between two days rate for currency1 */
    rateDiff1 = (rate1 - rate1Old);
    
    /* difference between two days rate for currency2 */
    rateDiff1 = (rate2 - rate2Old);
    
    /* difference between two days rate for currency3 */
    rateDiff1 = (rate3 - rate3Old);
    
    printf("You paid %d TL to advisor C for his advise\n", price);
    
    if(posibility <= 40)
    {
        if((rateDiff1 > 0) && (rateDiff2 > 0) && (rateDiff3 > 0))
        {
            choice = getRandomInt(1,3);
            switch(choice)
            {
                case 1:
                    printf("and he said \"You should buy €\"\n");
                    break;
                case 2:
                    printf("and he said \"You should buy $\"\n");
                    break;
                case 3:
                    printf("and he said \"You should buy £\"\n");
                    break;
            };
        }
        else if((rateDiff1 > 0) && (rateDiff2 > 0))
        {
            choice = getRandomInt(1,3);
            switch(choice)
            {
                case 1:
                    printf("and he said \"You should buy €\"\n");
                    break;
                case 2:
                    printf("and he said \"You should buy $\"\n");
                    break;
                case 3:
                    printf("and he said \"You should keep your money\"\n");
                    break;
              };
        }
        else if((rateDiff1 > 0) && (rateDiff3 > 0))
        {
            choice = getRandomInt(1,3);
            switch(choice)
            {
                case 1:
                    printf("and he said \"You should buy €\"\n");
                    break;
                case 2:
                    printf("and he said \"You should buy £\"\n");
                    break;
                case 3:
                    printf("and he said \"You should keep your money\"\n");
                    break;
              };
        }
        else if((rateDiff2 > 0) && (rateDiff3 > 0))
        {
            choice = getRandomInt(1,3);
            switch(choice)
            {
                case 1:
                    printf("and he said \"You should buy £\"\n");
                    break;
                case 2:
                    printf("and he said \"You should buy $\"\n");
                    break;
                case 3:
                    printf("and he said \"You should keep your money\"\n");
                    break;
              };
        }
        else if(rateDiff1 > 0)
        {
            choice = getRandomInt(1,2);
            switch(choice)
            {
                case 1:
                    printf("and he said \"You should buy €\"\n");
                    break;
                case 2:
                    printf("and he said \"You should keep your money\"\n");
                    break;
              };
        }
        else if(rateDiff2 > 0)
        {
            choice = getRandomInt(1,2);
            switch(choice)
            {
                case 1:
                    printf("and he said \"You should buy $\"\n");
                    break;
                case 2:
                    printf("and he said \"You should keep your money\"\n");
                    break;
              };
        }
        else if(rateDiff3 > 0)
        {
            choice = getRandomInt(1,2);
            switch(choice)
            {
                case 1:
                    printf("and he said \"You should buy £\"\n");
                    break;
                case 2:
                    printf("and he said \"You should keep your money\"\n");
                    break;
              };
        };
    }
    else
    {
        if((rateDiff1 > 0) && (rateDiff2 > 0) && (rateDiff3 > 0))
        {
            printf("and he said \"You should keep your money\"\n");
        }
        else if((rateDiff1 > 0) && (rateDiff2 > 0))
        {
            choice = getRandomInt(1,2);
            switch(choice)
            {
                case 1:
                    printf("and he said \"You should buy £\"\n");
                    break;
                case 2:
                    printf("and he said \"You should keep your money\"\n");
                    break;
              };
        }
        else if((rateDiff1 > 0) && (rateDiff3 > 0))
        {
            choice = getRandomInt(1,2);
            switch(choice)
            {
                case 1:
                    printf("and he said \"You should buy $\"\n");
                    break;
                case 2:
                    printf("and he said \"You should keep your money\"\n");
                    break;
              };
        }
        else if((rateDiff3 > 0) && (rateDiff2 > 0))
        {
            choice = getRandomInt(1,2);
            switch(choice)
            {
                case 1:
                    printf("and he said \"You should buy €\"\n");
                    break;
                case 2:
                    printf("and he said \"You should keep your money\"\n");
                    break;
              };
        }
        else if(rateDiff1 > 0)
        {
            choice = getRandomInt(1,3);
            switch(choice)
            {
                case 1:
                    printf("and he said \"You should buy $\"\n");
                    break;
                case 2:
                    printf("and he said \"You should buy £\"\n");
                    break;
                case 3:
                    printf("and he said \"You should keep your money\"\n");
                    break;
              };
        }
        else if(rateDiff2 > 0)
        {
            choice = getRandomInt(1,3);
            switch(choice)
            {
                case 1:
                    printf("and he said \"You should buy €\"\n");
                    break;
                case 2:
                    printf("and he said \"You should buy £\"\n");
                    break;
                case 3:
                    printf("and he said \"You should keep your money\"\n");
                    break;
              };
        }
        else if(rateDiff3 > 0)
        {
            choice = getRandomInt(1,3);
            switch(choice)
            {
                case 1:
                    printf("and he said \"You should buy $\"\n");
                    break;
                case 2:
                    printf("and he said \"You should buy €\"\n");
                    break;
                case 3:
                    printf("and he said \"You should keep your money\"\n");
                    break;
              };
        };
        
    };
    
    return (balance - price);
    
}

/* Function for exchanging money*/
double exchangeMoney(double balanceTL, double euroRate, double dollarRate,
                        double poundRate, char destCurrency)
{
    switch(destCurrency)
    {
        case 't': /* If currency if TL */
            return balanceTL;
            break;
        case 'e': /* If currency if Euro */
            return (balanceTL / euroRate);
            break;
        case 'd': /* If currency if Dollar */
            return (balanceTL / dollarRate);
            break;
        case 'p': /* If currency if Pound */
            return (balanceTL / poundRate);
            break;
        default: /* If given currency is not regocnized */
            printf("Unrecognized creency type (%c)\n", destCurrency);
            return -1;
            break;
    };
}


/* Calculates TL equality of given balance and currency */
double calculateTLEquality(double balance, double euroRate, 
                        double dollarRate, double poundRate, char currency)
{
    switch(currency)
    {
        case 'e': /* if destination currency is Euro */
            return (balance * euroRate);
            break;
        case 'd': /* if destination currency is Dollar */
            return (balance * dollarRate);
            break;
        case 'p': /* if destination currency is Pound */
            return (balance * poundRate);
            break;
        
       
        case 't': /* if destination currency is Turkish Lira */
            return balance;
            break;
        
        default: /* If destination currency is not regocnized */
            printf("Unregocnized cureency(%c)", currency);
            return -1; /* returns -1 if errors occured */
            break;
    }
}

/* Simply calculates percentage profit after buying/selling */
double calculateProfit(double balance, double balanceOLD)
{
    return (((balance / balanceOLD) -1 ) * 100);
}

/*Function for calculating rates different from TL */
double convertRate(double currRate, double destRate)
{
    return(currRate / destRate);
}

/*Converts Exchange rates by given currencies and prints them to screen */
int convertAndPrint(double euroRate, double dollarRate, double poundRate,
                                                char destCurrency, int day)
{
    double euro, dollar, pound, tl;
    
    if (destCurrency == 't')
    {
        printf("Day #%d\n", (day-1));
        printf("Yesterday's closing rates:\n");
        printf("Euro: \t%f\n", euroRate);
        printf("Dollar: %f\n", dollarRate);
        printf("Pound:  %f\n", poundRate);
        printf("Current Currecy :TL");
        printf("\n");
    };
    
    if(destCurrency == 'e')
    {
        tl = convertRate(1, euroRate);
        dollar = convertRate(dollarRate, euroRate);
        pound = convertRate(poundRate, euroRate);
        
        printf("Day #%d\n", (day-1));
        printf("Yesterday's closing rates:\n");
        printf("TL:\t%f\n", tl);
        printf("Dollar: %f\n", dollar);
        printf("Pound:\t%f\n", pound);
        printf("Current Currecy :€");
        printf("\n\n");
    }
    else if(destCurrency == 'd')
    {
        tl = convertRate(1, dollarRate);
        euro = convertRate(euroRate, dollarRate);
        pound = convertRate(poundRate, dollarRate);
        
        printf("Day #%d\n", (day-1));
        printf("Yesterday's closing rates:\n");
        printf("TL:\t%f\n", tl);
        printf("Euro: %f\n", euro);
        printf("Pound:\t%f\n", pound);
        printf("Current Currecy :$");
        printf("\n\n");
    }
    else if(destCurrency == 'p')
    {
        tl = convertRate(1, poundRate);
        euro = convertRate(euroRate, poundRate);
        dollar = convertRate(dollarRate, poundRate);
        
        printf("Day #%d\n", (day-1));
        printf("Yesterday's closing rates:\n");
        printf("TL:\t%f\n", tl);
        printf("Euro: %f\n", euro);
        printf("Dollar:\t%f\n", dollar);
        printf("Current Currecy :£");
        printf("\n\n");
    };
    
    return 1;
}

/*Converts Exchange rates by given currencies and writes them into file */
int convertAndWrite(FILE *file, double euroRate, double dollarRate,
            double poundRate, char destCurrency, int day, double profit)
{
    double euro, dollar, pound, tl;
    
    if (destCurrency == 't')
    {
        fprintf(file, "Day #%d\n", (day));
        fprintf(file, "Yesterday's closing rates:\n");
        fprintf(file, "Euro: \t%f\n", euroRate);
        fprintf(file, "Dollar: %f\n", dollarRate);
        fprintf(file, "Pound:  %f\n", poundRate);
        fprintf(file, "Current Currecy :TL\n");
        fprintf(file, "Your profit: %.2f%c \n", profit, '%');
        fprintf(file, "\n");
    };
    
    if(destCurrency == 'e')
    {
        tl = convertRate(1, euroRate);
        dollar = convertRate(dollarRate, euroRate);
        pound = convertRate(poundRate, euroRate);
        
        fprintf(file, "Day #%d\n", (day));
        fprintf(file, "Yesterday's closing rates:\n");
        fprintf(file, "TL:\t%f\n", tl);
        fprintf(file, "Dollar: %f\n", dollar);
        fprintf(file, "Pound:\t%f\n", pound);
        fprintf(file, "Current Currecy :€\n");
        fprintf(file, "Your profit: %.2f%c \n", profit, '%');
        fprintf(file, "n");
    }
    else if(destCurrency == 'd')
    {
        tl = convertRate(1, dollarRate);
        euro = convertRate(euroRate, dollarRate);
        pound = convertRate(poundRate, dollarRate);
        
        fprintf(file, "Day #%d\n", (day));
        fprintf(file, "Yesterday's closing rates:\n");
        fprintf(file, "TL:\t%f\n", tl);
        fprintf(file, "Euro: %f\n", euro);
        fprintf(file, "Pound:\t%f\n", pound);
        fprintf(file, "Current Currecy :$\n");
        fprintf(file, "Your profit: %.2f%c \n", profit, '%');
        fprintf(file, "\n");
    }
    else if(destCurrency == 'p')
    {
        tl = convertRate(1, poundRate);
        euro = convertRate(euroRate, poundRate);
        dollar = convertRate(dollarRate, poundRate);
        
        fprintf(file, "Day #%d\n", (day));
        fprintf(file, "Yesterday's closing rates:\n");
        fprintf(file, "TL:\t%f\n", tl);
        fprintf(file, "Euro: %f\n", euro);
        fprintf(file, "Dollar:\t%f\n", dollar);
        fprintf(file, "Current Currecy :£\n");
        fprintf(file, "Your profit: %.2f%c \n", profit, '%');
        fprintf(file, "\n");
    }
}

/* Writes detailed investment report into InvesmentReport.txt file */
int writeReport(FILE *file, int day, double balance, double profit,
       double euroRate, double dollarRate, double poundRate, char currency)
{
    double balanceTRL;
    int i, j;
    day -= 1;
    balanceTRL = calculateTLEquality(balance, euroRate, 
                                        dollarRate, poundRate, currency);
    
    i = fprintf(file, "Begining of Day #%d\n", day); /* Day's header*/
    for(j = 0; j < i; ++j)
    {
        fprintf(file, "-");
    };
    fprintf(file, "\n");
    fprintf(file, "Your balance at beginnig of the day: %.4f ", balance);
    if(currency == 'e')
    {
        fprintf(file, "Euros(€)\n");
        fprintf(file, "And it equals %.4f Turkish Liras(TL)\n", balanceTRL);
    }
    else if(currency == 'd')
    {
        fprintf(file, "Dollars($)\n");
        fprintf(file, "And it equals %.4f Turkish Liras(TL)\n", balanceTRL);
    }
    else if(currency == 'p')
    {
        fprintf(file, "Pounds(£)\n");
        fprintf(file, "And it equals %.4f Turkish Liras(TL)\n", balanceTRL);
    }
    else
        {fprintf(file, "Turkish Liras(TL)\n");};
    
    convertAndWrite(file, euroRate, dollarRate, 
                        poundRate, currency, day, profit);
    
    fprintf(file, "End of Day #%d\n", day);
    fprintf(file, "\n\n");
    
    
}

/*Prints investment report to screen */
int printReportToScreen(FILE *file)
{
    char courier;
    int isEOF;
    
    do{
        isEOF = fscanf(file, "%c", &courier);
        printf("%c", courier);
    }while(isEOF != EOF);
    
    return 1; /* returns 1 if execution succesful */
}
/* *********************************************************************** */
/*                         END OF PR01_111044016.c                         */
/* *********************************************************************** */
