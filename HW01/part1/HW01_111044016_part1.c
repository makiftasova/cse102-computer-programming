/* ************************************************************************** */
/*  FILENAME: HW01_111044016_part1.c                                          */
/*  Created on 1.03.2012 by Mehmet Akif TAŞOVA								  */
/*  ------------------------------------------------------------------------  */
/* This program calculates function f for every n between 1 and 10            */
/*       and a user defined value z. Function f is defined below              */
/*                                                                            */
/* f(z,n) = sqrt(z * ((1 - (n+1) * (z^n) + n * (z ^ (n+1)))                   */
/*                           / (1 - z) ^ 2) + e ^(-z * n))                    */
/*																			  */
/*  ------------------------------------------------------------------------  */
/*  References                                                                */
/*  1-) http://www.cs.cf.ac.uk/Dave/C/node17.html --> Usage of math.h file    */
/*                            				                                  */
/*  ------------------------------------------------------------------------  */
/* Extra Notes 																  */
/*	1-) Progam returns NaN(Not a Number) value in z greater or equal 2.2	  */
/*	2-) If 0 assings to variable z, program only returns infinite (inf)		  */
/* ************************************************************************** */

/* ************************************************************************** */
/*               				 INCLUDES    		                          */
/* ************************************************************************** */
#include <stdio.h>
#include <math.h>

/* ************************************************************************** */
/*                     		 	 DEFINITIONS  			   		              */
/* ************************************************************************** */
#define INCREASE_BY_ONE 1 /* Consant for incresing vaule of ant variable by 1 */
#define E 2.7182818284590452354 /*Mathematical constant number e */


int main(void)
{
    double z; /*this value will given by user */
     
    /* Variables for storing results of calculations */
    double result0, result1, result2, result3, result4, result5, result6, 
           result7, result8, result9;
           
    int n = 1, /* this will increase by 1 until it reaches 10 */
        counter = 1; /* counter to print results of calculations */
    
    
    /* Get vaule of z from user */
    printf("Enter the value of z: ");
    scanf("%lf", &z);
    
    /* Begin calcualate the results of the function F(z,n)  */
    /*    which defined in program description  */
    
    		/* calculate f(z, 1) */
    result0 = sqrt((z * ((1 - ((n + 1) * pow(z, n)) + (n * pow(z, n+1))) 
    			/ pow((1 - z), 2)) * exp((-1 * z) * n)));
    
    n += INCREASE_BY_ONE;	/* increase n by 1 to calculate f(z, 2) */
    
    		/* calculate f(z, 2) */
    result1 = sqrt((z * ((1 - ((n + 1) * pow(z, n)) + (n * pow(z, n+1))) 
    			/ pow((1 - z), 2)) * exp((-1 * z) * n)));
    
    n += INCREASE_BY_ONE;	/* increase n by 1 to calculate f(z, 3) */
    
    		/* calculate f(z, 3) */
    result2 = sqrt((z * ((1 - ((n + 1) * pow(z, n)) + (n * pow(z, n+1))) 
    			/ pow((1 - z), 2)) * exp((-1 * z) * n)));
    
    n += INCREASE_BY_ONE;	/* increase n by 1 to calculate f(z, 4) */
    
    		/* calculate f(z, 4) */
    result3 = sqrt((z * ((1 - ((n + 1) * pow(z, n)) + (n * pow(z, n+1))) 
    			/ pow((1 - z), 2)) * exp((-1 * z) * n)));
    
    n += INCREASE_BY_ONE;	/* increase n by 1 to calculate f(z, 5) */
    
    		/* calculate f(z, 5) */
    result4 = sqrt((z * ((1 - ((n + 1) * pow(z, n)) + (n * pow(z, n+1))) 
    			/ pow((1 - z), 2)) * exp((-1 * z) * n)));;
    
    n += INCREASE_BY_ONE;	/* increase n by 1 to calculate f(z, 6) */
    
    		/* calculate f(z, 6) */
    result5 = sqrt((z * ((1 - ((n + 1) * pow(z, n)) + (n * pow(z, n+1))) 
    			/ pow((1 - z), 2)) * exp((-1 * z) * n)));
    
    n += INCREASE_BY_ONE;	/* increase n by 1 to calculate f(z, 7) */
    
    		/* calculate f(z, 7) */
    result6 = sqrt((z * ((1 - ((n + 1) * pow(z, n)) + (n * pow(z, n+1))) 
    			/ pow((1 - z), 2)) * exp((-1 * z) * n)));
    
    n += INCREASE_BY_ONE;	/* increase n by 1 to calculate f(z, 8) */
    
    		/* calculate f(z, 8) */
    result7 = sqrt((z * ((1 - ((n + 1) * pow(z, n)) + (n * pow(z, n+1))) 
    			/ pow((1 - z), 2)) * exp((-1 * z) * n)));
    
    n += INCREASE_BY_ONE;	/* increase n by 1 to calculate f(z, 9) */
    
    		/* calculate f(z, 9) */
    result8 = sqrt((z * ((1 - ((n + 1) * pow(z, n)) + (n * pow(z, n+1))) 
    			/ pow((1 - z), 2)) * exp((-1 * z) * n)));
    
    n += INCREASE_BY_ONE;	/* increase n by 1 to calculate f(z, 10) */
    
    		/* calculate f(z, 10) */
    result9 = sqrt((z * ((1 - ((n + 1) * pow(z, n)) + (n * pow(z, n+1))) 
    			/ pow((1 - z), 2)) * exp((-1 * z) * n)));
    			
    /* End of calculations */
    
    /* Print results to screen. counter variable replaces with	*/
    /*		variable n for printinf f(z, n) to screen 			*/
    printf("\nf(%.4f, %d) = %.12f \n",z, counter, result0);
    counter += INCREASE_BY_ONE;
    
    printf("\nf(%.4f, %d) = %.12f \n",z, counter, result1);
    counter += INCREASE_BY_ONE;
    
    printf("\nf(%.4f, %d) = %.12f \n",z, counter, result2);
    counter += INCREASE_BY_ONE;
    
    printf("\nf(%.4f, %d) = %.12f \n",z, counter, result3);
    counter += INCREASE_BY_ONE;
    
    printf("\nf(%.4f, %d) = %.12f \n",z, counter, result4);
    counter += INCREASE_BY_ONE;
    
    printf("\nf(%.4f, %d) = %.12f \n",z, counter, result5);
    counter += INCREASE_BY_ONE;
    
    printf("\nf(%.4f, %d) = %.12f \n",z, counter, result6);
    counter += INCREASE_BY_ONE;
    
    printf("\nf(%.4f, %d) = %.12f \n",z, counter, result7);
    counter += INCREASE_BY_ONE;
    
    printf("\nf(%.4f, %d) = %.12f \n",z, counter, result8);
    counter += INCREASE_BY_ONE;
    
    printf("\nf(%.4f, %d) = %.12f \n",z, counter, result9);
    counter += INCREASE_BY_ONE;
    
    return 0;
}

/* ************************************************************************** */
/*                         END OF HW01_111044016_part1.c                      */
/* ************************************************************************** */
