/* ************************************************************************** */
/*  FILENAME: HW01_111044016_part2.c                                      	  */
/*  Created on 02.03.2012 by Mehmet Akif TASOVA                		 		  */
/*  ------------------------------------------------------------------------  */
/* This program calculates total cost of some houses for a period of 5 years  */
/*																			  */
/* ************************************************************************** */

/* ************************************************************************** */
/*               				 INCLUDES    		                          */
/* ************************************************************************** */
#include <stdio.h>

/* ************************************************************************** */
/*								   DEFINITIONS  							  */
/* ************************************************************************** */
#define MULTIPLE_BY_FIVE 5.0 /* Multiplier for calculating five years outgones */

/* ************************************************************************** */
/*                         FUNCTION PROTOTYPES	                              */
/* ************************************************************************** */
double totalCost(int initialHouseCost, int annualFuelCost, double taxRate);

int main(void)
{
    /* Variables for storing values for 1st house */
    double initialHouseCost1, annualFuelCost1, taxRate1;
    
    /* Variables for storing values for 2nd house */
    double initialHouseCost2, annualFuelCost2, taxRate2;
    
    /* Variables for storing values for 3rd house */
    double initialHouseCost3, annualFuelCost3, taxRate3;
    
    /* Variables for storing values for 4th house */
    double initialHouseCost4, annualFuelCost4, taxRate4;
    
    /* Variables for storing values for 5th house */
    double initialHouseCost5, annualFuelCost5, taxRate5;
    
    /* Variables for storing results of houses's totl outgone calculations */
    double totalCost1, totalCost2, totalCost3, totalCost4, totalCost5;
    
    FILE * inFile, * outFile;
    
    inFile = fopen("input.txt", "r");
    
    printf("\nImporting data from input.txt file...\n");
    
    fscanf(inFile, "%lf %lf %lf", &initialHouseCost1, &annualFuelCost1, &taxRate1);
    fscanf(inFile, "%lf %lf %lf", &initialHouseCost2, &annualFuelCost2, &taxRate2);
    fscanf(inFile, "%lf %lf %lf", &initialHouseCost3, &annualFuelCost3, &taxRate3);
    fscanf(inFile, "%lf %lf %lf", &initialHouseCost4, &annualFuelCost4, &taxRate4);
    fscanf(inFile, "%lf %lf %lf", &initialHouseCost5, &annualFuelCost5, &taxRate5);
    
    fclose(inFile);
    printf("All data imported successfuly!\n");
    printf("Starting calculations...\n");
    /* Start to calculate outgones for each house */
    totalCost1 = totalCost(initialHouseCost1, annualFuelCost1, taxRate1);
    totalCost2 = totalCost(initialHouseCost2, annualFuelCost2, taxRate2);
    totalCost3 = totalCost(initialHouseCost3, annualFuelCost3, taxRate3);
    totalCost4 = totalCost(initialHouseCost4, annualFuelCost4, taxRate4);
    totalCost5 = totalCost(initialHouseCost5, annualFuelCost5, taxRate5);
    
    printf("Writing results into output.txt file...\n");
    
    outFile = fopen("output.txt", "w"); /* Create output.txt file */
    
    fprintf(outFile, "Initial House Value\t\tAnnual Fuel Cost\tTax Rate\tTotal Cost\n");
    
    /* Write 1st houses calculations results into output.txt file */
    fprintf(outFile, "\t%9.0f \t\t\t\t%7.0f \t\t%6.2f \t\t%8.0f\n"
    		,initialHouseCost1,annualFuelCost1,taxRate1, totalCost1);
    
    /* Write 2nd houses calculations results into output.txt file */
    fprintf(outFile, "\t%9.0f \t\t\t\t%7.0f \t\t%6.2f \t\t%8.0f\n"
    		,initialHouseCost2,annualFuelCost2,taxRate2, totalCost2);
    
    /* Write 3rd houses calculations results into output.txt file */
    fprintf(outFile, "\t%9.0f \t\t\t\t%7.0f \t\t%7.3f \t%8.0f\n"
    		,initialHouseCost3,annualFuelCost3,taxRate3, totalCost3);
    
    /* Write 4th houses calculations results into output.txt file */
    fprintf(outFile, "\t%9.0f \t\t\t\t%7.0f \t\t%7.3f \t%8.0f\n"
    		,initialHouseCost4,annualFuelCost4,taxRate4, totalCost4);
    
    /* Write 5th houses calculations results into output.txt file */
    fprintf(outFile, "\t%9.0f \t\t\t\t%7.0f \t\t%6.2f \t\t%8.0f\n"
    		,initialHouseCost5,annualFuelCost5,taxRate5, totalCost5);
    
    fclose(outFile); /* Close the outputitxt fie preventing damages */
    
    printf("Writing results into output.txt is successfuly!\n\n");
    
    return 0;
}

/* ************************************************************************** */
/*                          FUNCTION DEFINITIONS                   		  	  */
/* ************************************************************************** */
double totalCost(int initialHouseCost, int annualFuelCost, double taxRate)
{
	double	totalTax,  /* Variable to store tax value for five years */
			totalFuelCost, /* Variable to store fuel cost for five years */
			totalOutgone; /* Variable to store total outgone and return value */
	
	totalTax = ((initialHouseCost * taxRate) * MULTIPLE_BY_FIVE);
	
	totalFuelCost = (annualFuelCost * MULTIPLE_BY_FIVE);
	
	totalOutgone = (totalTax + totalFuelCost + initialHouseCost);
	
	return totalOutgone;
}

/* ************************************************************************** */
/*                         END OF HW01_111044016_part2.c                      */
/* ************************************************************************** */
