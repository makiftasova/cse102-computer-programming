/*##########################################################################*/
/*                                                                          */
/* HW00_111044016_Part_2.c                                                  */
/* -----------------------                                         			*/
/* Created on 22.02.2012 by Mehmet Akif TAŞOVA								*/
/*Student Number: 111044016						                            */
/*                                                                          */
/* Description                                                              */
/* -----------------------                                         			*/
/*    The HW00_111044016_Part_2.c contains the program which calculates     */
/*    area of a rectangle if edge lengths are given for the second part     */
/*    of homework00.                                                        */
/*                                                                          */
/*##########################################################################*/

/*--------------------------------------------------------------------------*/
/*							  	   Includes		  						    */
/*--------------------------------------------------------------------------*/

#include <stdio.h>		/* standart input-output library */

/*###########################################################################*/
/*                                                                           */
/* int main()                                 						    	 */
/* -----------­­­­­­­­­­­                                                               */
/*   longEdgeLength - length of the long edge of the rectangle               */
/*   shortEdgeLength - length of the short edge of the rectangle             */
/*   areaOfRectangle - area of the rectangle            		             */
/*                    														 */
/*                                                                           */
/* Return                                                                    */
/* ­­­­­­-----------­­­­­­­­­­­                                                               */
/*     0 on success                                                          */
/*    			                                                             */
/*###########################################################################*/

int main(void)
{
	int longEdgeLength,  	/* length of the long edge of the rectangle */
		shortEdgeLength,	/* length of the short edge of the rectangle */
		areaOfRectangle;		/* area of the rectangle */
	
	/* get the long edge's length */	
	printf("Long edge's length: ");
	scanf("%d",&longEdgeLength);

	/* get the short edge's length */	
	printf("Short edge's length: ");
	scanf("%d",&shortEdgeLength);

	areaOfRectangle = longEdgeLength * shortEdgeLength;	/* compute the area */

	/* Display the area of the rectangle */
	printf("The area of the rectangle with edge lengths ");
	printf("%d, %d is %d.\n", longEdgeLength, shortEdgeLength, areaOfRectangle);
	
	return 0;

}

/*##########################################################################*/
/*						End of HW00_111044016_PART_1.c						*/
/*##########################################################################*/
