/* *********************************************************************** */
/*  FILENAME: HW03_111044016.c                                             */
/*  Created on 03.04.2012 by Mehmet Akif TASOVA                            */
/*  ---------------------------------------------------------------------- */
/* This code is implemention of Project 6.4 of                             */
/*    Program Solving and Program Desing in C Sixth Edition Page 355-356   */
/*    Pearson Education International, 2010                                */
/*    ISBN-13: 978-0-321-60151-3       ISBN-10: 0-321-60151-3              */
/*                                                                         */
/* *********************************************************************** */

/* *********************************************************************** */
/*                             INCLUDES                                    */
/* *********************************************************************** */
#include <stdio.h> /* Standart Input-Output Library */
#include <math.h> /* Definition of fabs function */


/* *********************************************************************** */
/*                               DEFINITIONS                               */
/* *********************************************************************** */

/* *********************************************************************** */
/*                         FUNCTION PROTOTYPES                             */
/* *********************************************************************** */

/* Displays the user menu, then inputs and returns as */
/*   the function value hte problem number seected    */
char getProblem(void);

/* Pormtps the user for the x-y coordinates of both points,                 */
/*    inputs the four coordinates, and returns them to the calling function */
/*    through output parameters                                             */
int get2_pt(double *x1, double *y1, double *x2, double *y2);

/* Pormpts the user for the slope and x-y coordinates of the point,     */
/*    inputs the three values and returns them to calling function      */
/*    through output paramaters                                         */
int get_pt_slope(double *x, double *y, double *m);

/* Takes four input parameters, the x-y coordinates of two points,           */
/*   and returns through output parameters the slope (m) and y-intercept (b) */
int slope_intcpt_from2_pt(double x1, double y1, double x2, double y2, 
                                                    double *m, double *b);

/* Takes three input parameters, the x-y coordinates of one point     */
/*   and the slope, and returns as the function value the y-intercept */
double intcpt_from_pt_slope (double x, double y, double m);

/* Takes four input parameters, the x-y coordinates of two points,   */
/*   and displays the two-point line equation with a heading         */
int display2_pt(double x1, double y1, double x2, double y2);

/* Takes three input parameters, the x-y coordinates of one point and the   */
/*   slope, and displays the point-slope line equation with a heading       */
int display_pt_slope(double x, double y, double m);

/* Takes two input parameters, the slope and y-intercept, and displays  */
/*   the slope-intercept line equation with a heading                   */
int display_slope_intcpt(double m, double b);

/* Main Function Begins */
int main(void)
{
    double x1, y1; /* variables for storing position of first point */
    
    double x2, y2; /* variables for storing position of second point */
    
    double m; /* variable for storing slope of line */
    
    double b; /* variable for stroing y-intercept of line */
    
    char choice; /* variable for stroing input method */
    
    char exitChoice; /* variable for eterminate ğrpgram control */
    
    char junk; /* variable for scanning junk char */
    
    int restart; /* variable for retarting program without exit */
    
    int isTrue; /* variables for storing bolean values */
    
    int isZeroDiv = 1; /* variabl for checking zero division error */
    
    /* get values and caculate part */
    do{
        choice = getProblem();
        switch(choice)
        {
            case '1': /* calculate by two-point form */
                get2_pt(&x1, &y1, &x2, &y2);
                isZeroDiv = slope_intcpt_from2_pt(x1,y1,x2,y2,&m,&b);
                if(isZeroDiv == 1)
                {
                    display2_pt(x1, y1, x2, y2);
                };
                break;
            case '2': /* calculate by slope-point form */
                get_pt_slope(&x1, &y1, &m);
                b = intcpt_from_pt_slope(x1, y1, m);
                isZeroDiv = 1;
                display_pt_slope(x1, y1, m);
                break;
            default:
                printf("Unrecognized Error(Error code: -1)\n");
                break;
        };
    
        if(isZeroDiv == 1)
        {
            display_slope_intcpt(m, b); /* display results to screen */
        };
        printf("\n\n\n");
        
        /* resart or exit part */
        do{
            printf("Do another conversion (Y or N): ");
            scanf("%c", &exitChoice);
            scanf("%c", &junk);
            
            switch(exitChoice)
            {
                case 'y':
                case 'Y':
                    restart = 1; /* restart program */
                    isTrue = 0; /* break asking loop */
                    break;
                
                case 'n':
                case 'N':
                    return 0; /* terminate program execution */
                    break;
                
                default:
                    printf("Unrecognized choice (%c). ", exitChoice);
                    printf("Please enter Y or N\n");
                    isTrue = 1; /* continue asking loop */
                    break;
            };
        }while(isTrue);
        
        
    }while(restart);
        return 0; /* terminate execution */
}
/* Main Function End */


/* *********************************************************************** */
/*                          FUNCTION DEFINITIONS                           */
/* *********************************************************************** */

/*Displays the user menu, then inputs and returns as */
/*     the function value hte problem number seected */
char getProblem(void)
{
    char choice; /* variable for toring user's choice */
    
    char junk; /* variable for reading junk char from inputs */
    
    int isDone=1; /* variable which using as loop breaker */
    
    int printedCharNum; /* stores number of printed chars */
    
    int i, j; /* junk variables ofr some useful jobs */
    
    do{
        i = printf("Select the form that you would like to convert to ");
        j = printf("slope-intercept form\n");
        printedCharNum = (i + j);
        for(i = 0; i < printedCharNum; ++i)
        {
            printf("-");
        };
        printf("\n");
        printf("[1]Two point form (You know two points on the line)\n");
        printf("[2]Point-slope form (You know the line's sope and one point)\n");
        printf("Enter your choice: ");
        scanf("%c", &choice);
        scanf("%c", &junk); /* reading junk \n char from inputs */
        
        if((choice == '1') || (choice == '2'))
        {
            isDone = 0;
        }
        else
        {
            isDone = 1;
        };
    }while(isDone);
    
    return choice; /* returns user's choice */
}

/* Pormtps the user for the x-y coordinates of both points,               */
/*   inputs the four coordinates, nd returns them to the calling function */
/*   through output parameters                                            */
int get2_pt(double *x1, double *y1, double *x2, double *y2)
{
    char junk; /* variable for reading junk char from inputs */
    
    char space; /* variable for reading space char as inputs */
    
    int firstDone=1, secondDone=1; /* variables which using as loop breaker */
    
    int scanCount1, scanCount2; /* validating for inputs */
    
    do{
        printf("Enter the x-y coordinates of ");
        printf("the first point sparated by a space: ");
        scanCount1 = scanf("%lf", x1);
        if(scanCount1 == 0)
        {
            scanf("%c",&junk);
        };
        
        scanf("%c", &space);
        
        scanCount2 = scanf("%lf", y1);
        if(scanCount2 == 0)
        {
            scanf("%c",&junk);
        };
        
        if((space == ' ') && (scanCount1 != 0) && (scanCount2 != 0 ))
        {
            firstDone = 0;
        };
        scanf("%c", &junk);
    }while(firstDone);
    
    do{
        printf("Enter the x-y coordinates of ");
        printf("the second point sparated by a space: ");
        scanCount1 = scanf("%lf", x2);
        if(scanCount1 == 0)
        {
            scanf("%c",&junk);
        };
        
        scanf("%c", &space);
        
        scanCount2 = scanf("%lf", y2);
        if(scanCount2 == 0)
        {
            scanf("%c",&junk);
        };
        
        if((space == ' ') && (scanCount1 != 0) && (scanCount2 != 0 ))
        {
            secondDone = 0;
        };
        scanf("%c", &junk);
    }while(secondDone);
    
    return 1; /* return 1 if execution successful */
}

/* Pormpts the user for the slope and x-y coordinates of the point,   */
/*   inputs the three values and returns them to calling function     */
/*   through output paramaters                                        */
int get_pt_slope(double *x, double *y, double *m)
{
    char junk; /* variable for reading junk char from inputs */
    
    char space; /* variable for reading space char as inputs */
    
    int firstDone=1; /* variables which using as loop breaker */
    
    int scanCount1, scanCount2; /* validating for inputs */
    
    do{
        printf("Enter the slope: ");
        scanCount1 = scanf("%lf", m);
        if(scanCount1 == 0)
        {
            scanf("%c", &junk);
        };
    }while((scanCount1 == 0) || (scanCount1 > 1));
                        
    do{
        printf("Enter the x-y coordinates of ");
        printf("the first point sparated by a space: ");
        scanCount1 = scanf("%lf", x);
        if(scanCount1 == 0)
        {
            scanf("%c",&junk);
        };
        
        scanf("%c", &space);
        
        scanCount2 = scanf("%lf", y);
        if(scanCount2 == 0)
        {
            scanf("%c",&junk);
        };
        
        if((space == ' ') && (scanCount1 != 0) && (scanCount2 != 0 ))
        {
            firstDone = 0;
        };
        scanf("%c", &junk);
    }while(firstDone);
    
    return 1; /* return 1 if execution successful */
}

/* Takes four input parameters, the x-y coordinates of two points,           */
/*   and returns through output parameters the slope (m) and y-intercept (b) */
int slope_intcpt_from2_pt(double x1, double y1, double x2, double y2, 
                                                    double *m, double *b)
{
    double slope; /* slope of the line */
    double intcpt; /* y-intercept point of the line */
    
    if((x2 - x1) != 0)
    {
        slope = ((y2 - y1) / (x2 - x1)); /* calculate the slope */
        
        intcpt = (y1 - (slope * x1)); /* calculate y-intercept */
        
        /* another way to calculate y-intercept  */
        /* intcpt = (y2 - (slope * x2));         */
        
        *b = intcpt; /* assign intcpt value to *b */ 
        printf("%f", intcpt);
        *m = slope; /* assign slope value to *m */
    }
    else
    {
        printf("ERROR: Zero division error (Error Code: -2 )\n");
        printf("Please Check your inputs...\n");
        return -2; /* return 0 if zero division error occurs */
    };
    
    return 1; /* return 1 if execution successful */
}

/* Takes three input parameters, the x-y coordinates of one point     */
/*   and the slope, and returns as the function value the y-intercept */
double intcpt_from_pt_slope (double x, double y, double m)
{
    double intcpt;
    
    intcpt = ((-1 * (m * x)) / y); /* calculate y-intercept */
    
    return intcpt; /* return y-intercept */
}

/* Takes four input parameters, the x-y coordinates of two points,   */
/*   and displays the two-point line equation with a heading         */
int display2_pt(double x1, double y1, double x2, double y2)
{
    int printedCharNum; /* stores number of printed chars */
    
    int printPart1, printPart2;
    
    int i; /* juck integer value for several jobs */
    
    /* clean screen for 3 lines */
    printf("\n\n\n");
    
    /* print header */
    printedCharNum = printf("   Two-point Form  \n");
    for(i = 0; i < printedCharNum; ++i)
    {
        printf("-");
    };
    printf("\n\n");
    
    /* print "(y2 - y1)" part */
    
    
    printPart1 = printf("    (%5.2f ", y2);
    
    if( y1 < 0)
    {
        y1 = fabs(y1);
        
        printPart2 = printf("+ %5.2f)\n", y1);
    }
    else
    {
        printPart2 = printf("- %5.2f)\n", y1);
    };
        
    /* print "m = ----------" part*/
    printedCharNum = printPart1 + printPart2;
    printf("m = ");
    for(i = 0; i < (printedCharNum - 4); ++i)
    {
        printf("-");
    };
    printf("\n");
    
    /* print "(x2 - x1)" part */
    printf("    (%5.2f ", x2);
    
    if( x1 < 0)
    {
        x1 = fabs(x1);
        
        printf("+ %5.2f)", x1);
    }
    else
    {
        printf("- %5.2f)", x1);
    };
    
    printf("\n\n");
    
    return 1; /* returns 1 if execution successful */
}

/* Takes three input parameters, the x-y coordinates of one point and the   */
/*   slope, and displays the point-slope line equation with a heading       */
int display_pt_slope(double x, double y, double m)
{
    int printedCharNum; /* stores number of printed chars */
    
    int i; /* juck integer value for several jobs */
    
    /* clean screen for 3 lines */
    printf("\n\n\n");
    
    /* print header */
    printedCharNum = printf("   Point-Slope Form  \n");
    for(i = 0; i < printedCharNum; ++i)
    {
        printf("-");
    };
    printf("\n\n");
    
    /* ** print point-slope form ** */
    
    /* print y related part */
    if(y < 0)
    {
        y = fabs(y);
        
        printf(" y + %5.2f = %5.2f",y ,m);
    }
    else if(y == 0)
    {
        printf("y = %5.2f",m);
    }
    else
    {
        printf("y - %5.2f = %5.2f", y, m);
    };
    
    /* print x realted part */
    if(x < 0)
    {
        x = fabs(x);
        
        printf("( x + %5.2f)", x);
    }
    else if(x == 0)
    {
        printf("x");
    }
    else
    {
        printf("( x - %5.2f)", x);
    };
    printf("\n\n");
    
    return 1; /* returns 1 if execution successful */
}

/* Takes two input parameters, the slope and y-intercept, and displays  */
/*   the slope-intercept line equation with a heading                   */
int display_slope_intcpt(double m, double b)
{
    int printedCharNum; /* stores number of printed chars */
    
    int i; /* juck integer value for several jobs */
    
    /* clean screen for 3 lines */
    printf("\n\n\n");
    
    /* print header */
    printedCharNum = printf("   Slope-Intercept Form  \n");
    for(i = 0; i < printedCharNum; ++i)
    {
        printf("-");
    };
    printf("\n\n");
    
    /* print Slope-Intercept */
    
    printf("  y ="); /* print "y =" part */
    
        /* print m related part */
    if(m > 0)
    {
        printf("%5.2fx ", m);
    }
    else if(m == 0)
    {
        printf(" ");
    }
    else
    {
        m = fabs(m);
        
        printf(" - %5.2fx ", m);
    };
    
        /*print b related part */
    if(b > 0)
    {
        printf("+ %5.2f\n\n", b);
    }
    else if(b == 0)
    {
        printf(" \n\n");
    }
    else
    {
        b = fabs(b);
        
        printf("- %5.2f\n\n", b);
    }
    
    return 1; /* returns 1 if execution successful */
}

/* *********************************************************************** */
/*                         END OF HW03_111044016.c                         */
/* *********************************************************************** */
