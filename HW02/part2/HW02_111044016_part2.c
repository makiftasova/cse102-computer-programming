/* ************************************************************************* */
/*  FILENAME: HW02_111044016_part2.c                                         */
/*  Created on 12.03.2012 by Mehmet Akif TASOVA                              */
/*  ------------------------------------------------------------------------ */
/* program to classify some given letters according to the English Alphabet. */
/* If a character which is not included in English alphabet is entered       */
/* by the user, program will produce an appropriate warning message.         */
/*                                                                           */
/* ************************************************************************* */
/* Extra Notes:                                                              */
/*                                                                           */
/* Every user defined function returns 1 if execution successful and         */
/* returns 0 if execution causes an error                                    */
/*                                                                           */
/* ************************************************************************* */

/* ************************************************************************* */
/*                               INCLUDES                                    */
/* ************************************************************************* */
#include <stdio.h>

/* ************************************************************************* */
/*                               DEFINITIONS                                 */
/* ************************************************************************* */
#define ASCII_TO_DECIMAL 48

/* ************************************************************************* */
/*                         FUNCTION PROTOTYPES                               */
/* ************************************************************************* */

/*The function which calls other functions and does computation*/
int mainMenu(int selection);

/* The function to read in the choice of the user */
int selectMetod(void);

/* If method's fuction */
int ifMethod(char chr);

/* Switch method's function */
int switchMethod(char chr);

/* Function Method's funnction */
int functionMethod(char chr);

/* ### Functions for deciding main groups ### */

/* if chr is in group0, returns 1, otherwise returns 0. */
int isGroup0(char chr);

/*if chr is in group1, returns 1, otherwise returns 0. */
int isGroup1(char chr);

/*if chr is in group2, returns 1, otherwise returns 0. */
int isGroup2(char chr);

/*if chr is in group3, returns 1, otherwise returns 0. */
int isGroup3(char chr);


/* ### main function begin ### */
int main(void)
{
    int selection=0, errorCondition;
    
    selection = selectMetod();
    
    if (selection != 0)
    {
        mainMenu(selection);
        return 1;
    }
    else
    {
        return 0;
    }
}
/* ### main function end ### */

/* ************************************************************************* */
/*                          FUNCTION DEFINITIONS                             */
/* ************************************************************************* */


int mainMenu(int selection)
{
    char voidchar; /* This just gets "\n" character from keyboard  */
                   /*            which causes an error             */
                   
    char chr; /* Varable to store character given by user */    
    int counter=0;/* counter for counting steps */
    
    if(selection < 0 || selection > 3)
        {
            printf("Illegal Choice, There is no option like this \n");
            return 1;
        };
    while(counter < 5)
    {
        printf("Eneter a character: ");
        scanf("%c%c",&voidchar, &chr);
    
        if(selection < 0 || selection > 3)
        {
            printf("Illegal Choice, There is no option like this \n");
            return 1;
        }
        else if (selection == 0)
        {
            printf("Program is terminted by user (Exit Code: 0) \n");
            return 0;
        }
        else if (selection == 1)
        {
            ifMethod(chr);
        }
        else if(selection == 2)
        {
            switchMethod(chr);
        }
        else if(selection == 3)
        {
            functionMethod(chr);
        };
        
        counter += 1;
    };
    return 1;
}

int selectMetod(void)
{
    char select='0'; /* Variable to store user's implementation method choice */
    int selection=0; /* Variable to return user's choice to caller fınction*/
    
    printf("Which implementation method do you prefer \n");
    printf("(1: if clauses, 2: switch clauses, 3:user-defined functions): ");
    scanf("%c",&select);
    
    if (select =='0')
    {
        printf("Program is terminted by user (Exit Code: 0) \n");
        return 0;
    }
    else
    {
        selection = (int) select;
        selection -= ASCII_TO_DECIMAL;
        return selection;
    };
}

int ifMethod(char chr)
{
    /* if chr equals to 0(zero) terminate execution */
    if (chr == '0')
    {
        printf("Program is terminted by user (Exit Code: 0) \n");
        return 0;
    }
    
    /* Group 0 characters */
    else if ((chr >= 'a' && chr <= 'g') || (chr >= 'A' &&  chr <= 'G'))
    {
        if ((chr >= 'a' && chr <= 'c') || (chr >= 'A' && chr <= 'C'))
        {
            printf("%c is in group 0.0 \n", chr);
        }
        else if ((chr >= 'd' && chr <= 'e') || (chr >= 'D' && chr <= 'E'))
        {
            printf("%c is in group 0.1 \n", chr);
        }
        else if ((chr >= 'f' && chr <= 'g') || (chr >= 'F' && chr <= 'G'))
        {
            printf("%c is in group 0.2 \n", chr);
        };
    }
    
    /* Group 1 characters */
    else if ((chr >= 'h' && chr <= 'm') || (chr >= 'H' &&  chr <= 'M'))
    {
        if ((chr >= 'h' && chr <= 'i') || (chr >= 'H' && chr <= 'I'))
        {
            printf("%c is in group 1.0 \n", chr);
        }
        else if ((chr >= 'j' && chr <= 'k') || (chr >= 'J' && chr <= 'K'))
        {
            printf("%c is in group 1.1 \n", chr);
        }
        else if ((chr >= 'l' && chr <= 'm') || (chr >= 'L' && chr <= 'M'))
        {
            printf("%c is in group 1.2 \n", chr);
        };
    }
    
    /* Group 2 characters */
    else if ((chr >= 'n' && chr <= 't') || (chr >= 'N' &&  chr <= 'T'))
    {
        if ((chr >= 'n' && chr <= 'o') || (chr >= 'N' && chr <= 'O'))
        {
            printf("%c is in group 2.0 \n", chr);
        }
        else if ((chr >= 'p' && chr <= 'r') || (chr >= 'P' && chr <= 'R'))
        {
            printf("%c is in group 2.1 \n", chr);
        }
        else if ((chr >= 's' && chr <= 't') || (chr >= 'S' && chr <= 'T'))
        {
            printf("%c is in group 2.2 \n", chr);
        };
    }
    
    /* Group 3 characters */
    else if ((chr >= 'u' && chr <= 'z') || (chr >= 'U' &&  chr <= 'Z'))
    {
        if ((chr >= 'u' && chr <= 'w') || (chr >= 'U' && chr <= 'W'))
        {
            printf("%c is in group 3.0 \n", chr);
        }
        else if ((chr >= 'x' && chr <= 'y') || (chr >= 'X' && chr <= 'Y'))
        {
            printf("%c is in group 3.1 \n", chr);
        }
        else if (chr >= 'z' || chr <= 'Z')
        {
            printf("%c is in group 3.2 \n", chr);
        };
    }
    
    /* If given character not recognized, call this part */
    else
    {
        printf("This character not recognized (%c) \n", chr);
    };
    return 1;
}

int switchMethod(char chr)
{
    switch (chr)
    {
        /* if chr equals to 0(zero) terminate execution */
        case '0':
            printf("Program is terminted by user (Exit Code: 0) \n");
            break;
        
        /* Group 0 characters */
            /* Group 0.0 characters */
        case 'a':
        case 'b':
        case 'c':
        case 'A':
        case 'B':
        case 'C':
            printf("%c is in group 0.0 \n", chr);
            break;
            /* Group 0.1 characters */
        case 'd':
        case 'e':
        case 'D':
        case 'E':
            printf("%c is in group 0.1 \n", chr);
            break;
            /* Group 0.2 characters */
        case 'f':
        case 'g':
        case 'F':
        case 'G':
            printf("%c is in group 0.2 \n", chr);
            break;
        
        /* Group 1 characters */
            /* Group 1.0 characters */
        case 'h':
        case 'i':
        case 'H':
        case 'I':
            printf("%c is in group 1.0 \n", chr);
            break;
            /* Group 1.1 characters */
        case 'j':
        case 'k':
        case 'J':
        case 'K':
            printf("%c is in group 1.1 \n", chr);
            break;
            /* Group 1.2 characters */
        case 'l':
        case 'm':
        case 'L':
        case 'M':
            printf("%c is in group 1.2 \n", chr);
            break;
        
        /* Group 2 characters */
            /* Group 2.0 characters */
        case 'n':
        case 'o':
        case 'N':
        case 'O':
            printf("%c is in group 2.0 \n", chr);
            break;
            /* Group 2.1 characters */
        case 'p':
        case 'q':
        case 'r':
        case 'P':
        case 'Q':
        case 'R':
            printf("%c is in group 2.1 \n", chr);
            break;
            /* Group 2.2 characters */
        case 's':
        case 't':
        case 'S':
        case 'T':
            printf("%c is in group 2.2 \n", chr);
            break;
        
        /* Group 3 characters */
            /* Group 3.0 characters */
        case 'u':
        case 'v':
        case 'w':
        case 'U':
        case 'V':
        case 'W':
            printf("%c is in group 3.0 \n", chr);
            break;
            /* Group 3.1 characters */
        case 'x':
        case 'y':
        case 'X':
        case 'Y':
            printf("%c is in group 3.1 \n", chr);
            break;
            /* Group 3.2 characters */
        case 'z':
        case 'Z':
            printf("%c is in group 3.2 \n", chr);
            break;
        
        /* If a not recognized given to function return part below */
        default :
            printf("This character not recognized (%c) \n", chr);
            break;
    }
    return 1;
}

int functionMethod(char chr)
{
    int isInGroup0=0, isInGroup1=0, isInGroup2=0, isInGroup3=0;
    
    /* if chr equals to 0(zero) terminate execution */
    if (chr == '0')
    {
        printf("Program is terminted by user (Exit Code: 0) \n");
        return 0;
    }
    
    isInGroup0 = isGroup0(chr);
    isInGroup1 = isGroup1(chr);
    isInGroup2 = isGroup2(chr);
    isInGroup3 = isGroup3(chr);
    
    if (isInGroup0 == 1)
    {
        printf("%c is in group 0 \n", chr);
        return 1;
    }
    else if (isInGroup1 == 1)
    {
        printf("%c is in group 1 \n", chr);
        return 1;
    }
    else if (isInGroup2 == 1)
    {
        printf("%c is in group 2 \n", chr);
        return 1;
    }
    else if (isInGroup3 == 1)
    {
        printf("%c is in group 3 \n", chr);
        return 1;
    };
}



int isGroup0(char chr)
{
    if ((chr >= 'a' && chr <= 'g') || (chr >= 'A' &&  chr <= 'G'))
    {
        return 1;
    }
    else
    {
        return 0;
    }
}

int isGroup1(char chr)
{
    if ((chr >= 'h' && chr <= 'm') || (chr >= 'H' &&  chr <= 'M'))
    {
        return 1;
    }
    else
    {
        return 0;
    }
}

int isGroup2(char chr)
{
    if ((chr >= 'n' && chr <= 't') || (chr >= 'N' &&  chr <= 'T'))
    {
        return 1;
    }
    else
    {
        return 0;
    }
}

int isGroup3(char chr)
{
    if ((chr >= 'u' && chr <= 'z') || (chr >= 'U' &&  chr <= 'Z'))
    {
        return 1;
    }
    else
    {
        return 0;
    }
}

/* ************************************************************************* */
/*                         END OF HW02_111044016_part2.c                     */
/* ************************************************************************* */
