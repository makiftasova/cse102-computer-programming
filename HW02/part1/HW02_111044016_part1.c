/* ************************************************************************* */
/*  FILENAME: HW02_111044016_part1.c                                  		 */
/*  Created on 11.03.2012 by Mehmet Akif TASOVA              		  		 */
/*  ------------------------------------------------------------------------ */
/* This program calculates function f for every n between 1 and 10           */
/*       and a user defined value z. Function f is defined below             */
/*                                                                           */
/* f(z,n) = sqrt(z * ((1 - (n+1) * (z^n) + n * (z ^ (n+1))) 				 */
/*							 / ((1 - z) ^ 2) + exp(-z * n))                  */
/* ************************************************************************* */

/* ************************************************************************* */
/*               				 INCLUDES    		                         */
/* ************************************************************************* */
#include <stdio.h> /* Standart input-output library */
#include <math.h> /* Definitions of "exp", pow" and "sqrt" functions */

/* ************************************************************************* */
/*                         FUNCTION PROTOTYPES	                             */
/* ************************************************************************* */
double calculateFunction(double z, int n); /* Function or calcualtions */


int main(void)
{
    double	z=0; /* Variable for the value z which one will taken from user */
    double	result=0; /* Variable to store result of calculation */
    
    int n=0; /* variable n which one will increase by 1 at every step */
    
    printf("Enter value of z: ");
    scanf("%lf", &z);
    printf("\n");
    
	if (z == 1)
	{
		printf("\a\a\a"); /*Beep three times cause of ZeroDivisionError */
		printf("If z equals to 0(zero), it will cause a Zero Division Error\n");
		printf("Program Execution will now terminated...\n");
		return 0;
	}
	else
	{
		n = 1;
		while(n < 11)
		{
			result = calculateFunction(z, n);
			printf("F(%.3f, %2d) = %24.12f \n",z, n, result);
			n += 1;
		};
	};
	
	return 0;
}

/* ************************************************************************* */
/*                          FUNCTION DEFINITIONS                   			 */
/* ************************************************************************* */
double calculateFunction(double z, int n)
{
	double part1, part2, part3, part4, part5;
	/* Local variables for making calculation easier */
	
	/* Start calulations */
	part1 = (1 - ((n + 1) * pow(z, n)) + (n * pow(z, n+1)));
	part2 = (part1 / pow((1 - z), 2));
	part3 = exp((-1 * z) * n);
	part4 = (z * part2) + part3;
	part5 = sqrt(part4);
	/* End of calculations */
	
	return part5; /* return final result to where function called */
}

/* ************************************************************************* */
/*                         END OF HW02_111044016_part1.c                  	 */
/* ************************************************************************* */
