/* *********************************************************************** */
/*  FILENAME: HW06_111044016_part2.c                                       */
/*  Created on 10.05.2012 by Mehmet Akif TASOVA                            */
/*  ---------------------------------------------------------------------- */
/* This program will solve a 8*8 maze with recursive method                */
/*      If fails prints "I failed"                                         */
/* This code is implemention of programming project 10.5 of                */
/*      Problem Solving And Program Design in C                            */
/*                                                                         */
/* *********************************************************************** */

/* *********************************************************************** */
/*                             INCLUDES                                    */
/* *********************************************************************** */
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

/* *********************************************************************** */
/*                               DEFINITIONS                               */
/* *********************************************************************** */

#define TRUE 1         /* Boolean value TRUE as integer */
#define FALSE 0        /* Boolean value FALSE as integer */

#define ROWS 8          /* Total number of rows of maze */
#define COLS 8          /* Total number of columns of maze */

#define START_SIGN 'S' /* Sign of start point */
#define END_SIGN 'E'
#define PATH ' '        /* Walking path */
#define WALL 'X'        /* Can't crossable wall */
#define BLOCKED '#'     /* Dead path */
#define PATH_SIGN '*'   /* The path leads to glory */
#define WAY_OUT '>'     /* Exit sign */

/* Start point coordinates */
#define START_X 0       /* X coordinate of start point of maze */
#define START_Y 1       /* Y coordinate of start point of maze */

/* End point coordinates */
#define END_X 7         /* X coordinate of end point of maze */
#define END_Y 7         /* Y coordinate of end point of maze */

#define WAIT_TIME 0.3   /* Wait time between frames             */
#define INIT_WAIT 2.0   /* Wait time at program initialization  */
#define NO_WAIT 0.0     /* Why you still waiting?               */
#define STEP_BY_STEP 1  /* Solve Step-By-Step switch            */

/* *********************************************************************** */
/*                         FUNCTION PROTOTYPES                             */
/* *********************************************************************** */

/* Maze printer function */
void displayMaze(char maze[ROWS][COLS], double waitTime);

/* The Pathfinder */
int findPath(char maze[ROWS][COLS], int x, int y);

/* wait function */
void wait(double seconds);

/* One Function To Rule Them All */
int main(void)
{
    /* Maze Array */
    char maze[ROWS][COLS] = {
                                "    XX X",
                                "X X    X",
                                "X XX X X",
                                "  X XX X",
                                "  X    X",
                                "X X XXXX",
                                "X X  X X",
                                "X XX    "
                            };

    /* Prepare maze for initial print */
    maze[START_X][START_Y] = START_SIGN;
    maze[END_X][END_Y] = END_SIGN;

    /* Display maze at beginnig */                     
    displayMaze(maze, INIT_WAIT);
    
    /* Prepare maze for solve */
    maze[START_X][START_Y] = PATH;
    maze[END_X][END_Y] = PATH;

    /* Start to solve maze */
	if(findPath(maze, START_X, START_Y) == TRUE ) {
        displayMaze(maze, NO_WAIT); /* Print solved maze */
        printf("Success!\n");
	} else { /* if there is no solution */
        displayMaze(maze, NO_WAIT);
		printf("The is no path to out of maze\n");
        printf("We're trapped here\n");
    };
    
	return 0;
}
/*End of Main Function*/

/* *********************************************************************** */
/*                          FUNCTION DEFINITIONS                           */
/* *********************************************************************** */

/* The maze printer */
/* It takes maze as a 2d character array and prints its ingredients to screen */
/* This function will clear screen before printing                            */
/* If step-by-step mode is on function will wait by WAIT_TIME seconds         */
/*      before terminate himself                                              */
void displayMaze(char maze[ROWS][COLS], double waitTime)
{
	int i, j; /* loop counters */
    
    system("clear"); /* clear the screen */

    /* print maze to screen */
	for ( i=0; i<ROWS; ++i ){
        for (j=0; j<COLS; ++j){
            printf("%c", maze[i][j]);
        };
        printf("\n");
    };
	printf("\n");

    /* if step-bstep mode on wait by given seconds */
    if(STEP_BY_STEP)
        wait(waitTime);
}

/* The Pathfiner function */
/* Finds how can we get out of maze by using recusive method    */
/* It takes array of maze and present coordinates of us         */
/*      then tries to solve maze                                */
int findPath(char maze[ROWS][COLS], int x, int y)
{

    /* If x,y is outside maze, return false because here is useless */
	if ( ((x < 0) || (x > (COLS - 1))) || ((y < 0) || (y > (ROWS - 1))) )
        return FALSE;

	/* If x and y points the exit point, then replace the   */
    /*      exit point's data with WAY_OUT                  */
	else if ( (x == END_X) && (y == END_Y)){
        maze[x][y] = WAY_OUT;
        return TRUE;
    }
    
	/* If x,y is walkable, return false. */
	else if (maze[x][y] != PATH){
        return FALSE;
    }
	/* Mark x,y as part of glory path. */
	maze[x][y] = PATH_SIGN;

    /* Print updates of maze array due step-by-step mode */
    if(STEP_BY_STEP)
        displayMaze(maze, WAIT_TIME);

	/* If there is a path at north of maze[x][y], return true. */
	if ( findPath(maze, x, y - 1) == TRUE )
        return TRUE;

	/* If there is a path at east of maze[x][y], return true. */
	if ( findPath(maze, x + 1, y) == TRUE )
        return TRUE;

	/* If there is a path at south of maze[x][y], return true. */
	if ( findPath(maze, x, y + 1) == TRUE )
        return TRUE;

	/* If there is a path at west of maze[x][y],  return true. */
	if ( findPath(maze, x - 1, y) == TRUE )
        return TRUE;

    /* If none of above are true then mark maze[x][y] as a bad idea */
	maze[x][y] = BLOCKED;

    /* Print updates of maze array due step-by-step mode */
    if(STEP_BY_STEP)
        displayMaze(maze, WAIT_TIME);

    /* If none of above true then this path is completely fail */
    return FALSE;
}

/* A little wait function                           */
/* Pauses program execution by given seconds value  */
void wait(double seconds)
{
    clock_t endwait;

    /* calculate when will waiting end */
    endwait = clock() + (seconds * CLOCKS_PER_SEC);

    /* make nothing while clock() lower than endwait */
    while(clock() < endwait);
}

/* *********************************************************************** */
/*                         END OF HW06_111044016_part2.c                   */
/* *********************************************************************** */
