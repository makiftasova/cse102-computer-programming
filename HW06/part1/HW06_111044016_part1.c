/* *********************************************************************** */
/*  FILENAME: HW06_111044016_part1.c                        	           */
/*  Created on 08.05.2012 by Mehmet Akif TASOVA                  	       */
/*  ---------------------------------------------------------------------- */
/* If n, k are non negative numbers and function f defined as below        */
/*   f(n, k) = f(n-1,k-1) + f(n-1, k) if n > 0, k > 0                      */
/*   f(n, k) = 1 if n >= 0, k = 0                                          */
/*   f(n, k) = 0 if n = 0, k > 0                                           */
/*                                                                         */
/* And this program will evaluaet this function by user selected number    */
/*    as a recursive function                                              */
/*                                                                         */
/* *********************************************************************** */

/* *********************************************************************** */
/*                             INCLUDES                                    */
/* *********************************************************************** */
#include <stdio.h>

/* *********************************************************************** */
/*                               DEFINITIONS                               */
/* *********************************************************************** */
/* the function f */
int f(int n, int k);

/* *********************************************************************** */
/*                         FUNCTION PROTOTYPES                             */
/* *********************************************************************** */

/* The function which rules them all */
int main(void)
{
	int n, k;

	printf("Enter value for n -->");
	scanf("%d", &n);
	printf("Enter value for k -->");
	scanf("%d", &k);
	
    printf("f(%d, %d) = %d\n",n, k, f(n,k));
    return 0;
}
/*End of Main Function*/

/* *********************************************************************** */
/*                          FUNCTION DEFINITIONS                           */
/* *********************************************************************** */

/* the function f */
int f(int n, int k)
{
	int result;
	
	if ((n == 0) && (k > 0))
		result = 0;
	if((n >= 0) && (k == 0))
		result = 1;
	if((n > 0) && (k > 0))
		result = (f(n-1, k-1) + f(n-1, k));

	return result;
}

/* *********************************************************************** */
/*                         END OF HW06_111044016_part1.c                   */
/* *********************************************************************** */
