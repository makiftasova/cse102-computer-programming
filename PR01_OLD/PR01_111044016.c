/* ************************************************************************* */
/*  FILENAME: PR01_111044016.c                                               */
/*  Created on 23.03.2012 by Mehmet Akif TASOVA                              */
/*  ------------------------------------------------------------------------ */
/* [INFORMATION ABOUT WHY YOU WROTE THIS CODE]                               */
/*                                                                           */
/*  -----------------------------------------------------------------------  */
/*  References                                                               */
/*                                                                           */
/*                                                                           */
/* ************************************************************************* */

/* ************************************************************************* */
/*                               INCLUDES                                    */
/* ************************************************************************* */
#include <stdio.h>
#include <stdlib.h>
#include <time.h>


/* ************************************************************************* */
/*                               DEFINITIONS                                 */
/* ************************************************************************* */
#define START_MONEY 10000.0
#define START_CURRENCY  't'
#define EURO_SIGN       'e'
#define DOLLAR_SIGN     'd'
#define POUND_SIGN     'p'
/* ************************************************************************* */
/*                         FUNCTION PROTOTYPES                               */
/* ************************************************************************* */

/* Function for prointing general instructions to screen */
int printInstuctions(double balance);

/* Fuction for passing header to reach rates */
int getHeaders(FILE *fileName); 

/* Function for readind day number from file and checking EOF status of file*/
int getDayNumber(FILE *fileName);

/* Function for getting exchange rates */
double getExchangeRate(FILE *fileName);

/* Returns an integer value in range [minimum,maximum) */
int randomInt(int minimum, int maximum); 

/* function for mentor 1's behaviors */
int mentor1(double balance, double euroRate, double dollarRate, double poundRate);

/* function for mentor 2's behaviors */
int mentor2(double balance, double euroRate, double dollarRate, double poundRate);

/* function for mentor 3's behaviors */
int mentor3(double balance, double euroRate, double dollarRate, double poundRate);

/* function for converting currencies each other */
double convertToEuro(double currBallance, char currCurrency, int rateEURO, int rateFromTRL);

/*funtion for printing currenct balance to screen */
double printBalance(double balance, char currency);

/* functipn for converting rates table based on another currency */
double convertRate(double currRate, char currCurrency, char targetCurrency);

/*function for pausing program by given seconds */
void pause(double second);

/* Main Function Begin */
int main(void)
{
    int day, i;
    double euroRate, dollarRate, poundRate, balance = START_MONEY;
    char currency = 't';
    
    
    FILE *exchangesTable;
    
    printInstuctions(balance);
    
    exchangesTable = fopen("ExchangeRates.txt", "r");
    getHeaders(exchangesTable);
    
    /*do{
        day = getDayNumber(exchangesTable);
        euroRate = getExchangeRate(exchangesTable);
        dollarRate = getExchangeRate(exchangesTable);
        poundRate = getExchangeRate(exchangesTable);
    
        if(day != EOF)
        {
            printf("Day #%d\n", day);
            printf("Euro \t%f\n", euroRate);
            printf("Dollar \t%f\n", dollarRate);
            printf("Pound \t%f\n", poundRate);
        };
        
        balance = convertToEuro(balance, currency, euroRate, euroRate);
        currency = 'e';
        printf("Your balance is %f euros now\n", balance);
        
    }while(day != EOF);*/
    
    do{
        i = randomInt(6,20);
        printf("random %d\n", i);
        pause(1);
    }while((i >= 6) && (i < 20));
  /* 
    balance = convertToEuro(balance, currency, euroRate, euroRate);
    printf("Your balance is %f euros now\n", balance);
    */
    printf("Hello World!...\n");
    return 0;
}
/*Main Function End*/


/* ************************************************************************* */
/*                          FUNCTION DEFINITIONS                             */
/* ************************************************************************* */
int printInstuctions(double balance)
{
    printf("Welcome to Financial Things...\n");
    printf("Your goal is earning money by changing currency of yours into others\n");
    printf("You have %.0f TRL as initial money... \n", balance);
    printf("You can want some help from advisors before changing your currency \n");
    printf("If you think your current currency will win more than others,\n");
    printf("You can keep it. You don't need change currency every turn\n");
    return 1; /* return integer value 1 if execution successful */
}

int getHeaders(FILE *fileName)
{
    char temp;
    
    do{
        fscanf(fileName, "%c", &temp);      
    }while(temp != '\n');
    
    return 1; /* function return integer value 1 if execution was successful */
}

int getDayNumber(FILE *fileName)
{
    int day, isEOF;
    isEOF = fscanf(fileName, "%d", &day);
    if(isEOF != EOF)
    {
        return day;
    }
    else
    {
        return isEOF;
    };
}

double getExchangeRate(FILE *fileName)
{
    double rate;
    fscanf(fileName, "%lf", &rate);
    return rate;
}

int randomInt(int minimum, int maximum)
{
    int number, difference;
    
    srand(time(NULL));
    if (minimum < maximum)
    {
        difference = maximum - minimum;
        number = (rand() % difference) + minimum;
        return number;
    }
    else
    {
        printf("ERROR: Invalid parameters!!!\n Please check your parameters...\n");
        return -1;
    };
}

double convertToEuro(double currBallance, char currCurrency, int rateEURO, int rateFromTRL)
{
    int balanceTL;
    
    switch(currCurrency)
    {
        case 't':
            return (currBallance / rateEURO);
            break;
        
        case 'e':
            printf("you already have %f euros...\n", currBallance);
            printf("It is unnecessary to converting curreny to euro again\n");
            return currBallance;
            break;
        
        case 'd':
            balanceTL = (currBallance * rateFromTRL);
            return (balanceTL * rateEURO);
            break;
            
        case 'p':
            balanceTL = (currBallance * rateFromTRL);
            return (balanceTL * rateEURO);
            break;
            
        default:
            printf("This currency is not recognized (%c)\n", currCurrency);
            printf("Plesae check your input...\n");
            return -1;
            break;
            
    }
}

double printBalance(double balance, char currency)
{
    switch(currency)
    {
        case 't':
            printf("You have %.0f TRL\n", balance);
            break;
        case 'e':
            printf("You have %.0f Euros\n", balance);
            break;
        case 'd':
            printf("You have %.0f Dollars\n", balance);
            break;
        case 'p':
            printf("You have %.0f Pounds\n", balance);
            break;
        default:
            printf("Unregocnized cureency(%c)", currency);
            break;
    }
    return 1;
}

void pause(double second)
{
    clock_t endwait;
    endwait = clock() + (second * CLOCKS_PER_SEC);
    while  (clock() < endwait);
}
/* ************************************************************************* */
/*                         END OF PR01_111044016.c                           */
/* ************************************************************************* */
