/*
 * allegro.c
 * 
 * Copyright 2012 Mehmet Akif TAŞOVA <makiftasova@gmail.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */


#include <stdio.h>
#include <allegro.h>

int main(int argc, char **argv)
{
   /* you should always do this at the start of Allegro programs */
   if (allegro_init() != 0)
      return 1;
 
   /* set up the keyboard handler */
   install_keyboard(); 
 
   /* set a graphics mode sized 640x480 */
   if (set_gfx_mode(GFX_AUTODETECT, 640, 480, 0, 0) != 0) {
      if (set_gfx_mode(GFX_SAFE, 640, 480, 0, 0) != 0) {
    set_gfx_mode(GFX_TEXT, 0, 0, 0, 0);
    allegro_message("Unable to set any graphic mode\n%s\n", allegro_error);
    return 1;
      }
   }
 
   /* set the color palette */
   set_palette(desktop_palette);
 
   /* clear the screen to white */
   clear_to_color(screen, makecol(255, 255, 255));
 
   /* you don't need to do this, but on some platforms (eg. Windows) things
    * will be drawn more quickly if you always acquire the screen before
    * trying to draw onto it.
    */
   acquire_screen();
 
   /* write some text to the screen with black letters and transparent background */
   textout_centre_ex(screen, font, "Hello, world!", SCREEN_W/2, SCREEN_H/2, makecol(0,0,0), -1);
 
   /* you must always release bitmaps before calling any input functions */
   release_screen();
 
   /* wait for a keypress */
   readkey();
 
   return 0;
}
 
END_OF_MAIN()

