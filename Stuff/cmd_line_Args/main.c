/* 
 * File:   main.c
 * Author: makiftasova
 *
 * Created on June 13, 2012, 11:39 AM
 */

#include <stdio.h>
#include <stdlib.h>

/*
 * 
 */
int main(int argc, char** argv) {

    int i;
    
    for(i=0; i<argc; ++i){
        printf(" %d - %s\n", i, argv[i]);
    };    
    
    return (EXIT_SUCCESS);
}

