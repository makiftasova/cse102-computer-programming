/* Another hello world program 						   */
/* @Author: Mehmet Akif TAŞOVA <makiftasova@gmail.com> */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define S "Hello World\n"

int main(void)
{
	exit(printf(S) == strlen(S) ? 0 : 1);
	return (EXIT_SUCCESS);
}
