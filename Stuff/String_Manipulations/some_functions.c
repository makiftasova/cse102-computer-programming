/* Fill string with space character 
 * Pre: a string defined and length is MAX_LEN
 * Post: string filled with space(' ') characters 
 */
void cleanString(char string[MAX_LEN])
{
    int i=0;
    
    for(i=0;i<MAX_LEN;++i)
        string[i] = SPACE;
    
    return;
}

/* Find last character of noun. Function will search a character until
 *     found a whitespace character
 * 
 * Pre: Noun to search defined in a string
 * Post: Find and returnd address of last character of noun
 */
char* findLastChar(char string[MAX_LEN], /* input string to manipulate */
                    int *index)     /* output - index of found char */
{
    int indx=0; /* variable for storing index value */
    
    /* find position of noun's last character */
    for(indx=0;(string[indx]!=SPACE)&&(string[indx]!='\0');++indx);
    --indx;
    
    *index = indx; /* return index of noun's last character */
    
    return (&string[indx]); /* return address of noun's last character */
}

/* Gets a string which includes one noun and make this noun plural
 *      return number of inserted characters to noun
 * Pre: String which includes a noun defined
 * Post: Plural form of noun
 */
int makePlural(char string[MAX_LEN])
{
    int index=0, numOfChars=0;
    char lastChar, beforeLastChar;
    
    lastChar = *findLastChar(string, &index); /* get info abour last char */
    beforeLastChar = string[index - 1];
    
    switch (tolower(lastChar))
    {
        case 'y': /* if string ends with 'y' */
            string[index] = NULL_CHAR; /* replace last char of string with NULL */
            strcat(string, ENDS_Y); /* add 'ies' suffix to string */
            numOfChars = 2;
            break;
        case 's' : /* if string ends with 's' */
            strcat(string, ENDS_S); /* add 'es 'suffix to string */
            numOfChars = 2;
            break;
        case 'h': /* if string ends with 'h' */
            switch (tolower(beforeLastChar))
            {
                case 'c': /* if string ends with 'ch' */
                case 's': /* if string ends with 'sh' */
                    strcat(string, ENDS_S); /* add 'es 'suffix to string */
                    numOfChars = 2;
                    break;
            };
            break; /* end of "ending with 'h'" situation */
            
        default: /* if noun doesn't fit other cases */
            strcat(string, ENDS_DEF); /* add 's' suffix to string */
            numOfChars = 1;
            break;
    };
    
    return (numOfChars);
}

/* Finds and replaces space characters in a string with NULL 
 *      if there is at least one
 * Pre: a string defined and has at least one space character 
 * Post: all space characters in string replaced with NULLs
 *         and returned total number of changes
 */
int replaceSpaces(char string[MAX_LEN])
{
    int index=0, count=0;
    
    for(index=0;string[index]!=NULL_CHAR;++index)
    {
        ++count;
        if(string[index] == SPACE)
        {
            string[index] = NULL_CHAR;
        };
    };
    
    return (count);
}

/* Gets a string and counts how many space 
 *      characters included in this string
 * Pre: string defined
 * Post: total number of space characters in given string counted and returned
 */
int countSpaces(const char string[MAX_LEN])
{
    int numOfSpaces=0, count=0;
    
    for(count=0;string[count]!=NULL_CHAR; ++count)
    {
        if(string[count] == SPACE)
            ++numOfSpaces;
    };
    
    return (numOfSpaces);
}

/* Finds first space character in a string and returns its index 
 * Pre: string defined
 * Post: index of first space character found and returned
 */
int findSpaces(const char string[MAX_LEN])
{
    int index=0;
    
    for(index=0;string[index]!=SPACE;++index);
    
    return (index);
}

/* Finds and replaces NULL characters in a string with space 
 *      if there is more than one
 * Pre: a string defined and has more than one NULL('\0') characters 
 * Post: NULL characters in string changed by spaces except last NULL
 *          and returned total number of changes
 */
int replaceNulls(char string[MAX_LEN])
{
    int index=0, totalNulls=0, count=0;
    
    totalNulls = countNulls(string);
    
    for(index=0;index<MAX_LEN;++index)
    {
        ++count;
        if((string[index] == NULL_CHAR) && (count < totalNulls))
        {
            string[index] = SPACE;
        };
    };
    
    return (count);
}

/* Counts total number of NULL chracters in a string 
 * Pre: A string defined with more than one NULLs
 * Post: Total number of Nulls in string counted and returned */
int countNulls(char string[MAX_LEN])
{
    int numOfNulls=0, count=0;
    
    for(count=0;count<MAX_LEN;++count)
    {
        if(string[count] == NULL_CHAR)
            ++numOfNulls;
    };
    
    return (numOfNulls);
}
