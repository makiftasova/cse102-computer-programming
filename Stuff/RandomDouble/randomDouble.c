/*
 * randomDouble.c
 * 
 * Copyright 2012 Mehmet Akif TAŞOVA <makiftasova@gmail.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */


#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>


/* a double for creating random doubles more randomly */
#define RANDOM_DECREASE 0.628456


/*generates a random integer between given minimum and maximum numbers */
/* and minimum and maximum numbers are included in return range        */
int getRandomInt(int minimum, int maximum);

/*generates a random double number in given range by using  */
/*  getRandomInt function. int variable precision defines   */
/*  length of fraction part of generated double             */
/*  precision value must be powers of 10                    */
double getRandomDouble(int minimum, int maximum, int precision);

int main(int argc, char **argv)
{
    double x;
    
    x = getRandomDouble(0,100,5);
    
    printf("%f\n", x);
    
    return 0;
}

/*generates a random integer between given minimum and maximum numbers */
/* and minimum and maximum numbers are included in return range        */
int getRandomInt(int minimum, int maximum)
{
    int temp=0;
    
    if (minimum < maximum)
    {
        return (rand() % ((maximum - minimum + 1)) + minimum);
    }
    if(minimum > maximum)
    {
        temp = maximum;
        maximum = minimum;
        minimum = temp;
        return (rand() % ((maximum - minimum + 1)) + minimum);
    }
    else
    {
        printf("ERROR: Invalid parameters!!!\n");
        printf("Please check your parameters...\n");
        return -1;
    };
}

/*generates a random double number in given range by using  */
/*  getRandomInt function. int variable precision defines   */
/*  length of fraction part of generated double             */
/*  precision value must be powers of 10                    */
double getRandomDouble(int minimum, int maximum, int precision)
{
    double num1, num2; /* local variables for calling getRandomInt function */
    
    double returnRandom; /* variable for returning random number */
    
    double divider; /* variable for storing precision divider */
    
    divider = pow(10, precision);
    
    num1 = getRandomInt(minimum, maximum);
    
    num2 = getRandomInt(minimum, maximum);
    
    num2 /= divider;
    
    /* decrease value num2 for creating more randomly numbers */
    num2 -= RANDOM_DECREASE;
    
    returnRandom = (num1 + num2);
    
    return returnRandom;
    
}
