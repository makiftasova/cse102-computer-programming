/** CSS placed here will be applied to all skins */
/** allefant doesnt like the indentation :(
div#bodyContent p { margin-left:1em; }
div#bodyContent div pre { margin-left:1em; }
div#bodyContent div ul { margin-left:1em; }
div#bodyContent div ol { margin-left:1em; }
*/

/* style for tables */
table.wikitable {
    margin: 1em;
    border: 1px #111111 solid;
}

table.wikiTable {
    margin: 0em;
    border: 1px #111111 solid;
}

table.wikitable th {
    padding: 0.1em;
    background-color: #ccddee
}

table.wikiTable th {
    padding: 0.0em;
    background-color: #ccddee
}

table.wikitable td {
    padding: 0.1em;
}

table.wikiTable td {
    padding: 0.0em;
}

table.wikiTable td.wikiTableRowEven {
    background-color: #ddeeff
}

table.wikiTable td.wikiTableRowOdd {
    background-color: #ccddee
}

/* Remove heading form Main Page */
.page-Main_Page h1.firstHeading {
    display: none;
}