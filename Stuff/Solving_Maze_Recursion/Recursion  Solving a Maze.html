
<!-- saved from url=(0039)http://www.cs.bu.edu/teaching/alg/maze/ -->
<html><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Recursion: Solving a Maze</title>
<script src="./Recursion  Solving a Maze_files/vars.js" language="Javascript1.2">
</script>
<script src="./Recursion  Solving a Maze_files/charmatrix.js" language="Javascript1.2">
</script>
<script src="./Recursion  Solving a Maze_files/maze.js" language="Javascript1.2">
</script>
<script src="./Recursion  Solving a Maze_files/code.js" language="Javascript1.2">
</script>
</head>

<body onload="setup()" onresize="history.go(0)">

<h1 align="center">Recursion: Solving a Maze</h1>

<hr>

<h2>The Problem</h2>

A robot is asked to navigate a maze.  It is placed at a certain
position (the <em>starting</em> position) in the maze and is asked to
try to reach another position (the <em>goal</em> position).  Positions
in the maze will either be open or blocked with an obstacle.  Positions
are identified by (x,y) coordinates.

<p>
<img src="./Recursion  Solving a Maze_files/simple-maze.gif" alt="[Example of a simple maze]" align="left" hspace="10" vspace="4">
At any given moment, the robot can only move 1 step in one of 4 directions.
Valid moves are:

</p><p>
</p><ul>
<li>Go North: (x,y) -&gt; (x,y-1)
</li><li>Go East: (x,y) -&gt; (x+1,y)
</li><li>Go South: (x,y) -&gt; (x,y+1)
</li><li>Go West: (x,y) -&gt; (x-1,y)
</li></ul>

<p>
Note that positions are specified in zero-based coordinates (i.e.,
0...size-1, where <em>size</em> is the size of the maze in the
corresponding dimension).

</p><p>
The robot can only move to positions without obstacles and must stay
within the maze.

</p><p>
The robot should search for a path from the starting position to the
goal position (a <em>solution path</em>) until it finds one or until it
exhausts all possibilities.  In addition, it should mark the path it
finds (if any) in the maze.
<br clear="all">

</p><p>
</p><hr>

<h2>Representation</h2>

To make this problem more concrete, let's consider a maze represented
by a matrix of characters.  An example 6x6 input maze is:

<p>
</p><center>
<table cellpadding="6">
<tbody><tr valign="top">
<td>
<script>
insertMaze("S#####\n.....#\n#.####\n#.####\n...#.G\n##...#")
</script><pre>S#####
.....#
#.####
#.####
...#.G
##...#
</pre>
</td>
<td width="5%">
</td>
<td>

<table cellspacing="0">
<tbody><tr valign="top">
<td align="center">
'<script>insertChar("open")</script><code>.</code>'</td><td>-</td>
<td>
where the robot can move (open positions)
</td>
</tr>
<tr valign="top">
<td align="center">
'<script>insertChar("blocked")</script><code>#</code>'</td><td>-</td>
<td>
obstacles (blocked positions)
</td>
</tr>
<tr valign="top">
<td align="center">
'<script>insertChar("start")</script><code>S</code>'</td><td>-</td>
<td>
start position (here, x=0, y=0)
</td>
</tr>
<tr valign="top">
<td align="center">
'<script>insertChar("goal")</script><code>G</code>'</td><td>-</td>
<td>
goal (here, x=5, y=4)
</td>
</tr>
</tbody></table>

</td>
</tr>
</tbody></table>
</center>

<p>
</p><hr align="left" width="20%">
<b>Aside:</b> Remember that we are using <em>x</em> and <em>y</em>
coordinates (that start at 0) for maze positions.  A <em>y</em>
coordinate therefore corresponds to a row in the matrix and an
<em>x</em> coordinate corresponds to a column.
<hr align="left" width="20%">

<p>
A path in the maze can be marked by the
'<script>insertChar("path")</script><code>+</code>' symbol...

</p><p>
<table cellpadding="6">
<tbody><tr>
<td>
A <em>path</em> refers to either a <em>partial</em> path, marked while the
robot is still searching:
</td>
<td>
<script>
insertMaze("+#####\n++++.#\n#.####\n#.####\n...#.G\n##...#")
</script><pre>+#####
++++.#
#.####
#.####
...#.G
##...#
</pre>
</td>
<td>
(i.e., one that may or may not lead to a solution).  Or, a
<em>solution</em> path:
</td>
<td>
<script>
insertMaze("S#####\n++...#\n#+####\n#+####\n.++#+G\n##+++#")
</script><pre>S#####
++...#
#+####
#+####
.++#+G
##+++#
</pre>
</td>
<td>
which leads from start to goal.
</td>
</tr>
</tbody></table>

</p><p>
</p><hr>

<h2>Algorithm</h2>

We'll solve the problem of finding and marking a solution path using
<em>recursion</em>.

<p>
Remember that a recursive algorithm has at least 2 parts:

</p><p>
</p><ul>
<li><em>Base case(s)</em> that determine when to stop.

<p>
</p></li><li><em>Recursive part(s)</em> that call the same algorithm (i.e., itself)
to assist in solving the problem.

</li></ul>

<h3>Recursive parts</h3>

Because our algorithm must be <em>recursive</em>, we need to view the
problem in terms of similar <em>subproblems</em>.  In this case, that
means we need to "find a path" in terms of "finding paths."

<p>
Let's start by assuming there is already some algorithm that finds a path
from some point in a maze to the goal, call it <code>FIND-PATH(x, y)</code>.

</p><p>
Also suppose that we got from the start to position x=1, y=2 in the
maze (by some method):

</p><p>
<table cellpadding="6">
<tbody><tr>
<td>
<script>
insertMaze(
  "+#####\n++...#\n#+####\n#.####\n...#.G\n##...#",
  {x: 1, y: 2}
)
</script><pre>+#####
++...#
#<font color="#ee0000">+</font>####
#.####
...#.G
##...#
</pre>
</td>
<td>
What we now want to know is whether there is a path from x=1, y=2 to
the goal.  If there is a path to the goal from x=1, y=2, then there is
a path from the <em>start</em> to the goal (since we already got to
x=1, y=2).
</td>
</tr>
</tbody></table>

</p><p>
To find a path from position x=1, y=2 to the goal, we can just ask
<code>FIND-PATH</code> to try to find a path from the
<script>insertDir(0)</script>North, <script>insertDir(1)</script>East,
<script>insertDir(2)</script>South, and <script>insertDir(3)</script>West of
x=1, y=2:

</p><p>
</p><ul>
<li><code>FIND-PATH(<script>insertCoord(1, 2, 0)</script>x=1, y=1)</code>
<em><script>insertDir(0)</script>North</em>
</li><li><code>FIND-PATH(<script>insertCoord(1, 2, 1)</script>x=2, y=2)</code>
<em><script>insertDir(1)</script>East</em>
</li><li><code>FIND-PATH(<script>insertCoord(1, 2, 2)</script>x=1, y=3)</code>
<em><script>insertDir(2)</script>South</em>
</li><li><code>FIND-PATH(<script>insertCoord(1, 2, 3)</script>x=0, y=2)</code>
<em><script>insertDir(3)</script>West</em>
</li></ul>

<p>
Generalizing this, we can call <code>FIND-PATH</code> recursively to
move from any location in the maze to adjacent locations.  In this way,
we move through the maze.

</p><h3>Base cases</h3>

It's not enough to know how to use <code>FIND-PATH</code> recursively
to advance through the maze.  We also need to determine when
<code>FIND-PATH</code> must stop.

<p>
One such <em>base case</em> is to stop when it <em>reaches the goal</em>.

</p><p>
The other base cases have to do with moving to invalid positions.  For
example, we have mentioned how to search <script>insertDir(0)</script>North of the
current position, but disregarded whether that <script>insertDir(0)</script>North
position is legal.  In order words, we must ask:

</p><p>
</p><ul>
<li><em>Is the position in the maze</em> (...or did we just go outside
its bounds)?
</li><li><em>Is the position open</em> (...or is it blocked with an obstacle)?
</li></ul>

<p>
Now, to our base cases and recursive parts, we must add some steps to
mark positions we are trying, and to unmark positions that we tried, but
from which we failed to reach the goal:

</p><p>
<code>
<b>FIND-PATH(x, y)</b>
</code></p><ol><code>
<li>if (x,y outside maze) return false
</li><li>if (x,y is goal) return true
</li><li>if (x,y not open) return false
</li><li>mark x,y as part of solution path
</li><li>if (FIND-PATH(<script>insertDir(0)</script>North of x,y) == true) return true
</li><li>if (FIND-PATH(<script>insertDir(1)</script>East of x,y) == true) return true
</li><li>if (FIND-PATH(<script>insertDir(2)</script>South of x,y) == true) return true
</li><li>if (FIND-PATH(<script>insertDir(3)</script>West of x,y) == true) return true
</li><li>unmark x,y as part of solution path
</li><li>return false
</li></code></ol><code>
</code>

All these steps together complete a basic algorithm that finds and
marks a path to the goal (if any exists) and tells us whether a path
was found or not (i.e., returns true or false).  This is just one such
algorithm--other variations are possible.

<p>
</p><hr align="left" width="20%">
<b>Note:</b>
<code>FIND-PATH</code> will be called at least once for each position
in the maze that is tried as part of a path.

<p>
Also, after going to another position (e.g., <script>insertDir(0)</script>North):

</p><blockquote><code>
if (FIND-PATH(<script>insertDir(0)</script>North of x,y)¹ == true) return true²
</code></blockquote>

if a path to the goal was found, it is important that the algorithm
stops.  I.e., if going <script>insertDir(0)</script>North of x,y finds a path
(i.e., returns <strong>true</strong>¹), then from the current
position (i.e., current call of <code>FIND-PATH</code>) there is no
need to check <script>insertDir(1)</script>East, <script>insertDir(2)</script>South
or <script>insertDir(3)</script>West.  Instead, <code>FIND-PATH</code> just
need <strong>return true</strong>² to the previous call.

<p>
Path marking will be done with the '<script>insertChar("path")</script><code>+</code>' symbol
and unmarking with the '<script>insertChar("badpath")</script><code>x</code>' symbol.

</p><hr align="left" width="20%">

<h3>Using Algorithm</h3>

To use <code>FIND-PATH</code> to find and mark a path from the start to
the goal with our given representation of mazes, we just need to:

<p>
</p><ol>
<li>Locate the start position (call it <em>startx</em>, <em>starty</em>).
</li><li>Call <code>FIND-PATH(startx, starty)</code>.
</li><li>Re-mark<b>*</b> the start position with
'<script>insertChar("start")</script><code>S</code>'.
</li></ol>

<p>
</p><hr align="left" width="20%">
<b>*</b>In the algorithm, the start position (marked
'<script>insertChar("start")</script><code>S</code>') needs to be considered an
<em>open</em> position and must be <em>marked</em> as part of the path
for <code>FIND-PATH</code> to work correctly.  That is why we re-mark
it at the end.
<hr align="left" width="20%">

<h3>Backtracking</h3>

An important capability that the recursive parts of the algorithm will
give us is the ability to <em>backtrack</em>.

<p>
<table cellpadding="6">
<tbody><tr>
<td>
<script>
insertMaze(
  "++####\n#+#..#\n#+#..#\n#++#.#\n###...\nG...##",
  {x: 2, y: 3}
)
</script><pre>++####
#+#..#
#+#..#
#+<font color="#ee0000">+</font>#.#
###...
G...##
</pre>
</td>
<td valign="center">
For example, suppose the algorithm just <em>marked</em> position x=2,
y=3 in this maze.  I.e, it is in the call to <code>FIND-PATH(x=2,
y=3)</code>.  After marking...
</td>
</tr>
</tbody></table>

</p><p>
<table cellpadding="6">
<tbody><tr>
<td>
<script>
insertMaze(
  "++####\n#+#..#\n#+#..#\n#++#.#\n###...\nG...##",
  coordOf(2, 3, 0)
)
</script><pre>++####
#+#..#
#+<font color="#ee0000">#</font>..#
#++#.#
###...
G...##
</pre>
</td>
<td valign="top">
First, it will try to find a path to the goal from the position
<font color="#ee0000"><script>insertDir(0)</script>North</font> of x=2, y=3,
calling <code>FIND-PATH(<script>insertCoord(2, 3, 0)</script>x=2, y=2)</code>.

<p>
Since the <script>insertDir(0)</script>North position is not open, the call
<code>FIND-PATH(<script>insertCoord(2, 3, 0)</script>x=2, y=2)</code> will return
false, and then it will go back (<em>backtrack</em>) to
<code>FIND-PATH(x=2, y=3)</code> and resume at the step just after it
went <script>insertDir(0)</script>North.
</p></td>
</tr>
</tbody></table>

</p><p>
<table cellpadding="6">
<tbody><tr>
<td>
<script>
insertMaze(
  "++####\n#+#..#\n#+#..#\n#++#.#\n###...\nG...##",
  coordOf(2, 3, 1)
)
</script><pre>++####
#+#..#
#+#..#
#++<font color="#ee0000">#</font>.#
###...
G...##
</pre>
</td>
<td valign="top">
Next, it will go <font color="#ee0000"><script>insertDir(1)</script>East</font>
of x=2, y=3, calling
<code>FIND-PATH(<script>insertCoord(2, 3, 1)</script>x=3, y=3)</code>.

<p>
This position is not open, so it will <em>backtrack</em> to
<code>FIND-PATH(x=2, y=3)</code> and resume at the step just after it
went <script>insertDir(1)</script>East.
</p></td>
</tr>
</tbody></table>

</p><p>
<table cellpadding="6">
<tbody><tr>
<td>
<script>
insertMaze(
  "++####\n#+#..#\n#+#..#\n#++#.#\n###...\nG...##",
  coordOf(2, 3, 2)
)
</script><pre>++####
#+#..#
#+#..#
#++#.#
##<font color="#ee0000">#</font>...
G...##
</pre>
</td>
<td valign="top">
Next, it will go <font color="#ee0000"><script>insertDir(2)</script>South</font>
of x=2, y=3, calling call
<code>FIND-PATH(<script>insertCoord(2, 3, 2)</script>x=2, y=4)</code>.

<p>
This position is not open, so it will <em>backtrack</em> to
<code>FIND-PATH(x=2, y=3)</code> and resume at the step just after it
went <script>insertDir(2)</script>South.
</p></td>
</tr>
</tbody></table>

</p><p>
<table cellpadding="6">
<tbody><tr>
<td>
<script>
insertMaze(
  "++####\n#+#..#\n#+#..#\n#++#.#\n###...\nG...##",
  coordOf(2, 3, 3)
)
</script><pre>++####
#+#..#
#+#..#
#<font color="#ee0000">+</font>+#.#
###...
G...##
</pre>
</td>
<td valign="top">
Finally, it will go <font color="#ee0000"><script>insertDir(3)</script>West</font> of x=2, y=3, calling
<code>FIND-PATH(<script>insertCoord(2, 3, 3)</script>x=1, y=3)</code>.

<p>
This position is not open, so it will <em>backtrack</em> to
<code>FIND-PATH(x=2, y=3)</code> and resume at the step just after it
went <script>insertDir(3)</script>West.

</p><p>
Since <script>insertDir(3)</script>West is the last direction to search
from x=2, y=3, it will unmark x=2, y=3, and <em>backtrack</em> to the
previous call, <code>FIND-PATH(x=1, y=3)</code>.
</p></td>
</tr>
</tbody></table>

</p><p>
To better illustrate this <em>backtracking</em>, let's look at an
example run in which a dead end is reached at some point in the
search:

</p><p>
</p><form name="mazeForm">

<center>
<table cellpadding="4" border="1">

<tbody><tr valign="center">
<td align="center" rowspan="2">

<table>
<tbody><tr>
<td>
</td>
<td align="left">
Maze:
</td>
</tr>
<tr>
<td valign="center" align="right">
y
</td>
<td>
<textarea name="mazeEntry" rows="6" cols="10" onchange="
maze.set(mazeForm.mazeEntry.value)
this.value = maze
changed = true
"></textarea>
</td>
</tr>
<tr>
<td>
</td>
<td valign="top" align="center">
x
</td>
</tr>
</tbody></table>

</td>

<td valign="center" align="left">

<div style="margin-left:1em;">
Symbols:

<small>
<em>
'<script>insertChar("open")</script><code>.</code>' = open,
'<script>insertChar("blocked")</script><code>#</code>' = blocked,
'<script>insertChar("start")</script><code>S</code>' = start,
'<script>insertChar("goal")</script><code>G</code>' = goal,
'<script>insertChar("path")</script><code>+</code>' = path<script>
if (maze.chars.badpath != maze.chars.open)
  document.write(", '")
  insertChar("badpath")
  document.write("' = bad path")
</script>, '<code>x</code>' = bad path
</em>
</small>

</div>

</td>
</tr>

<tr>
<td align="left">

<div style="margin-left:1em;">

<table cellspacing="0">
<tbody><tr>
<td>
Order:
</td>
<td>
<select name="dirEntry" onchange="setDir(this, 0, running != null)">
<option value="N">1.North
</option><option value="E">1.East
</option><option value="S">1.South
</option><option value="W">1.West
</option></select>
<select name="dirEntry" onchange="setDir(this, 1, running != null)">
<option value="N">2.North
</option><option value="E">2.East
</option><option value="S">2.South
</option><option value="W">2.West
</option></select>
<select name="dirEntry" onchange="setDir(this, 2, running != null)">
<option value="N">3.North
</option><option value="E">3.East
</option><option value="S">3.South
</option><option value="W">3.West
</option></select>
<select name="dirEntry" onchange="setDir(this, 3, running != null)">
<option value="N">4.North
</option><option value="E">4.East
</option><option value="S">4.South
</option><option value="W">4.West
</option></select>
</td>
</tr>
</tbody></table>

<p>
Run Speed:
<input type="radio" name="runInterval" value="1400" onclick="
interval = this.value
">
Slow
<input type="radio" name="runInterval" value="600" checked="" onclick="
interval = this.value
">
Medium
<input type="radio" name="runInterval" value="100" onclick="
interval = this.value
">
Fast

</p><p>
Follow:
<input type="checkbox" name="algCheck" onclick="
if (this.checked)
  algWin=window.open(
    &quot;alg.html&quot;, &quot;ALG&quot;,
    &quot;width=440,height=455,resizable=yes,scrollbars=yes,dependent=yes&quot;
  )
else
  algWin.close()
">
Algorithm
<input type="checkbox" calls"="" name="callsCheck" onclick="
if (this.checked)
  callsWin=window.open(
    &quot;calls.html&quot;, &quot;CALLS&quot;,
    &quot;width=455,height=260,resizable=yes,scrollbars=yes,dependent=yes&quot;
  )
else
  callsWin.close()
">
Recursive Calls

</p></div>

</td>
</tr>

<tr valign="center">
<td align="left" colspan="2">

<table>

<tbody><tr valign="center">
<td align="left">
Current:
</td>
<td align="left">
Step description:
</td>
<td align="left">
Goal:
</td>
<td align="left">
Search:
</td>
</tr>

<tr valign="top">
<td align="left">
x=<input type="text" name="curx" value="" size="3" onfocus="this.blur()">
<nobr>
y=<input type="text" name="cury" value="" size="3" onfocus="this.blur()">
</nobr>
</td>
<td align="left">
<input type="text" name="stepDesc" value="" size="18" onfocus="this.blur()">
</td>
<td align="left">
<input type="text" name="reachedGoal" value="No" size="3" onfocus="this.blur()">
</td>
<td align="left">
<input type="button" value="Run" onclick="
if (running != null)
  return
if (changed) {
  if (!maze.startedSearch() ||
        confirm(&quot;Parameters changed.  Re-run search?&quot;)) {
    changed = false
    resetSearch()
    if (firstStep())
      running = setTimeout(runSearch, interval)
  }
} else if (!maze.startedSearch()) {
  if (firstStep())
    running = setTimeout(runSearch, interval)
} else if (maze.noMoreSteps()) {
  if (confirm(&quot;Search done.  Re-run search?&quot;)) {
    resetSearch()
    if (firstStep())
      running = setTimeout(runSearch, interval)
  }
} else {
  oneStep()
  running = setTimeout(runSearch, interval)
}
">
<input type="button" value="Stop" onclick="running = null">
<input type="button" value="One Step" onclick="
if (running != null)
  return
if (changed) {
  if (!maze.startedSearch() ||
        confirm(&quot;Parameters changed.  Re-do first step of search?&quot;)) {
    changed = false
    resetSearch()
    firstStep()
  }
} else if (!maze.startedSearch()) {
  firstStep()
} else if (maze.noMoreSteps()) {
  if (confirm(&quot;Search done.  Re-do first step of search?&quot;)) {
    resetSearch()
    firstStep()
  }
} else {
  oneStep()
}
">
<input type="button" value="Reset" onclick="resetSearch()">
</td>
</tr>

</tbody></table>

</td>
</tr>

</tbody></table>
</center>

</form>

<p>
Feel free to follow the search algorithm using other input mazes and
parameters.

</p><p>
</p><hr>

<h2>Questions</h2>

In order to demonstrate an understanding of this problem and solution,
you should be able to answer the following questions:

<p>
</p><ul>
<li>What happens if instead of searching in the order
<em><script>insertDir(0)</script>North, <script>insertDir(1)</script>East,
<script>insertDir(2)</script>South, <script>insertDir(3)</script>West</em>,
<code>FIND-PATH</code> searchs <em><script>insertDir(0)</script>North,
<script>insertDir(2)</script>South, <script>insertDir(1)</script>East,
<script>insertDir(3)</script>West</em>?

<p>
</p></li><li>In all cases when <code>FIND-PATH</code> returns <em>false</em>,
does that mean there is no path from the start to the goal?

<p>
</p></li><li>Can parts of the maze be searched by <code>FIND-PATH</code> more
than once?  If so, how can this be rectified?  If not, how does the
algorithm prevent that?

<p>
</p></li><li>What can you say about the kinds of paths that <code>FIND-PATH</code>
finds?

</li></ul>

<p>
</p><hr>
<address>
BU CAS CS - Recursion: Solving a Maze
<br>
Copyright © 1993-2000 by
<a target="_top">Robert I. Pitts</a>
&lt;rip at bu dot edu&gt;.
All Rights Reserved.
</address>


</body></html>