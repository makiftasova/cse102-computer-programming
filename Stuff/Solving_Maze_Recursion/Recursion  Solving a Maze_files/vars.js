// Copyright 2000 by Robert I. Pitts <rip@cs.bu.edu>
// All rights reserved.

// getURLVars:
//
//   Parse var=value pairs given with URL (after ?) and return them
//   in an array.  Each variable's value will be stored in array at
//   both indices "varname" and #, where # is the array index
//   representing the order it was present in the URL.
//
//   If you supply one or more variable names, then you can be sure
//   that the array index "varname" for that variable will be null
//   if that variable was not specified in the URL.

function getURLVars()
{
  var vals = new Array

  for (var i = 0; i < arguments.length; i++)
    vals[arguments[i]] = null

  // Skip first char, which is ?
  // Var=value pairs are separated by &'s.
  var pairs = location.search.substr(1).split("&")

  for (var i = 0; i < pairs.length; i++) {
    var pair = pairs[i].split("=")

    // Use unescape() to translate %NN (i.e., escaped) characters.

    var ident = unescape(pair[0])

    if (arguments.length == 0 ||
          typeof vals[ident] != "undefined") {
      vals[ident] = unescape(pair[1])
    }
  }

  return vals
}
